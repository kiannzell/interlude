package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.handler.SkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Object;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HealPercent implements ISkillHandler 
{
    private static final Logger LOGGER = LoggerFactory.getLogger(HealPercent.class.getName());
    private static final SkillType[] SKILL_IDS = 
    	{
    	SkillType.CPHEAL_PERCENT
    	};

    @Override
    public void useSkill(L2Character activeChar, L2Skill skill, L2Object[] targets) 
    {
        //check for other effects
        try {
            ISkillHandler handler = SkillHandler.getInstance().getSkillHandler(SkillType.BUFF);

            if (handler != null) {
                handler.useSkill(activeChar, skill, targets);
            }
        } catch (Exception e) {
            LOGGER.error("", e);
        }
        for(L2Object object : targets)
		{
			if (!(object instanceof L2Character))
			{
				continue;
			}
        SystemMessage sm;
		double amount = 0;
		boolean full = skill.getPower() == 100.0;
		L2Character target = (L2Character) object;
        {
        if (skill.getSkillType() == SkillType.CPHEAL_PERCENT) 
        {
        	amount = Math.min(((full) ? target.getMaxCp() : (target.getMaxCp() * skill.getPower() / 100.0)), target.getMaxCp() - target.getCurrentCp());
			target.setCurrentCp(amount + target.getCurrentCp());
			
			sm = SystemMessage.getSystemMessage(SystemMessageId.S1_CP_WILL_BE_RESTORED);
			sm.addNumber((int) amount);
			target.sendPacket(sm);
			StatusUpdate sump = new StatusUpdate(target.getObjectId());
			sump.addAttribute(StatusUpdate.CUR_CP, (int) target.getCurrentCp());
        }
       }
    }
   }

    @Override
    public SkillType[] getSkillIds() 
    {
        return SKILL_IDS;
    }
}