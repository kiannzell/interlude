package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

/**
 * This class allows user to turn XP-gain off and on.
 *
 * @author Notorious ( Sakretsos Rework on frozen )
 */
public class NoExp implements IVoicedCommandHandler
{
   private static final String[] _voicedCommands =
   {
      "expoff",
      "expon"
   };
   
   /**
    * 
    * @author Notorious ( Sakretsos Rework on frozen )
    */
   @Override
public boolean useVoicedCommand(String command, L2PcInstance activeChar, String params)
   {
      if (command.equalsIgnoreCase("expoff"))
      {
         activeChar.cantGainXP(true);
         activeChar.sendMessage("SVR : You are not receiving experience from mobs anymore.");
      }
      else if (command.equalsIgnoreCase("expon"))
      {
         activeChar.cantGainXP(false);
         activeChar.sendMessage("SVR : You are now receiving experience from mobs.");
      }
      return true;
   }
   
   /**
    * 
    * @author Notorious ( Sakretsos Rework on frozen )
 * @return 
    */
   @Override
public String[] getVoicedCommandList()
   {
      return _voicedCommands;
   }
}