package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.logging.Logger;

import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;


public class HelpVoice implements IVoicedCommandHandler
{
	static final Logger _log = Logger.getLogger(HelpVoice.class.getName());
	
	public static final int ZONE_PEACE = 2;
	
	private static final String[]	_voicedCommands	=
		{ 
		"shopkentoy", "bufferkentoy", "ggkkentoy", "classchangekentoy", "eventmanagerkentoy", "castlekentoy", "dungeonkentoy", "delevelkentoy", "subclassnoblessekentoy"
		};
	
	private static final String PARENT_DIR = "data/html/mods/";
	
    @Override
    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target) 
    {
    	if(activeChar.isMovementDisabled() || activeChar.isAlikeDead())
			return false;
		
		if(!activeChar.isInsideZone(ZONE_PEACE))
		{
			activeChar.sendMessage("You can only use this feature in peace zone.");
			return false;
		}
		
		if(activeChar.isInFunEvent())
		{
			activeChar.sendMessage("You can't use this feature! You are in event now.");
			return false;
		}
		
    	if(activeChar.isCursedWeaponEquiped())
		{
			activeChar.sendMessage("You can't use this feature! You are currently holding a cursed weapon.");
			return false;
		}

		//check player is in Olympiade
		if(activeChar.isInOlympiadMode() || activeChar.getOlympiadGameId() != -1)
		{
			activeChar.sendMessage("You can't use this feature! Your are fighting in Olympiad!");
			return false;
		}

		// Check player is in observer mode
		if(activeChar.inObserverMode())
		{
			activeChar.sendMessage("You can't use this feature! You are in Observer mode!");
			return false;
		}
		
		if(activeChar.isInFunEvent() || activeChar.isInOlympiadMode())
		{
			activeChar.sendMessage("Sorry,you are in event now.");
			return false;
		}
    	
        if (command.equalsIgnoreCase("shopkentoy")) 
        {
        	activeChar.teleToLocation(46848, 186044, -3480);
        	NpcHtmlMessage html = new NpcHtmlMessage(1);
    		html.setFile(PARENT_DIR+"help.htm");
    		sendHtmlMessage(activeChar, html);
		}
		else if(command.equalsIgnoreCase("bufferkentoy"))
		{
			activeChar.teleToLocation(47665, 185652, -3512);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("ggkkentoy"))
		{
			activeChar.teleToLocation(47917, 186808, -3480);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("classchangekentoy"))
		{
			activeChar.teleToLocation(48961, 185927, -3480);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("eventmanagerkentoy"))
		{
			activeChar.teleToLocation(48132, 185132, -3480);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("castlekentoy"))
		{
			activeChar.teleToLocation(48601, 186577, -3480);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("dungeonkentoy"))
		{
			activeChar.teleToLocation(46633, 187693, -3480);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("delevelkentoy"))
		{
			activeChar.teleToLocation(47661, 187147, -3512);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
		else if(command.equalsIgnoreCase("subclassnoblessekentoy"))
		{
			activeChar.teleToLocation(47957, 185306, -3512);
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"help.htm");
			sendHtmlMessage(activeChar, html);
        }
        return false;
    }
    
    private void sendHtmlMessage(L2PcInstance player, NpcHtmlMessage html)
    {
        player.sendPacket(html);
    }

    @Override
    public String[] getVoicedCommandList() {
        return _voicedCommands;
    }
}

