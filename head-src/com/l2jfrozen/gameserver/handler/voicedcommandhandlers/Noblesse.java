/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.handler.admincommandhandlers.AdminNoble;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.ItemList;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;

/**
 * 
 * 
 * @author Sensei
 */
public class Noblesse implements IVoicedCommandHandler
{
	protected static final Logger LOGGER = LoggerFactory.getLogger(AdminNoble.class.getName());
	
	private static String[] _voicedCommands =
	{
			"setmenoblesseHOMIE"
	};

	/**
	 * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#useVoicedCommand(java.lang.String,
	 *      com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance, java.lang.String)
	 */
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	{
	    
		if (activeChar.isNoble())
		{
			activeChar.sendMessage("You're already a noblesse!");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return true;
		}
		else if (activeChar.isAio())
		{
			activeChar.sendMessage("Aio buffers are not allowed!");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return true;
		}
		else if (activeChar.getInventory().getItemByItemId(9210) == null && activeChar.getInventory().getItemByItemId(9211) == null 
				&& activeChar.getInventory().getItemByItemId(9213) == null && activeChar.getInventory().getItemByItemId(9212) == null)
		{
			activeChar.sendMessage("You don't have the requirments needed to become a noblesse!");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return true;
		}
		else if (activeChar.getInventory().getItemByItemId(9210) == null || activeChar.getInventory().getItemByItemId(9211) == null 
				|| activeChar.getInventory().getItemByItemId(9213) == null || activeChar.getInventory().getItemByItemId(9212) == null)
		{
			activeChar.sendMessage("You don't have the requirments needed to become a noblesse!");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return true;
		}
		else
		{
			activeChar.setNoble(true);
			activeChar.destroyItemByItemId("Noble", 9210, 1, activeChar, true);
			activeChar.destroyItemByItemId("Noble", 9211, 1, activeChar, true);
			activeChar.destroyItemByItemId("Noble", 9212, 1, activeChar, true);
			activeChar.destroyItemByItemId("Noble", 9213, 1, activeChar, true);
			activeChar.getInventory().addItem("NoblesseTiara", 7694, 1, activeChar, null);
			activeChar.getInventory().updateDatabase();
			updateDatabase(activeChar, true);
			activeChar.sendPacket(new ItemList(activeChar, true));
			activeChar.sendMessage("Congratulations! You are now a noblesse.");
			activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), 16));
		}
		return true;
	}
	
	private void updateDatabase(L2PcInstance player, boolean newNoble)
	{
		Connection con = null;

		try
		{
			// prevents any NPE.
			// ----------------
			if(player == null)
				return;

			// Database Connection
			//--------------------------------
			con = L2DatabaseFactory.getInstance().getConnection(false);
			PreparedStatement stmt = con.prepareStatement(newNoble ? INSERT_DATA : DEL_DATA);

			// if it is a new donator insert proper data
			// --------------------------------------------
			if(newNoble)
			{

				stmt.setInt(1, player.getObjectId());
				stmt.setString(2, player.getName());
				stmt.setInt(3, player.isHero() ? 1 : 0);
				stmt.setInt(4, 1);
				stmt.setInt(5, player.isDonator() ? 1 : 0);
				stmt.execute();
				stmt.close();
				stmt = null;
			}
			else
			// deletes from database
			{
				stmt.setInt(1, player.getObjectId());
				stmt.execute();
				stmt.close();
				stmt = null;
			}
		}
		catch(Exception e)
		{
			if(Config.ENABLE_ALL_EXCEPTIONS)
				e.printStackTrace();
			
			LOGGER.info( "Error: could not update database: ", e);
		}
		finally
		{
			CloseUtil.close(con);
		}
	}

	// Updates That Will be Executed by MySQL
	// ----------------------------------------
	String INSERT_DATA = "REPLACE INTO characters_custom_data (obj_Id, char_name, hero, noble, donator) VALUES (?,?,?,?,?)";
	String DEL_DATA = "UPDATE characters_custom_data SET noble = 0 WHERE obj_Id=?";

	
	/**
	 * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#getVoicedCommandList()
	 */
	@Override
	public String[] getVoicedCommandList()
	{
		return _voicedCommands;
	}
}
