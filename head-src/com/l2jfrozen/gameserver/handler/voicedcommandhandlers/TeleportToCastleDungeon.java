/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.datatables.sql.ItemTable;
import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.event.LastManStanding;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;

/**
 * 
 * 
 * @author Sensei
 */
public class TeleportToCastleDungeon implements IVoicedCommandHandler
{
	private static String ItemName = ItemTable.getInstance().createDummyItem(Config.DUNGEON_TP_REQUIRED).getItemName();
	
	private static String[] _voicedCommands =
	{
			"castledungeon"
	};

	/**
	 * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#useVoicedCommand(java.lang.String,
	 *      com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance, java.lang.String)
	 */
	@Override
	public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target)
	{
		if (activeChar.getInventory().getItemByItemId(Config.DUNGEON_TP_REQUIRED) == null)
		{
			activeChar.sendMessage("You need " + Config.DUNGEON_TP_PRICE + " " + ItemName +  " to be able to port inside the dungeon.");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return false;
		}
		else if (LastManStanding.players.contains(activeChar) && LastManStanding.isActive())
		{
			activeChar.sendMessage("You cannot port while registered in LMS");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return false;
		}
		else if (activeChar.getPvptoken()<Config.DUNGEON_TP_PRICE)
		{
			activeChar.sendMessage("You need " + Config.DUNGEON_TP_PRICE + " " + ItemName +  " to be able to port inside the dungeon.");
			activeChar.sendPacket(ActionFailed.STATIC_PACKET);
			return false;
		}
		else
		{
			//activeChar.teleToLocation(113680, -126107, -3490);
			activeChar.destroyItemByItemId("teleport", Config.DUNGEON_TP_REQUIRED, Config.DUNGEON_TP_PRICE, activeChar, true);
			activeChar.teleToLocation(113680, -126107, -3490, true);
		}
		return true;
	}

	/**
	 * @see com.l2jfrozen.gameserver.handler.IVoicedCommandHandler#getVoicedCommandList()
	 */
	@Override
	public String[] getVoicedCommandList()
	{
		return _voicedCommands;
	}
}
