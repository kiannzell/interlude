package com.l2jfrozen.gameserver.handler.itemhandlers;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.handler.IItemHandler;
import com.l2jfrozen.gameserver.model.L2Clan;
import com.l2jfrozen.gameserver.model.actor.instance.L2ItemInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PlayableInstance;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.PledgeShowInfoUpdate;

public class ReputationItem implements IItemHandler
{

	public ReputationItem()
	{
	//null
	}

	@Override
	public void useItem(L2PlayableInstance playable, L2ItemInstance item)
	{
			if(!(playable instanceof L2PcInstance))
				return;

			L2PcInstance activeChar = (L2PcInstance) playable;
			L2Clan clan = activeChar.getClan();
			if(activeChar.isInOlympiadMode())
			{
				activeChar.sendMessage("This Item Cannot Be Used On Olympiad Games.");
			}
			else
			{
				if (activeChar.getClan() != null && activeChar.isClanLeader())
				{
				clan.setReputationScore(clan.getReputationScore()+Config.REPUTATION_CUSTOM_POINTS, true);
				clan.broadcastToOnlineMembers(new PledgeShowInfoUpdate(clan));
				activeChar.sendMessage("Your clan received "+ Config.REPUTATION_CUSTOM_POINTS +" reputation point(s)!");
				activeChar.broadcastUserInfo();
				playable.destroyItem("Consume", item.getObjectId(), 1, null, false);
				}
				else
				{
					activeChar.sendMessage("This item can only be use by clan leaders!");
					activeChar.sendPacket(ActionFailed.STATIC_PACKET);
				}
			}
			activeChar = null;
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}

	private static final int ITEM_IDS[] =
	{
		Config.REPUTATION_CUSTOM_ITEM_ID
	};

}