/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.usercommandhandlers;

import javolution.text.TextBuilder;

import com.l2jfrozen.gameserver.handler.IUserCommandHandler;
import com.l2jfrozen.gameserver.model.L2ClanMember;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;

public class SiegeStatus implements IUserCommandHandler
{
	private static final int[] COMMAND_IDS =
	{
		99
	};
	
	private int auxDeaths, auxKills = 0;
	private String _status;
	
	@Override
	public boolean useUserCommand(int id, L2PcInstance activeChar)
	{
		if (COMMAND_IDS[0] != id)
			return false;
		
		if (!activeChar.isClanLeader())
		{
			activeChar.sendPacket(SystemMessageId.ONLY_THE_CLAN_LEADER_IS_ENABLED);
			return false;
		}
		
		if (!activeChar.isNoble())
		{
			activeChar.sendPacket(SystemMessageId.ONLY_NOBLESSE_LEADER_CAN_VIEW_SIEGE_STATUS_WINDOW);
			return false;
		}
		
		if (id == 99 && activeChar.getClan() != null && activeChar.getClan().getLeaderName() == activeChar.getName())
		{
			if (id == 99 && activeChar.getSiegeState() != 0)
			{
				L2ClanMember[] members = activeChar.getClan().getMembers();
				int membersCount = activeChar.getClan().getMembersCount();
				auxDeaths = activeChar.getClan().getSiegeKills();
				auxKills = activeChar.getClan().getSiegeDeaths();
				
				NpcHtmlMessage nhm = new NpcHtmlMessage(5);
				TextBuilder replyMSG = new TextBuilder("");
				replyMSG.append("<html><title>Siege Status Report</title>");
				replyMSG.append("<body>");
				replyMSG.append("<table width=\"100%\">");
				replyMSG.append("<tr><td>Kills: " + auxKills + "</td></tr>");
				replyMSG.append("<tr><td>Deaths: " + auxDeaths + "</td></tr>");
				replyMSG.append("</table");
				replyMSG.append("<table width=320 bgcolor=\"FFFFFF\">");
				replyMSG.append("<tr><td>Name</td>");
				replyMSG.append("<td align=\"right\">Status</td></tr></table>");
				replyMSG.append("<center><table width=\"100%\">");
				for (int i = 0; i < membersCount; i++)
				{
					if (members[i].isOnline())
					{
						if (members[i].getPlayerInstance().isInsideZone(4))
						{
							_status = "In Combat Zone";
						}
						else
						{
							_status = "Outside Combat Zone";
						}
						replyMSG.append("<tr><td>" + members[i].getPlayerInstance().getName() + "</td>");
						replyMSG.append("<td align=\"right\">" + _status + "</td></tr>");
					}
				}
				replyMSG.append("</table></center></body></html>");
				nhm.setHtml(replyMSG.toString());
				activeChar.sendPacket(nhm);
				activeChar.sendPacket(new ActionFailed());
			}
			else
			{
				activeChar.sendMessage("Siege is not currently in progress!");
			}
		}
		
		return true;
	}
	
	@Override
	public int[] getUserCommandList()
	{
		return COMMAND_IDS;
	}
}