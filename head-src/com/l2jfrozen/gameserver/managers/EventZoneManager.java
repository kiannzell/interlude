/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.managers;

import javolution.util.FastList;

import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.zone.type.L2EventZone;

/**
 * Event Zone Manager
 * 
 * @author -Sense-
 */

public class EventZoneManager
{
	// =========================================================
	private static EventZoneManager _instance;

	public static final EventZoneManager getInstance()
	{
		if(_instance == null)
		{
			System.out.println("Initializing EventZoneManager");
			_instance = new EventZoneManager();
		}
		return _instance;
	}

	// =========================================================

	// =========================================================
	// Data Field
	private FastList<L2EventZone> _event;

	// =========================================================
	// Constructor
	public EventZoneManager()
	{}

	// =========================================================
	// Property - Public

	public void addEventZone(L2EventZone event)
	{
		if(_event == null)
		{
			_event = new FastList<L2EventZone>();
		}

		_event.add(event);
	}

	public final L2EventZone getEventZone(L2Character character)
	{
		if(_event != null)
		{
			for(L2EventZone temp : _event)
				if(temp.isCharacterInZone(character))
					return temp;
		}

		return null;
	}

	public final L2EventZone getEventZone(int x, int y, int z)
	{
		if(_event != null)
		{
			for(L2EventZone temp : _event)
				if(temp.isInsideZone(x, y, z))
					return temp;
		}

		return null;
	}
}
