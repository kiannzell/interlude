/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.managers;

import javolution.util.FastList;

import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.zone.type.L2ChaoticZone;

/**
 * Chaotic Zone Manager
 * 
 * @author -Sense-
 */

public class ChaoticManager
{
	// =========================================================
	private static ChaoticManager _instance;

	public static final ChaoticManager getInstance()
	{
		if(_instance == null)
		{
			System.out.println("Initializing ChaoticManager");
			_instance = new ChaoticManager();
		}
		return _instance;
	}

	// =========================================================

	// =========================================================
	// Data Field
	private FastList<L2ChaoticZone> _chaotic;

	// =========================================================
	// Constructor
	public ChaoticManager()
	{}

	// =========================================================
	// Property - Public

	public void addChaotic(L2ChaoticZone chaotic)
	{
		if(_chaotic == null)
		{
			_chaotic = new FastList<L2ChaoticZone>();
		}

		_chaotic.add(chaotic);
	}

	public final L2ChaoticZone getChaotic(L2Character character)
	{
		if(_chaotic != null)
		{
			for(L2ChaoticZone temp : _chaotic)
				if(temp.isCharacterInZone(character))
					return temp;
		}

		return null;
	}

	public final L2ChaoticZone getChaotic(int x, int y, int z)
	{
		if(_chaotic != null)
		{
			for(L2ChaoticZone temp : _chaotic)
				if(temp.isInsideZone(x, y, z))
					return temp;
		}

		return null;
	}
}
