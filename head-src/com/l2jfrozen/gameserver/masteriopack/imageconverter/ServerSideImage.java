package com.l2jfrozen.gameserver.masteriopack.imageconverter;

/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */


import java.io.File;









import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.masteriopack.rankpvpsystem.Rank;
import com.l2jfrozen.gameserver.masteriopack.rankpvpsystem.RankPvpSystemConfig;
import com.l2jfrozen.gameserver.masteriopack.rankpvpsystem.RankTable;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

import javolution.text.TextBuilder;
import javolution.util.FastMap;
import javolution.util.FastMap.Entry;

/**
 * @author Masterio
 *
 */
public class ServerSideImage
{

	private static ServerSideImage _instance = null;
	
	/** <rank_id, converted image data as byte array> */
	private FastMap<Integer, byte[]> _iconImages = new FastMap<Integer, byte[]>();
	private FastMap<Integer, byte[]> _nameImages = new FastMap<Integer, byte[]>();
	private FastMap<Integer, byte[]> _expImages = new FastMap<Integer, byte[]>();
	
	public final int IMG_PREFIX = RankPvpSystemConfig.IMAGE_PREFIX;
	
	
	private ServerSideImage()
	{

		load();

		System.out.println(" - IconImages loaded "+(getIconImages().size())+" objects.");
		System.out.println(" - NameImages loaded "+(getNameImages().size())+" objects.");
		System.out.println(" - ExpImages loaded "+(getExpImages().size())+" objects.");
		
	}
	
	public static ServerSideImage getInstance()
	{
		if(_instance == null)
		{
			_instance = new ServerSideImage();
		}
		
		return _instance;
	}
	
	public void load()
	{
		FastMap<Integer, Rank> rankList = RankTable.getInstance().getRankList();
		
		for (Entry<Integer, Rank> e = rankList.head(), end = rankList.tail(); (e = e.getNext()) != end;) 
		{
			
			String src = null;
			File image = null;
			
			try
			{
				
				// set icon images:
				src = "rank_pvp_system/rank/rank_"+e.getValue().getId();
				image = new File("data/images/"+src+".png");
				getIconImages().put(e.getValue().getId(), DDSConverter.convertToDDS(image).array());
				
				// set name images:
				src = "rank_pvp_system/rank_name/rank_name_"+e.getValue().getId();
				image = new File("data/images/"+src+".png");
				getNameImages().put(e.getValue().getId(), DDSConverter.convertToDDS(image).array());
				
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
				return;
			}
			
		}
		
		for (int i=0; i<=100; i++)
		{
			
			String src = null;
			File image = null;
			
			try
			{
				
				// set exp images:
				src = "rank_pvp_system/exp/exp_"+i;
				image = new File("data/images/"+src+".png");
				getExpImages().put(i, DDSConverter.convertToDDS(image).array());
				
			}
			catch(Exception e1)
			{
				e1.printStackTrace();
				return;
			}
		}
		
	}
	
	public TextBuilder getExpImageHtmlTag(L2PcInstance player, int expId, int width, int height)
	{
		
		TextBuilder tb = new TextBuilder();
		try
		{

			int id = 400000000 + expId + (IMG_PREFIX * 200);
			ImagePacketServer packet = new ImagePacketServer(id, getExpImages().get(expId));
			player.sendPacket(packet);

			tb.append("<img src=\"Crest.crest_" + Config.SERVER_ID + "_" + id + "\" width="+width+" height="+height+">");
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return tb;
		
	}
	
	public TextBuilder getRankIconImageHtmlTag(L2PcInstance player, int rankId, int width, int height){
		
		TextBuilder tb = new TextBuilder();
		try
		{

			int id = 500000000 + rankId + (IMG_PREFIX * 200);
			ImagePacketServer packet = new ImagePacketServer(id, getIconImages().get(rankId));
			player.sendPacket(packet);

			tb.append("<img src=\"Crest.crest_" + Config.SERVER_ID + "_" + id + "\" width="+width+" height="+height+">");
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return tb;
		
	}
	
	public TextBuilder getRankNameImageHtmlTag(L2PcInstance player, int rankId, int width, int height)
	{
		
		TextBuilder tb = new TextBuilder();
		try
		{

			int id = 600000000 + rankId + (IMG_PREFIX * 200);
			ImagePacketServer packet = new ImagePacketServer(id, getNameImages().get(rankId));
			player.sendPacket(packet);

			tb.append("<img src=\"Crest.crest_" + Config.SERVER_ID + "_" + id + "\" width="+width+" height="+height+">");
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		return tb;
		
	}

	public FastMap<Integer, byte[]> getIconImages() 
	{
		return _iconImages;
	}

	public void setIconImages(FastMap<Integer, byte[]> _iconImages)
	{
		this._iconImages = _iconImages;
	}

	public FastMap<Integer, byte[]> getNameImages()
	{
		return _nameImages;
	}

	public void setNameImages(FastMap<Integer, byte[]> _nameImages) 
	{
		this._nameImages = _nameImages;
	}

	public FastMap<Integer, byte[]> getExpImages()
	{
		return _expImages;
	}

	public void setExpImages(FastMap<Integer, byte[]> _expImages)
	{
		this._expImages = _expImages;
	}

}
