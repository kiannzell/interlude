package com.l2jfrozen.gameserver.masteriopack.imageconverter;

import com.l2jfrozen.gameserver.network.serverpackets.L2GameServerPacket;






/**
 * Packet used as PledgeCrest packet.
 * @author Masterio
 *
 */
public class ImagePacketServer extends L2GameServerPacket
{
	private final int _crestId;
	private final byte[] _data;
	
	public ImagePacketServer(int crestId, byte[] data)
	{
		_crestId = crestId;
		_data = data;
	}
	
	@Override
	protected final void writeImpl()
	{
		writeC(0x6c); // IL client: 0x6c, H5 client: 0x6a
		writeD(_crestId);
		if (_data != null)
		{
			writeD(_data.length);
			writeB(_data);
		}
		else
		{
			writeD(0);
		}
	}

	@Override
	public String getType()
	{
		return null;
	}
}
