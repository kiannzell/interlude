package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;

import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;


/**
 * 
 * @author Masterio
 *
 */
public class RankPvpSystemBypass 
{
	public static void execute(L2PcInstance activeChar, String command)
	{
		/*
		 * Commands:
		 * 
		 * RPS.PS 									- PvP Status window.
		 * RPS.DS									- Death Status window.
		 * RPS.RPC:<pageNo>							- RPS Exchange window (default page number is: 1).
		 * RPS.RPCReward:<rewardId>,<pageNo>		- Give RPC Reward for current player, and show RPC Exchange on pageNo. [The reward list is constant]
		 * 
		 */

		String param = command.split("\\.", 2)[1];
		
		RankPvpSystemPc pc = activeChar._rankPvpSystemPc;
		
		// reset death status:
		if(!activeChar.isDead())
			pc.setRankPvpSystemDeathStatus(null);
		
		if(param.equals("PS")) // PvP Status
		{	
			// get killer as target for Death Status window:
			if(pc.isDeathStatusWindowActive() && pc.getRankPvpSystemDeathStatus().getKiller() != null)
			{
				RankPvpSystemPvpStatus.sendPlayerResponse(activeChar, pc.getRankPvpSystemDeathStatus().getKiller());
			}
			else // otherwise get target when command was used:
			{
				RankPvpSystemPvpStatus.sendPlayerResponse(activeChar,pc.getTempTarget());
			}			
		}
		else if(param.equals("DS")) // Death Status
		{
			if(pc.isDeathStatusWindowActive() &&  pc.getRankPvpSystemDeathStatus().getKiller() != null)
			{
				 pc.getRankPvpSystemDeathStatus().sendVictimResponse();
			}
			else
			{
				activeChar.sendMessage("You can see Death Status only when you are dead!");
			}
		}
		else if(param.startsWith("RPC:"))
		{
			String param2 = command.split(":", 2)[1].trim();
			
			int pageNo = 1;
			
			try
			{
				pageNo = Integer.valueOf(param2);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			
			if(pageNo < 1)
			{
				pageNo = 1;
			}
			
			RankPvpSystemRPC.sendPlayerResponse(activeChar, pageNo);
		}
		else if(param.startsWith("RPCReward:"))
		{
			try
			{
				int rpcRewardId = Integer.valueOf(command.split(":", 2)[1].trim().split(",", 2)[0].trim());
				int pageNo = Integer.valueOf(command.split(":", 2)[1].trim().split(",", 2)[1].trim());
				
				RPCReward rpcReward = RPCRewardTable.getInstance().getRpcRewardList().get(rpcRewardId);
				
				RPCRewardTable.getInstance().giveReward(activeChar, rpcReward);
				
				RankPvpSystemRPC.sendPlayerResponse(activeChar, pageNo);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}
	
	
}
