package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;



/**
 * 
 * @author Masterio
 *
 */
public class RPCReward 
{
	private int _id = 0;
	private int _itemId = 0;
	private long _itemAmount = 0;
	private long _rpc = 0; // how much RPC for (item x amount)
	
	public int getId()
	{
		return _id;
	}

	public void setId(int _id) 
	{
		this._id = _id;
	}

	public int getItemId() 
	{
		return _itemId;
	}
	
	public void setItemId(int _itemId) 
	{
		this._itemId = _itemId;
	}
	
	public long getItemAmount()
	{
		return _itemAmount;
	}
	
	public void setItemAmount(long _itemAmount)
	{
		this._itemAmount = _itemAmount;
	}
	
	public long getRpc()
	{
		return _rpc;
	}
	
	public void setRpc(long _rpc)
	{
		this._rpc = _rpc;
	}
}
