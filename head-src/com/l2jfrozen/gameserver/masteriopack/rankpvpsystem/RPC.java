package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;



/**
 * 
 * @author Masterio
 *
 */
public class RPC 
{
	private int _playerId = 0;
	private long _rpcTotal = 0;
	private long _rpcCurrent = 0;

	private byte _dbStatus = DBStatus.NONE;
	
	public RPC()
	{
		
	}
	
	public RPC(int playerId)
	{
		_playerId = playerId;
	}
	
	public int getPlayerId()
	{
		return _playerId;
	}
	
	public void setPlayerId(int _playerId)
	{
		this._playerId = _playerId;
	}
	
	public long getRpcTotal()
	{
		return _rpcTotal;
	}
	
	public void setRpcTotal(long _rpcTotal) 
	{
		this._rpcTotal = _rpcTotal;
	}
	
	public long getRpcCurrent()
	{
		return _rpcCurrent;
	}
	
	public void setRpcCurrent(long _rpcCurrent) 
	{
		this._rpcCurrent = _rpcCurrent;
	}
	
	/**
	 * Decrease RPC, and update DBStatus into updated, or leave inserted and return RpcCurrent.
	 * @param decreaseValue
	 * @return 
	 */
	public long decreaseRpcCurrentBy(long decreaseValue)
	{
		_rpcCurrent -= decreaseValue;
		
		if(getDbStatus() != DBStatus.INSERTED)
		{
			setDbStatus(DBStatus.UPDATED);
		}
		
		return _rpcCurrent;
	}
	
	public void increaseRpcTotalAndCurrentBy(long increaseValue)
	{
		_rpcCurrent += increaseValue;
		_rpcTotal += increaseValue;
		
		if(getDbStatus() != DBStatus.INSERTED)
		{
			setDbStatus(DBStatus.UPDATED);
		}
	}

	public byte getDbStatus()
	{
		return _dbStatus;
	}

	/**
	 * DBStatus class as parameter.
	 * @param _dbStatus
	 */
	public void setDbStatus(byte _dbStatus)
	{
		this._dbStatus = _dbStatus;
	}
	
}
