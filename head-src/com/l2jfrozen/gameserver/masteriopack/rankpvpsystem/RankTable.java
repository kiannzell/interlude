package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;



import javolution.util.FastMap;

public class RankTable 
{
	private static RankTable _instance = null;
	
	/** FastMap &lt;rankId, Rank&gt; - store all Ranks as Rank objects. */
	private static FastMap<Integer, Rank> _rankList = new FastMap<Integer, Rank>();

	private RankTable()
	{

	}
	
	public static RankTable getInstance()
	{
		if(_instance == null)
		{
			_instance = new RankTable();
		}
		
		return _instance;
	}
	
	public FastMap<Integer, Rank> getRankList()
	{
		return _rankList;
	}

	public void setRankList(FastMap<Integer, Rank> _rankList)
	{
		RankTable._rankList = _rankList;
	}
	
	/**
	 * Returns Rank object by rank id, if not founded returns null.
	 * @param id
	 * @return
	 */
	public Rank getRankById(int id)
	{
		return _rankList.get(id);
	}

}
