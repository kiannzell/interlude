package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;



import javolution.util.FastMap;

public class KillerPvpStats
{
	private int _characterId = 0;
	
	private int _totalKills = 0;
	private int _totalKillsToday = 0;
	private int _totalKillsLegal = 0;
	private int _totalKillsLegalToday = 0;
	private long _totalRankPoints = 0;
	private long _totalRankPointsToday = 0;
	private int _totalWarKills = 0;
	private int _totalWarKillsLegal = 0;

	private int _rankId = 0;
	
	/** <victim_id, PvP> contains killer pvp's data (victim, kills on victim, last kill time, etc.) */
	private FastMap<Integer, Pvp> _victimPvpTable = new FastMap<Integer, Pvp>();
	
	private long _lastKillTime = 0; // store last <legal> kill time
	
	public void addTotalKills(int kills)
	{
		_totalKills += kills;
	}
	
	public void addTotalKillsToday(int killsToday)
	{
		_totalKillsToday += killsToday;
	}
	
	public void addTotalKillsLegal(int killsLegal)
	{
		_totalKillsLegal += killsLegal;
	}
	
	public void addTotalKillsLegalToday(int killsLegalToday)
	{
		_totalKillsLegalToday += killsLegalToday;
	}
	
	/** Add Rank Points to Total Rank Points and update Rank. <br>
	 * Is NOT recommended use this method in LOOP, because this method use: <b>onUpdateRankPoints()</b>.
	 * @param rankPoints */
	public void addTotalRankPoints(long rankPoints)
	{
		_totalRankPoints += rankPoints;
		onUpdateRankPoints();
	}
	
	/** Add Rank Points to Total Rank Points and <b>NOT</b> update Rank. <br>
	 * After finish adding points, you should execute this method: <b>onUpdateRankPoints();</b> 
	 * @param rankPoints */
	public void addTotalRankPointsOnly(long rankPoints)
	{
		_totalRankPoints += rankPoints;
	}
	
	public void addTotalRankPointsToday(long rankPointsToday)
	{
		_totalRankPointsToday += rankPointsToday;
	}
	
	public void addTotalWarKills(int warKills)
	{
		_totalWarKills += warKills;
	}
	
	public void addTotalWarKillsLegal(int warKillsLegal)
	{
		_totalWarKillsLegal += warKillsLegal;
	}

	/**
	 * @return the _characterId
	 */
	public int getCharacterId()
	{
		return _characterId;
	}
	/**
	 * @param _characterId the _characterId to set
	 */
	public void setCharacterId(int _characterId)
	{
		this._characterId = _characterId;
	}
	/**
	 * @return the _totalKills
	 */
	public int getTotalKills()
	{
		return _totalKills;
	}
	/**
	 * @param _totalKills the _totalKills to set
	 */
	public void setTotalKills(int _totalKills)
	{
		this._totalKills = _totalKills;
	}
	/**
	 * @return the _totalKillsToday
	 */
	public int getTotalKillsToday()
	{
		return _totalKillsToday;
	}
	/**
	 * @param _totalKillsToday the _totalKillsToday to set
	 */
	public void setTotalKillsToday(int _totalKillsToday)
	{
		this._totalKillsToday = _totalKillsToday;
	}
	/**
	 * @return the _totalKillsLegal
	 */
	public int getTotalKillsLegal()
	{
		return _totalKillsLegal;
	}
	/**
	 * @param _totalKillsLegal the _totalKillsLegal to set
	 */
	public void setTotalKillsLegal(int _totalKillsLegal)
	{
		this._totalKillsLegal = _totalKillsLegal;
	}
	/**
	 * @return the _totalKillsLegalToday
	 */
	public int getTotalKillsLegalToday()
	{
		return _totalKillsLegalToday;
	}
	/**
	 * @param _totalKillsLegalToday the _totalKillsLegalToday to set
	 */
	public void setTotalKillsLegalToday(int _totalKillsLegalToday)
	{
		this._totalKillsLegalToday = _totalKillsLegalToday;
	}
	/**
	 * @return the _totalRankPoints
	 */
	public long getTotalRankPoints()
	{
		return _totalRankPoints;
	}
	/**
	 * Set Total Rank Points and update Rank.<br>
	 * Is NOT recommended use this method in LOOP, because this method use: <b>onUpdateRankPoints()</b>.
	 * @param _totalRankPoints the _totalRankPoints to set
	 */
	public void setTotalRankPoints(long _totalRankPoints)
	{
		this._totalRankPoints = _totalRankPoints;
		onUpdateRankPoints();
	}
	/**
	 * Set Total Rank Points and update Rank.<br>
	 * It is not update Rank!
	 * @param _totalRankPoints
	 */
	public void setTotalRankPointsOnly(long _totalRankPoints)
	{
		this._totalRankPoints = _totalRankPoints;
	}
	/**
	 * @return the _totalRankPointsToday
	 */
	public long getTotalRankPointsToday()
	{
		return _totalRankPointsToday;
	}
	/**
	 * @param _totalRankPointsToday the _totalRankPointsToday to set
	 */
	public void setTotalRankPointsToday(long _totalRankPointsToday)
	{
		this._totalRankPointsToday = _totalRankPointsToday;
	}
	/**
	 * @return the _totalWarKills
	 */
	public int getTotalWarKills()
	{
		return _totalWarKills;
	}
	/**
	 * @param _totalWarKills the _totalWarKills to set
	 */
	public void setTotalWarKills(int _totalWarKills)
	{
		this._totalWarKills = _totalWarKills;
	}
	/**
	 * @return the _totalWarKillsLegal
	 */
	public int getTotalWarKillsLegal()
	{
		return _totalWarKillsLegal;
	}
	/**
	 * @param _totalWarKillsLegal the _totalWarKillsLegal to set
	 */
	public void setTotalWarKillsLegal(int _totalWarKillsLegal)
	{
		this._totalWarKillsLegal = _totalWarKillsLegal;
	}

	/**
	 * @return the _rank
	 */
	public int getRankId()
	{
		return _rankId;
	}
	
	public Rank getRank()
	{
		return RankTable.getInstance().getRankById(getRankId());
	}

	/**
	 * @param _rankId the _rank to set
	 */
	public void setRankId(int _rankId)
	{
		this._rankId = _rankId;
	}
	
	/** Update current Rank values for this character,<br>should be executed always when total rank points was updated. */
	public void onUpdateRankPoints()
	{
		FastMap<Integer, Rank> list = RankTable.getInstance().getRankList();
		
		for (FastMap.Entry<Integer, Rank> e = list.head(), end = list.tail(); (e = e.getNext()) != end;) 
		{
			// set up rank for current rank points:
			if(e.getValue().getMinPoints() <= getTotalRankPoints())
			{
				setRankId(e.getValue().getId());
				return;
			}
	    }
		
		// if not set any rank, set minimum rank:
		if(list.tail() != null)
		{
			setRankId(list.tail().getValue().getId());
		}
	}

	/** 
	 * Updates daily fields like: total kills today, etc.<br> 
	 * Other fields are actual (updated on each PvP).
	 * @param systemDay 
	 * */
	public void updateDailyStats(long systemDay)
	{
		int totalKillsToday = 0;
		int totalKillsLegalToday = 0;
		long totalRankPointsToday = 0;
		
		for (FastMap.Entry<Integer, Pvp> e = getVictimPvpTable().head(), end = getVictimPvpTable().tail(); (e = e.getNext()) != end;) 
		{
			Pvp pvp = e.getValue();

			if(e.getValue().getKillDay() == systemDay)
			{
				totalKillsToday += pvp.getKillsToday();
				totalKillsLegalToday += pvp.getKillsLegalToday();
				totalRankPointsToday += e.getValue().getRankPointsToday();
				
			}
			else
			{
				e.getValue().resetDailyFields(); //TODO probably not required here.
			}
	    }
		
		setTotalKillsToday(totalKillsToday);
		setTotalKillsLegalToday(totalKillsLegalToday);
		setTotalRankPointsToday(totalRankPointsToday);

	}

	public FastMap<Integer, Pvp> getVictimPvpTable()
	{
		return _victimPvpTable;
	}

	public void setVictimPvpTable(FastMap<Integer, Pvp> _victimPvpTable)
	{
		this._victimPvpTable = _victimPvpTable;
	}
	
	/**
	 * Add pvp into victimPvpTable and updates killerPvpStats.<br>
	 * Used on initialization.
	 * @param pvp
	 * @return 
	 */
	public boolean addVictimPvp(Pvp pvp)
	{
		if(!getVictimPvpTable().containsKey(pvp.getVictimObjId()))
		{
			// add pvp:
			getVictimPvpTable().put(pvp.getVictimObjId(), pvp);
			
			// update killer pvp stats:
			addTotalKills(pvp.getKills());
			addTotalKillsToday(pvp.getKillsToday());
			addTotalKillsLegal(pvp.getKillsLegal());
			addTotalKillsLegalToday(pvp.getKillsLegalToday());
			
			addTotalRankPoints(pvp.getRankPoints());
			addTotalRankPointsToday(pvp.getRankPointsToday());
			addTotalWarKills(pvp.getWarKills());
			addTotalWarKillsLegal(pvp.getWarKillsLegal());
			
			// set last kill time:
			if(pvp.getKillTime() > getLastKillTime())
			{
				setLastKillTime(pvp.getKillTime());
			}
			
			return true;
		}
		
		return false;
	}

	public long getLastKillTime()
	{
		return _lastKillTime;
	}

	public void setLastKillTime(long _lastKillTime) 
	{
		this._lastKillTime = _lastKillTime;
	}
}
