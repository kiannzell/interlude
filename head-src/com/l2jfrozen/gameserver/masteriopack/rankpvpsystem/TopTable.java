package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;

/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;






import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.util.database.L2DatabaseFactory;

import javolution.util.FastMap;

/**
 * @author Masterio
 */
public class TopTable
{
	private static final String SQL_PLAYER_DATA = "SELECT char_name as name, base_class as base_class, level as level FROM characters WHERE obj_Id=?";
	
	public static final int TOP_LIMIT = 100; // limit for top list players data.
	
	private static TopTable _instance = null;
	
	private boolean _isUpdating = false;
	
	/** <PlayerId, TopField> contains top list. <br>List is ordered from 1st position to last. */
	private FastMap<Integer, TopField> _topKillsTable = new FastMap<Integer, TopField>();
	/** <PlayerId, TopField> contains top list. <br>List is ordered from 1st position to last. */
	private FastMap<Integer, TopField> _topGatherersTable = new FastMap<Integer, TopField>();
	
	private static long _lastUpdateTime = 0;

	private TopTable()
	{
		if(RankPvpSystemConfig.TOP_LIST_ENABLED)
		{
			load();
		}
	}
	
	public static TopTable getInstance()
	{
		if(_instance == null)
		{
			_instance = new TopTable();
		}
		
		return _instance;
	}

	private void load()
	{
		Calendar c = Calendar.getInstance();	
		//long startTime = c.getTimeInMillis();
		
		//String info = "loaded";
	
		// initialize _updateTime:
		Calendar _updateTime = Calendar.getInstance();
		_updateTime.set(Calendar.HOUR_OF_DAY, RankPvpSystemConfig.TOP_TABLE_UPDATE_HOUR);
		_updateTime.set(Calendar.MINUTE, RankPvpSystemConfig.TOP_TABLE_UPDATE_MINUTE);
		_updateTime.set(Calendar.SECOND, 1);
		_updateTime.set(Calendar.MILLISECOND, 0);
		
		// initialize _nextUpdateTime:
		Calendar _nextUpdateTime = Calendar.getInstance();
		_nextUpdateTime.add(Calendar.DAY_OF_YEAR, 1);
		_nextUpdateTime.set(Calendar.HOUR_OF_DAY, RankPvpSystemConfig.TOP_TABLE_UPDATE_HOUR);
		_nextUpdateTime.set(Calendar.MINUTE, RankPvpSystemConfig.TOP_TABLE_UPDATE_MINUTE);
		_nextUpdateTime.set(Calendar.SECOND, 1);
		_nextUpdateTime.set(Calendar.MILLISECOND, 0);
		
		// calculate time to run next update:
		long initialTime = _nextUpdateTime.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
		
		// initialize _lastUpdateTime: 
		loadLastUpdate();
		
		// initialize Top Tables:
		// if last update time < today's update time
		if(_lastUpdateTime < _updateTime.getTimeInMillis())
		{
			updateTopTable();
			//info = "updated";
		}
		else
		{
			restoreTopTable();
		}
		
		// update every 24h, delayed by 24h because on server start is checked first update:
		ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new TopTableSchedule(), initialTime, 86400000); // 86400000 = 24h
		
		// TEST MODE:
		//ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(new TopTableSchedule(), 180000, 120000); // 86400000 = 24h
		
		c = Calendar.getInstance();
		@SuppressWarnings("unused")
		long endTime = c.getTimeInMillis();
		
		//System.out.println(" - TopTable "+info+" "+getTopKillsTable().size()+" TopKillers, "+getTopGatherersTable().size()+" TopGatherers in "+(endTime-startTime)+" ms.");
	
	}
	
	
	/**
	 * Update top table and save results in rank_pvp_system_top table.
	 * @return 
	 */
	boolean updateTopTable()
	{
		boolean ok = false;
		
		// lock table:
		setUpdating(true);
		
		// clear tables:
		getTopKillsTable().clear();
		getTopGatherersTable().clear();
		
		// get minimum allowed time:
		long sysTime = Calendar.getInstance().getTimeInMillis() - RankPvpSystemConfig.TOP_LIST_IGNORE_TIME_LIMIT;

		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		
		// order and load top killers & gatherers from model:
		FastMap<Integer, KillerPvpStats> pvpList = PvpTable.getInstance().getKillerPvpTable();
		
		FastMap<Integer, TopField> tmpTopKillsTable = new FastMap<Integer, TopField>();
		FastMap<Integer, TopField> tmpTopGatherersTable = new FastMap<Integer, TopField>();
		
		int KillPosition = 0;
		int PointPosition = 0;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			
			// for TopKillersTable:
			for(int i=1; i<=TOP_LIMIT; i++)
			{
				int maxKills = 0;
				int bestKiller = 0;
				
				long maxPoints = 0;
				int bestGatherer = 0;
				
				for (FastMap.Entry<Integer, KillerPvpStats> e = pvpList.head(), end =  pvpList.tail(); (e = e.getNext()) != end;)
				{
					KillerPvpStats kps = e.getValue();
					
					if(RankPvpSystemConfig.TOP_LIST_IGNORE_TIME_LIMIT == 0 || kps.getLastKillTime() >= sysTime) // if last kill is in TOP_LIST_IGNORE_TIME_LIMIT.
					{
						if(!tmpTopKillsTable.containsKey(e.getKey())) // don't check already added killers.
						{
							if(kps.getTotalKillsLegal() > maxKills && kps.getTotalKillsLegal() > 0) // finding the best.
							{
								maxKills = kps.getTotalKillsLegal();
								bestKiller = kps.getCharacterId();
							}
						}
						
						if(!tmpTopGatherersTable.containsKey(e.getKey())) // don't check already added gatherers.
						{
							if(kps.getTotalRankPoints() > maxPoints && kps.getTotalRankPoints() > 0) // finding the best.
							{
								maxPoints = kps.getTotalRankPoints();
								bestGatherer = kps.getCharacterId();
							}				
						}
					}
				}
			
				// if killer found:
				if(bestKiller != 0)
				{
					KillPosition++;
					
					// if founded, add him to list:
					TopField tf = new TopField();
					
					tf.setCharacterId(bestKiller);
					tf.setCharacterPoints(maxKills);
					tf.setTopPosition(KillPosition);
					
					// get character data:
					statement = con.prepareStatement(SQL_PLAYER_DATA);
					statement.setInt(1, bestKiller);
					
					rset = statement.executeQuery();
					
					while(rset.next())
					{
						tf.setCharacterName(rset.getString("name"));
						tf.setCharacterLevel(rset.getInt("level"));
						tf.setCharacterBaseClassId(rset.getInt("base_class"));
					}
					
					rset.close();

					// add this killer on temporary top list:
					tmpTopKillsTable.put(bestKiller, tf);
				}
				
				// if any best killer not found, break action:
				if(bestGatherer != 0)
				{
					PointPosition++;
					
					// if founded, add him to list:
					TopField tf = new TopField();
					
					tf.setCharacterId(bestGatherer);
					tf.setCharacterPoints(maxPoints);
					tf.setTopPosition(PointPosition);
					
					// get character data:
					statement = con.prepareStatement(SQL_PLAYER_DATA);
					statement.setInt(1, bestGatherer);
					
					rset = statement.executeQuery();
					
					while(rset.next())
					{
						tf.setCharacterName(rset.getString("name"));
						tf.setCharacterLevel(rset.getInt("level"));
						tf.setCharacterBaseClassId(rset.getInt("base_class"));
					}
					
					rset.close();
	
					// add this gatherer on top list:
					tmpTopGatherersTable.put(bestGatherer, tf);
				}
				
			}
			
			// TODO dodac sortowanie tablic tmp , a do piero potem je przypisywac.
			// add new top tables:
			setTopKillsTable(tmpTopKillsTable);
			setTopGatherersTable(tmpTopGatherersTable);
		
			
			if(statement == null)
			{
				statement = con.prepareStatement("");
			}
			
			// clear Top Killer table:
			statement.addBatch("DELETE FROM rank_pvp_system_top_killer");
			
			// search new or updated fields in PvpTable:
			for (FastMap.Entry<Integer, TopField> e = getTopKillsTable().head(), end = getTopKillsTable().tail(); (e = e.getNext()) != end;)
			{
				statement.addBatch("INSERT INTO rank_pvp_system_top_killer (position, player_id, kills) VALUES ("+e.getValue().getTopPosition()+","+e.getValue().getCharacterId()+","+e.getValue().getCharacterPoints()+")");
			}

			statement.executeBatch();

			
			
			// clear Top Gatherers table:
			statement.addBatch("DELETE FROM rank_pvp_system_top_gatherer");
			
			// search new or updated fields in PvpTable:
			for (FastMap.Entry<Integer, TopField> e = getTopGatherersTable().head(), end =  getTopGatherersTable().tail(); (e = e.getNext()) != end;)
			{
				statement.addBatch("INSERT INTO rank_pvp_system_top_gatherer (position, player_id, points) VALUES ("+e.getValue().getTopPosition()+","+e.getValue().getCharacterId()+","+e.getValue().getCharacterPoints()+")");
			}

			statement.executeBatch();

			
			// save time of update in rank_pvp_system_options table:
			long calendar = Calendar.getInstance().getTimeInMillis();
			statement = con.prepareStatement("UPDATE rank_pvp_system_options SET option_value_long=? WHERE option_id=1");
			statement.setLong(1, calendar);
			
			statement.execute();
			
			statement.close();
			
			_lastUpdateTime = calendar;
			
			ok = true;
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			
			// clear tables:
			getTopKillsTable().clear();
			getTopGatherersTable().clear();
			
			ok = false;
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
					con = null;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		

		// unlock table:
		setUpdating(false);
		return ok;
		
	}
	
	/**
	 * Load Top Table from rank_pvp_system_top table.
	 * Used only on server start.
	 */
	private void restoreTopTable()
	{

		// clear tables:
		getTopKillsTable().clear();
		getTopGatherersTable().clear();
		
		// load Tables:
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			
			// delete database from useless records:
			statement = con.prepareStatement("DELETE FROM rank_pvp_system WHERE kills = 0");
			statement.execute();

			// get top killers:
			statement = con.prepareStatement("SELECT position, player_id, kills FROM rank_pvp_system_top_killer ORDER BY position");

			rset = statement.executeQuery();

			while(rset.next())
			{
				
				TopField tf = new TopField();
					tf.setCharacterId(rset.getInt("player_id"));
					tf.setCharacterPoints(rset.getLong("kills"));
					tf.setTopPosition(rset.getInt("position"));
					
					// get character data:
					PreparedStatement statement2 = con.prepareStatement(SQL_PLAYER_DATA);
					statement2.setInt(1, rset.getInt("player_id"));
					
					ResultSet rset2 = statement2.executeQuery();
					
					while(rset2.next())
					{
						tf.setCharacterName(rset2.getString("name"));
						tf.setCharacterLevel(rset2.getInt("level"));
						tf.setCharacterBaseClassId(rset2.getInt("base_class"));
					}
					
				// get killer pvp stats:
				getTopKillsTable().put(rset.getInt("player_id"), tf);

			}
			
			rset.close();


			// get top point gatherers:
			statement = con.prepareStatement("SELECT position, player_id, points FROM rank_pvp_system_top_gatherer ORDER BY position");

			rset = statement.executeQuery();

			while(rset.next())
			{
				
				TopField tf = new TopField();
					tf.setCharacterId(rset.getInt("player_id"));
					tf.setCharacterPoints(rset.getLong("points"));
					tf.setTopPosition(rset.getInt("position"));
					
					// get character data:
					PreparedStatement statement2 = con.prepareStatement(SQL_PLAYER_DATA);
					statement2.setInt(1, rset.getInt("player_id"));
					
					ResultSet rset2 = statement2.executeQuery();
					
					while(rset2.next())
					{
						tf.setCharacterName(rset2.getString("name"));
						tf.setCharacterLevel(rset2.getInt("level"));
						tf.setCharacterBaseClassId(rset2.getInt("base_class"));
					}
					
				// get killer pvp stats:
				getTopGatherersTable().put(rset.getInt("player_id"), tf);

			}
			
			rset.close();
			
			statement.close();
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
					con = null;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	private void loadLastUpdate()
	{
		Connection con = null;
		PreparedStatement statement = null;
		ResultSet rset = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();

			// get top killers:
			statement = con.prepareStatement("SELECT option_value_long FROM rank_pvp_system_options WHERE option_id=1");

			rset = statement.executeQuery();

			while(rset.next())
			{
				_lastUpdateTime = (rset.getLong("option_value_long"));
			}
			
			rset.close();
			statement.close();
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
					con = null;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	/**
	 * List is ordered from 1st position to last.
	 * @return the _topKillsTable
	 */
	public FastMap<Integer, TopField> getTopKillsTable()
	{
		return _topKillsTable;
	}

	/**
	 * @param _topKillsTable the _topKillsTable to set
	 */
	public void setTopKillsTable(FastMap<Integer, TopField> _topKillsTable)
	{
		this._topKillsTable = _topKillsTable;
	}

	/**
	 * List is ordered from 1st position to last.
	 * @return the _topGatherersTable
	 */
	public FastMap<Integer, TopField> getTopGatherersTable()
	{
		return _topGatherersTable;
	}

	/**
	 * @param _topGatherersTable the _topGatherersTable to set
	 */
	public void setTopGatherersTable(FastMap<Integer, TopField> _topGatherersTable)
	{
		this._topGatherersTable = _topGatherersTable;
	}

	/**
	 * @return the _isUpdating
	 */
	public boolean isUpdating()
	{
		return _isUpdating;
	}

	/**
	 * @param _isUpdating the _isUpdating to set
	 */
	public void setUpdating(boolean _isUpdating)
	{
		this._isUpdating = _isUpdating;
	}

	public static long getLastUpdateTime() 
	{
		return _lastUpdateTime;
	}

	public static void setLastUpdateTime(long _lastUpdateTime) 
	{
		TopTable._lastUpdateTime = _lastUpdateTime;
	}
	
	private static class TopTableSchedule implements Runnable
	{
		public TopTableSchedule()
		{
			
		}

		@Override
		public void run()
		{
			if(!TopTable.getInstance().isUpdating())
			{
				long sysTimeO = Calendar.getInstance().getTimeInMillis();
				
				if(TopTable.getInstance().updateTopTable())
				{
					System.out.println("TopTable updated successfuly. ("+(Calendar.getInstance().getTimeInMillis() - sysTimeO)+" ms) <<<");
				}
				else
				{
					System.out.println("TopTable update failed! <<<");
				}	
			}
			else
			{
				ThreadPoolManager.getInstance().scheduleGeneral(new TopTableSchedule() , 60000);
			}
		}
	}
}