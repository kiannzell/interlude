package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;

import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;





/**
 * Class used in L2PcInstance. Contains some system variables used in game.
 * @author Masterio
 *
 */
public class RankPvpSystemPc 
{
	private RankPvpSystemDeathStatus _rankPvpSystemDeathStatus = null;
	
	private L2PcInstance _tempTarget = null;
	
	public void runPvpTask(L2PcInstance player, L2Character target)
	{
		if(RankPvpSystemConfig.RANK_PVP_SYSTEM_ENABLED)
		{
			(new Thread(new RankPvpSystemPvpTask(player, target))).start();
		}
	}

	public class RankPvpSystemPvpTask implements Runnable
	{
		private L2PcInstance _killer = null;

		private L2Character _victim = null;
		
		public RankPvpSystemPvpTask(L2PcInstance killer, L2Character victim)
		{
			this._killer = killer;
			this._victim = victim;
		}
		
		@Override
		public void run() 
		{
			
			RankPvpSystem rps = new RankPvpSystem(_killer, _victim);
				
			rps.doPvp();

		}
	}

	public RankPvpSystemDeathStatus getRankPvpSystemDeathStatus()
	{
		return _rankPvpSystemDeathStatus;
	}
	
	public boolean isDeathStatusWindowActive()
	{
		if(_rankPvpSystemDeathStatus == null)
			return false;
		
		return true;
	}

	public void setRankPvpSystemDeathStatus(RankPvpSystemDeathStatus _rankPvpSystemDeathStatus)
	{
		this._rankPvpSystemDeathStatus = _rankPvpSystemDeathStatus;
	}

	/**
	 * Target when command .pvpinfo executed, used only for PvP Status window.
	 * @return
	 */
	public L2PcInstance getTempTarget() 
	{
		return _tempTarget;
	}

	/** 
	 * Target when command .pvpinfo executed, used only for PvP Status window.
	 * @param _tempTarget
	 */
	public void setTempTarget(L2PcInstance _tempTarget) 
	{
		this._tempTarget = _tempTarget;
	}
}
