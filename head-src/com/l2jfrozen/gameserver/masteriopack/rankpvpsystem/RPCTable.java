package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;




import com.l2jfrozen.util.database.L2DatabaseFactory;

import javolution.util.FastMap;

/**
 * 
 * @author Masterio
 *
 */
public class RPCTable 
{
	private static RPCTable _instance = null;
	
	/** [PlayerObjectId, RPC object] */
	private FastMap<Integer, RPC> _rpcList = new FastMap<Integer, RPC>();

	private RPCTable()
	{
		Calendar c = Calendar.getInstance();	
		long startTime = c.getTimeInMillis();
		
		load();
		
		c = Calendar.getInstance();
		long endTime = c.getTimeInMillis();
		
		System.out.println(" - RPCTable loaded "+(getRpcList().size())+" objects in "+(endTime - startTime)+" ms.");
	}
	
	public static RPCTable getInstance()
	{
		if(_instance == null)
		{
			_instance = new RPCTable();
		}
		
		return _instance;
	}
	
	public FastMap<Integer, RPC> getRpcList() 
	{
		return _rpcList;
	}

	public void setRpcList(FastMap<Integer, RPC> _rpcList) 
	{
		this._rpcList = _rpcList;
	}
	
	public void addRpcForPlayer(int playerId, long addRpc)
	{
		if(getRpcList().containsKey(playerId))
		{
			RPC rpc = getRpcList().get(playerId);
			
			rpc.increaseRpcTotalAndCurrentBy(addRpc);

			return;
		}
		
		// else create new RPC for this player:
		
		RPC rpc = new RPC(playerId);
		
		rpc.increaseRpcTotalAndCurrentBy(addRpc);
		rpc.setDbStatus(DBStatus.INSERTED); // as inserted
		
		getRpcList().put(rpc.getPlayerId(), rpc);
	}
	
	/**
	 * This method (RPC system too) is strongly connected with PvPStats updater, so I return null, when I find nothing.
	 * @param playerId
	 * @return NULL if not founded.
	 */
	public RPC getRpcByPlayerId(int playerId)
	{
		FastMap<Integer, RPC> rpcList = RPCTable.getInstance().getRpcList();
		
		for (FastMap.Entry<Integer, RPC> e = rpcList.head(), end = rpcList.tail(); (e = e.getNext()) != end;) 
		{
			if(e.getKey() == playerId)
			{
				return e.getValue();
			}
		}
		
		return null;
	}
	
	private void load()
	{
		Connection con = null;
	    try
	    {
	    	con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM rank_pvp_system_rpc ORDER BY rpc_total"); // ordered for faster search.


	        ResultSet rset = statement.executeQuery();
	
	        while(rset.next())
	        {
	        	RPC rpc = new RPC();
	        	
	        	rpc.setPlayerId(rset.getInt("player_id"));
	        	rpc.setRpcTotal(rset.getLong("rpc_total"));
	        	rpc.setRpcCurrent(rset.getLong("rpc_current"));
	        	
	        	_rpcList.put(rpc.getPlayerId(), rpc); 
	        }  

			rset.close();
			statement.close();
		}
	    catch(SQLException e)
	    {
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Update executed in PvpTable class.
	 */
	public void updateDB()
	{

		Connection con = null;
		Statement statement = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			
			// search new or updated fields in RPCTable:
			for (FastMap.Entry<Integer, RPC> e = getRpcList().head(), end = getRpcList().tail(); (e = e.getNext()) != end;)
			{
				if(e.getValue().getDbStatus() == DBStatus.UPDATED)
				{	
					e.getValue().setDbStatus(DBStatus.NONE);
					statement.addBatch("UPDATE rank_pvp_system_rpc SET player_id="+e.getValue().getPlayerId()+", rpc_total="+e.getValue().getRpcTotal()+", rpc_current="+e.getValue().getRpcCurrent()+" WHERE player_id="+e.getValue().getPlayerId());
				}
				else if(e.getValue().getDbStatus() == DBStatus.INSERTED)
				{
					e.getValue().setDbStatus(DBStatus.NONE);
					statement.addBatch("INSERT INTO rank_pvp_system_rpc (player_id, rpc_total, rpc_current) VALUES ("+e.getValue().getPlayerId()+", "+e.getValue().getRpcTotal()+", "+e.getValue().getRpcCurrent()+")");
				}
			}
			
			statement.executeBatch();
			
			statement.close();
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
					con = null;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
}
