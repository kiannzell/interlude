package com.l2jfrozen.gameserver.masteriopack.rankpvpsystem;

/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;





import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.util.database.L2DatabaseFactory;

import javolution.util.FastMap;

/**
 * @author Masterio
 *
 */
public class PvpTable
{
	private static PvpTable _instance = null;

	/** [killer_id, KillerPvpStats] */
	private FastMap<Integer, KillerPvpStats> _killerPvpTable = new FastMap<Integer, KillerPvpStats>();
	
	private PvpTable()
	{
		Calendar c = Calendar.getInstance();	
		long startTime = c.getTimeInMillis();
		
		if(RankPvpSystemConfig.DATABASE_CLEANER_ENABLED)
		{
			cleanPvpTable();
		}
		
		load();
		
		c = Calendar.getInstance();
		long endTime = c.getTimeInMillis();
		
		System.out.println(" - PvpTable loaded "+(this.getKillerPvpTable().size())+" objects in "+(endTime-startTime)+" ms.");
		
		ThreadPoolManager.getInstance().scheduleGeneral(new PvpTableSchedule() , RankPvpSystemConfig.PVP_TABLE_UPDATE_INTERVAL);
	
	}
	
	public static PvpTable getInstance()
	{
		if(_instance == null)
		{
			_instance = new PvpTable();
		}
		
		return _instance;
	}

	/**
	 * Get PvP object, if not found returns new PvP object for killer -> victim.<br>
	 * 1: Search killerPvpTable by killerId<br>
	 * 2: Search victimPvpTable by victimId.
	 * @param killerId 
	 * @param victimId 
	 * @return 
	 */
	public Pvp getPvp(int killerId, int victimId)
	{
		// find and get PvP:
		KillerPvpStats kps = getKillerPvpStats(killerId);
		
		if(kps != null)
		{
			Pvp pvp = kps.getVictimPvpTable().get(victimId);
			
			if(pvp != null)
			{
				return pvp;
			}
		}

		// otherwise create and get new PvP:
		if(kps == null)
		{
			kps = new KillerPvpStats();
			kps.setCharacterId(killerId);
			kps.onUpdateRankPoints();
		}
		
		Pvp pvp = kps.getVictimPvpTable().get(victimId);
		
		if(pvp == null)
		{
			pvp = new Pvp();

			pvp.setVictimObjId(victimId);
			pvp.setDbStatus(DBStatus.INSERTED);
			//pvp.setDbStatus(DBStatus.NONE);
			
			kps.getVictimPvpTable().put(victimId, pvp);
		}

		return pvp;
	}
	
	/**
	 * Get PvP object, if not found returns new PvP object for killer -> victim.<br>
	 * 1: Search killerPvpTable by killerId<br>
	 * 2: Search victimPvpTable by victimId.<br>
	 * Reset daily fields if required.
	 * @param killerId
	 * @param victimId
	 * @param systemDay
	 * @return
	 */
	public Pvp getPvp(int killerId, int victimId, long systemDay)
	{
		// find and get PvP:
		KillerPvpStats kps = getKillerPvpStats(killerId, systemDay);
		
		if(kps != null)
		{
			Pvp pvp = kps.getVictimPvpTable().get(victimId);
			
			if(pvp != null)
			{
				// check daily fields, reset if kill day is other than system day:
				if(pvp.getKillDay() != systemDay)
				{
					pvp.resetDailyFields();
				}
				
				return pvp;
			}
		}

		// otherwise create and get new PvP:
		if(kps == null)
		{
			kps = new KillerPvpStats();
			kps.setCharacterId(killerId);
			kps.onUpdateRankPoints();
		}
		
		Pvp pvp = kps.getVictimPvpTable().get(victimId);
		
		if(pvp == null)
		{
			pvp = new Pvp();

			pvp.setVictimObjId(victimId);
			pvp.setDbStatus(DBStatus.INSERTED);
			//pvp.setDbStatus(DBStatus.NONE);
			
			kps.getVictimPvpTable().put(victimId, pvp);
		}

		return pvp;
	}
	
	/**
	 * Returns Killer PvP statistics like total kills, total legal kills, etc. for killer id.<br>
	 * Reset daily fields if required (today taken from current time).
	 * @param killerId
	 * @return
	 */
	public KillerPvpStats getKillerPvpStats(int killerId)
	{
		// get system day for update daily fields:
		Calendar c = Calendar.getInstance();	
		c.set(Calendar.MILLISECOND, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.HOUR, 0);
		
		long systemDay = c.getTimeInMillis(); // current system day
		
		// find and get PvP:
		KillerPvpStats kps =  getKillerPvpTable().get(killerId);
		
		if(kps == null)
		{
			kps = new KillerPvpStats();
			kps.setCharacterId(killerId);
			kps.onUpdateRankPoints();
			
			getKillerPvpTable().put(killerId, kps);
		}
		else
		{
			kps.updateDailyStats(systemDay);
		}
		
		return kps;
	}
	
	/**
	 * Returns Killer PvP statistics like total kills, total legal kills, etc. for killer id.<br>
	 * Reset daily fields if required.
	 * @param killerId
	 * @param systemDay 
	 * @return
	 */
	public KillerPvpStats getKillerPvpStats(int killerId, long systemDay)
	{
		// find and get PvP:
		KillerPvpStats kps =  getKillerPvpTable().get(killerId);
		
		if(kps == null)
		{
			kps = new KillerPvpStats();
			kps.setCharacterId(killerId);
			kps.onUpdateRankPoints();
			
			getKillerPvpTable().put(killerId, kps);
		}
		else
		{
			kps.updateDailyStats(systemDay);
		}
		
		return kps;
	}

	public FastMap<Integer, KillerPvpStats> getKillerPvpTable()
	{
		return _killerPvpTable;
	}

	public void setKillerPvpTable(FastMap<Integer, KillerPvpStats> _killerPvpTable)
	{
		this._killerPvpTable = _killerPvpTable;
	}

	private void load()
	{
		
		Connection con = null;
	    try
	    {
	    	con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement statement = con.prepareStatement("SELECT * FROM rank_pvp_system"); 

	        ResultSet rset = statement.executeQuery();

	        while(rset.next())
	        {
	        	Pvp pvp = new Pvp();

	        	pvp.setVictimObjId(rset.getInt("victim_id"));
	        	
	        	pvp.setKills(rset.getInt("kills"));
	        	pvp.setKillsToday(rset.getInt("kills_today"));
	        	pvp.setKillsLegal(rset.getInt("kills_legal"));
	        	pvp.setKillsLegalToday(rset.getInt("kills_today_legal"));
	        	pvp.setRankPoints(rset.getLong("rank_points"));
	        	pvp.setRankPointsToday(rset.getLong("rank_points_today"));
	        	pvp.setWarKillsLegal(rset.getInt("war_kills_legal"));
	        	pvp.setWarKills(rset.getInt("war_kills"));
	        	pvp.setKillTime(rset.getLong("kill_time"));
	        	pvp.setKillDay(rset.getLong("kill_day"));

	        	pvp.setDbStatus(DBStatus.NONE); // load as NONE
	        	
	        	KillerPvpStats kps = getKillerPvpStats(rset.getInt("killer_id"));
	        	
	        	kps.addVictimPvp(pvp);

	        }  
	        
			rset.close();
			statement.close();
		}
	    catch(SQLException e)
	    {
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

	}
	
	public void updateDB()
	{

		Connection con = null;
		Statement statement = null;
		
		try
		{
			con = L2DatabaseFactory.getInstance().getConnection();
			statement = con.createStatement();
			
			// search new or updated fields in VictimPvpTable:
			for (FastMap.Entry<Integer, KillerPvpStats> e = getKillerPvpTable().head(), end = getKillerPvpTable().tail(); (e = e.getNext()) != end;)
			{
				KillerPvpStats kps = e.getValue();
				
				for (FastMap.Entry<Integer, Pvp> f = kps.getVictimPvpTable().head(), end2 = kps.getVictimPvpTable().tail(); (f = f.getNext()) != end2;)
				{
					Pvp pvp = f.getValue();
					
					if(pvp.getDbStatus() == DBStatus.UPDATED)
					{	
						statement.addBatch("UPDATE rank_pvp_system SET kills="+pvp.getKills()+", kills_today="+pvp.getKillsToday()+", kills_legal="+pvp.getKillsLegal()+", kills_today_legal="+pvp.getKillsLegalToday()+", rank_points="+pvp.getRankPoints()+", rank_points_today="+pvp.getRankPointsToday()+", war_kills_legal="+pvp.getWarKillsLegal()+", war_kills="+pvp.getWarKills()+", kill_time="+pvp.getKillTime()+", kill_day="+pvp.getKillDay()+" WHERE killer_id="+kps.getCharacterId()+" AND victim_id="+pvp.getVictimObjId());
						pvp.setDbStatus(DBStatus.NONE); // it is after query because pvp is updating in real time.
					}
					else if(pvp.getDbStatus() == DBStatus.INSERTED)
					{
						statement.addBatch("INSERT INTO rank_pvp_system (killer_id, victim_id, kills, kills_today, kills_legal, kills_today_legal, rank_points, rank_points_today, war_kills_legal, war_kills, kill_time, kill_day) VALUES ("+kps.getCharacterId()+", "+pvp.getVictimObjId()+", "+pvp.getKills()+", "+pvp.getKillsToday()+", "+pvp.getKillsLegal()+", "+pvp.getKillsLegalToday()+", "+pvp.getRankPoints()+", "+pvp.getRankPointsToday()+", "+pvp.getWarKillsLegal()+", "+pvp.getWarKills()+", "+pvp.getKillTime()+", "+pvp.getKillDay()+")");
						pvp.setDbStatus(DBStatus.NONE);
					}
				}
			}

			statement.executeBatch();
			
			statement.close();
			
		}
		catch(SQLException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
					con = null;
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Remove permanently not active players!
	 */
	private static void cleanPvpTable()
	{
		Connection con = null;
	    try
	    {
	    	con = L2DatabaseFactory.getInstance().getConnection();
			PreparedStatement statement = con.prepareStatement("DELETE FROM rank_pvp_system WHERE (SELECT (lastAccess) FROM characters WHERE obj_Id = killer_id) < ?"); 
			
			// calculate ignore time:
			Calendar c = Calendar.getInstance();
			long ignoreTime = c.getTimeInMillis() - RankPvpSystemConfig.DATABASE_CLEANER_REPEAT_TIME;
			
			statement.setLong(1, ignoreTime);

	        statement.execute();

			statement.close();
		}
	    catch(SQLException e)
	    {
			e.printStackTrace();
		}
		finally
		{
			try
			{
				if(con != null)
				{
					con.close();
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	    
	    System.out.println(" - Cleaned Pvp Table with players who are inactive for longer than "+Math.round((double)RankPvpSystemConfig.DATABASE_CLEANER_REPEAT_TIME / (double)86400000)+" day(s).");
	    
	}

	private static class PvpTableSchedule implements Runnable
	{
		public PvpTableSchedule()
		{
			
		}

		@Override
		public void run()
		{
			if(!TopTable.getInstance().isUpdating())
			{
				//PvpTable.getInstance().updateDB();
				
				// update RPC here:
				RPCTable.getInstance().updateDB();
				
				ThreadPoolManager.getInstance().scheduleGeneral(new PvpTableSchedule() , RankPvpSystemConfig.PVP_TABLE_UPDATE_INTERVAL);
			}
			else
			{
				ThreadPoolManager.getInstance().scheduleGeneral(new PvpTableSchedule() , 30000);
			}
		}
	}
}