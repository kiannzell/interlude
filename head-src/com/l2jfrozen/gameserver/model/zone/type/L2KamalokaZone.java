/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.zone.type;

import org.w3c.dom.Node;

import javolution.util.FastList;

import com.l2jfrozen.gameserver.datatables.csv.MapRegionTable;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.zone.L2ZoneType;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.util.random.Rnd;

/**
 * Chaotic Zone
 * 
 * @author -Sense-
 */
public class L2KamalokaZone extends L2ZoneType
{
	private FastList<int[]> _spawnLoc;

	public L2KamalokaZone(int id)
	{
		super(id);

		_spawnLoc = new FastList<int[]>();
		
	}

	@Override
	public void setSpawnLocs(Node node)
	{
		int ai[] = new int[3];

		Node node1 = node.getAttributes().getNamedItem("X");

		if(node1 != null)
		{
			ai[0] = Integer.parseInt(node1.getNodeValue());
		}

		node1 = node.getAttributes().getNamedItem("Y");

		if(node1 != null)
		{
			ai[1] = Integer.parseInt(node1.getNodeValue());
		}

		node1 = node.getAttributes().getNamedItem("Z");

		if(node1 != null)
		{
			ai[2] = Integer.parseInt(node1.getNodeValue());
		}
		_spawnLoc.add(ai);
	}

	@Override
	protected void onEnter(L2Character character)
	{
		character.setInsideZone(L2Character.ZONE_KAMALOKA, true);

		if(character instanceof L2PcInstance)
		{
			final L2PcInstance player = (L2PcInstance) character;
			
			 if (player.isInKamaloka() || player.isGM())
			 {
			   player.sendMessage("You entered a Four Sepulcher Zone");
			 }
			else
			{
				ThreadPoolManager.getInstance().scheduleGeneral(new Runnable()
				{
					@Override
					public void run()
					{
						player.sendMessage("You're not allowed in this zone!");
						player.teleToLocation(46525, 187585, -3512);
					}
				}, 10000);
			}
			}
		}

	@Override
	protected void onExit(L2Character character)
	{
		character.setInsideZone(L2Character.ZONE_KAMALOKA, false);

		if(character instanceof L2PcInstance)
		{
			if (((L2PcInstance) character).isInKamaloka())
			{
			((L2PcInstance) character).setInKamaloka(false);
			}
			((L2PcInstance) character).sendMessage("You left a Four Sepulcher Zone");
		}
	}

	@Override
	protected void onDieInside(L2Character character)
	{}

	@Override
	protected void onReviveInside(L2Character character)
	{}

	public void oustAllPlayers()
	{
		if(_characterList == null)
			return;

		if(_characterList.isEmpty())
			return;

		for(L2Character character : _characterList.values())
		{
			if(character == null)
			{
				continue;
			}

			if(character instanceof L2PcInstance)
			{
				L2PcInstance player = (L2PcInstance) character;

				if(player.isOnline() == 1)
				{
					player.teleToLocation(MapRegionTable.TeleportWhereType.Town);
				}

				player = null;
			}
		}
	}

	/**
	 * Returns this zones spawn location
	 * 
	 * @return
	 */
	public final int[] getSpawnLoc()
	{
		int ai[] = new int[3];

		ai = _spawnLoc.get(Rnd.get(_spawnLoc.size()));

		return ai;
	}
}
