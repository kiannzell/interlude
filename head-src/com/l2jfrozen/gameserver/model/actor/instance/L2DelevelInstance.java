/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.actor.instance;

import javolution.text.TextBuilder;

import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import com.l2jfrozen.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfrozen.gameserver.templates.L2NpcTemplate;

/**
 * This class ...
 * @author -Sensei-
 */
public class L2DelevelInstance extends L2FolkInstance
{

	public L2DelevelInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		int level = player.getStat().getLevel();     
		if(command.startsWith("delevel"))
		{
			if (player.getActingPlayer().getLevel() <= 20)
			{
	    		lowlevel(player);
	    		player.sendPacket(ActionFailed.STATIC_PACKET);
	        	return;
			}
			else if (player.isAio())
			{
	    		aio(player);
	    		player.sendPacket(ActionFailed.STATIC_PACKET);
	        	return;
			}
			else if (player.getAdena() < 1000*3)
			{
	    		adena(player);
	    		player.sendPacket(ActionFailed.STATIC_PACKET);
	        	return;
			}
			else if (player.getKarma() > 0)
			{
	    		karma(player);
	    		player.sendPacket(ActionFailed.STATIC_PACKET);
	        	return;
			}
			else
			{
    		final long pXp = player.getStat().getExp();
        	final long tXp = player.getStat().getExpForLevel(level-1);
        	player.getStat().removeExpAndSp(pXp - tXp, 0);
        	player.reduceAdena("delevel",1000, null, true);
        	delevelInfo(player);
        	return;
		}
	}
}
	
	//mod
	
	/**
	 * this is called when a player interacts with this NPC
	 * 
	 * @param player
	 */
	@Override
	public void onAction(L2PcInstance player)
	{
			if (this != player.getTarget()) 
			{
					player.setTarget(this);
					player.sendPacket(new MyTargetSelected(getObjectId(), player.getLevel() - getLevel()));
					player.sendPacket(new ValidateLocation(this));
				}
				else if (isInsideRadius(player, INTERACTION_DISTANCE, false, false)) 
				{
					//SocialAction sa = new SocialAction(this, Rnd.get(8));
					player.broadcastPacket(new SocialAction(player.getObjectId(), 6));
					//broadcastPacket(sa);
					//player.setCurrentFolkNPC(this);
					delevelInfo(player);
					player.sendPacket(ActionFailed.STATIC_PACKET);
				}
				else 
				{
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
					player.sendPacket(ActionFailed.STATIC_PACKET);
				}
	}
	//mod 2
	private void delevelInfo(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("Hello traveler, I am the delevel manager!<br> I can reduce your level, one level at a time.<br>So, I believe you didn't come here just to waste my time<Br>You can't find any service like mine anywhere in this region.<br>Well then,do you want to decrease your previous experience? I can help you do so for 1,000 adena!<br>");
		sb.append("<br>");
		sb.append("<br><br>");
		sb.append("<center><a action=\"bypass -h npc_" + getObjectId() + "_delevel\">Delevel</a></center>");
		sb.append("<br><br><br><br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	
	private void lowlevel(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>Your level is too low!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	private void aio(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>Aio buffers are not allowed to delevel!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	private void karma(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>Players with karma cannot delevel!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	private void adena(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>You don't have enough adena!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
}
