package com.l2jfrozen.gameserver.model.actor.instance;

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */



import java.util.ArrayList;
import java.util.StringTokenizer;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.datatables.BufferSkillsTable;
import com.l2jfrozen.gameserver.datatables.CharSchemesTable;
import com.l2jfrozen.gameserver.datatables.SkillTable;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.MagicSkillUser;
import com.l2jfrozen.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfrozen.gameserver.powerpak.PowerPakConfig;
import com.l2jfrozen.gameserver.templates.L2NpcTemplate;
import com.l2jfrozen.util.StringUtil;

import javolution.text.TextBuilder;
import javolution.util.FastList;
import javolution.util.FastMap;


/**
 * <b><font size=3>NPC Buffer instance handler</font></b><br>
 * <br>
 * This class contains some methods that can be sorted by different types and functions:<br><br>
 * 
 * - Methods that overrides to superclass' (L2FolkInstance):
 *  <li>onAction
 *  <li>onBypassFeedback 
 *  <li>onActionShift
 *  <br><br>
 * 
 * - Methods to show html windows: 
 * <li>showGiveBuffsWindow
 * <li>showManageSchemeWindow
 * <li>showEditSchemeWindow
 * <br></br>
 * 
 * - Methods to get and build info (Strings, future html content) from character schemes, state, etc.
 * <li>getPlayerSchemeListFrame: Returns a table with player's schemes names
 * <li>getGroupSkillListFrame: Returns a table with skills available in the skill_group
 * <li>getPlayerSkillListFrame: Returns a table with skills already in player's scheme (scheme_key)
 * 		
 * <br>
 * <br>
 * @author  House
 */

public class L2AIOInstance extends L2NpcInstance
{
	private static final String PARENT_DIR = "data/html/mods/buffer/";
	
	public L2AIOInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{   		
		StringTokenizer st = new StringTokenizer(command, " ");
		String currentCommand = st.nextToken();

	    if (currentCommand.startsWith("schemebuff"))
		{
		       NpcHtmlMessage html = new NpcHtmlMessage(1);
		       html.setFile(PARENT_DIR+"menu.htm");
		       sendHtmlMessage(player, html);
	    }
	    else if (currentCommand.startsWith("aiobuff"))
		{
		   showMessageWindow(player);
		}
	    else if (currentCommand.startsWith("menu"))
		{
			NpcHtmlMessage html = new NpcHtmlMessage(1);
			html.setFile(PARENT_DIR+"menu.htm");
			sendHtmlMessage(player, html);
		}
	    else if (currentCommand.startsWith("support"))
		{
			String targettype = st.nextToken();			
			showGiveBuffsWindow(player, targettype);
		}
	    else if (currentCommand.startsWith("givebuffs"))
		{
			String targettype = st.nextToken();
			String scheme_key = st.nextToken();
			int cost = Integer.parseInt(st.nextToken());
			
			if (cost == 0 || cost <= player.getInventory().getAdena()) 
			{
				L2Character target = player;
				if (targettype.equalsIgnoreCase("pet"))
					target = player.getPet();
				
				else if (target != null)
				{			
					for (L2Skill sk : CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key))
					{
						sk.getEffects(this, target);
					}	
					player.reduceAdena("NPC Buffer", cost, this, true);
				}
				else
				{
					player.sendMessage("Incorrect Pet");
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(PARENT_DIR+"menu.htm");
					sendHtmlMessage(player, html);
				}				
			}
			else
			{
				player.sendMessage("Not enough adena");
				showGiveBuffsWindow(player, targettype);
			}
		}
	    else if (currentCommand.startsWith("editscheme"))
		{
			String skill_group = st.nextToken();
			String scheme_key = null;
			try
			{
				scheme_key = st.nextToken();
			}
			catch (Exception e)
			{
			}
			
			showEditSchemeWindow(player, skill_group, scheme_key);			
		}
	    else if (currentCommand.startsWith("skill"))
		{
			String skill_group = st.nextToken();
			String scheme_key = st.nextToken();
			int skill_id = Integer.parseInt(st.nextToken());
			int level = BufferSkillsTable.getInstance().getSkillLevelById(skill_id);
			
			if (currentCommand.startsWith("skillselect") && !scheme_key.equalsIgnoreCase("unselected"))
			{	
				if (CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key).size()< Config.NPCBUFFER_MAX_SKILLS)
					CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key).add(SkillTable.getInstance().getInfo(skill_id, level));
				else 
					player.sendMessage("This scheme has reached maximun amount of buffs");
			}
			else if (currentCommand.startsWith("skillunselect"))
			{
				CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key).remove(SkillTable.getInstance().getInfo(skill_id, level));
			}
			
			showEditSchemeWindow(player, skill_group, scheme_key);			
		}
	    else if (currentCommand.startsWith("manageschemes"))
		{
			showManageSchemeWindow(player);
		}
	    else if (currentCommand.startsWith("createscheme"))
		{
			String name = st.nextToken();
			if (name.length() > 14) 
			{
				player.sendMessage("Error: Scheme's name must contain up to 14 chars without any spaces");
				showManageSchemeWindow(player);
			}			
			else if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) != null 
					&& CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).size() == Config.NPCBUFFER_MAX_SCHEMES)
			{
				player.sendMessage("Error: Maximun schemes amount reached, please delete one before creating a new one");
				showManageSchemeWindow(player);
			}
			else if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) != null 
					&& CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).containsKey(name))
			{
				player.sendMessage("Error: duplicate entry. Please use another name");
				showManageSchemeWindow(player);
			}
			else
			{
				if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) == null) 
					CharSchemesTable.getInstance().getSchemesTable().put(player.getObjectId(), new FastMap<String, FastList<L2Skill>>(Config.NPCBUFFER_MAX_SCHEMES+1));
				
				CharSchemesTable.getInstance().setScheme(player.getObjectId(), name.trim(), new FastList<L2Skill>(Config.NPCBUFFER_MAX_SKILLS+1));
				
				showManageSchemeWindow(player);
			}			
		}
	     else if (currentCommand.startsWith("deletescheme"))
		{
			String name = st.nextToken();
			
			if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) != null 
					&& CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).containsKey(name))
			{
				CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).remove(name);
				showManageSchemeWindow(player);
			}			
		}
	     else if (currentCommand.startsWith("clearscheme"))
		{
			String name = st.nextToken();
			
			if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) != null 
					&& CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).containsKey(name))
			{
				CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).get(name).clear();
				showManageSchemeWindow(player);
			}
			
		}
	     else if (currentCommand.equalsIgnoreCase("restore")) 
	        {
	            player.setCurrentHpMp(player.getMaxHp(), player.getMaxMp());
	            player.setCurrentCp(player.getMaxCp());
	            showMainMenu(player);
	        }
	     else if (currentCommand.equalsIgnoreCase("cancel")) 
	        {
	            player.stopAllEffects();
	            showMainMenu(player);
	        }
	       //predefined buffs
			else if (currentCommand.startsWith("magebuff"))
			{
				ArrayList<L2Skill> skills_to_buff = new ArrayList<L2Skill>();
				for(int skillId:PowerPakConfig.MAGE_SKILL_LIST.keySet())
				{
						L2Skill skill = SkillTable.getInstance().getInfo(skillId, PowerPakConfig.MAGE_SKILL_LIST.get(skillId));
						if(skill!=null)
						{
							skills_to_buff.add(skill);
						}
						
					}
					
				}
				else if(currentCommand.startsWith("fighterbuff"))
				{
					ArrayList<L2Skill> skills_to_buff = new ArrayList<L2Skill>();
					for(int skillId:PowerPakConfig.FIGHTER_SKILL_LIST.keySet())
					{
						L2Skill skill = SkillTable.getInstance().getInfo(skillId, PowerPakConfig.FIGHTER_SKILL_LIST.get(skillId));
						if(skill!=null)
						{
							skills_to_buff.add(skill);
						}
					}
				}
		   else if (currentCommand.equalsIgnoreCase("getbuff")) 
	        {
	    	    int buffid = 0;
	            int bufflevel = 1;
				
				if (st.countTokens() == 2) 
		        {
		            buffid = Integer.valueOf(st.nextToken());
		            bufflevel = Integer.valueOf(st.nextToken());
		        }
		        else if (st.countTokens() == 1)
		        {
		            buffid = Integer.valueOf(st.nextToken());
		        }
				if (buffid != 0) 
	            {
	                MagicSkillUser mgc = new MagicSkillUser(this, player, buffid, bufflevel, 5, 0);
	                SkillTable.getInstance().getInfo(buffid, bufflevel).getEffects(this, player);
	                showMessageWindow(player);
	                player.broadcastPacket(mgc);
	            }
	        }
          else 
	    	 super.onBypassFeedback(player, command);
	}
	
	@Override
	public void onAction(L2PcInstance player)
	{
		
		if (player.isOutOfControl())
		{
			player.sendPacket(ActionFailed.STATIC_PACKET);
			return;
		}
		
		if (Olympiad.getInstance().isRegistered(player))
		{
			player.sendMessage("You cannot use the buffer while registered in Olympiad.");
			player.sendPacket(ActionFailed.STATIC_PACKET);
			return;
		}

		// Check if the L2PcInstance already target the L2NpcInstance
		if (this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);

			// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
			MyTargetSelected my = new MyTargetSelected(getObjectId(), 0);
			player.sendPacket(my);

			// Send a Server->Client packet ValidateLocation to correct the L2NpcInstance position and heading on the client
			player.sendPacket(new ValidateLocation(this));
		}
		else
		{
			// Calculate the distance between the L2PcInstance and the L2NpcInstance
			if (!canInteract(player))
			{
				// Notify the L2PcInstance AI with AI_INTENTION_INTERACT
				// note: commented out so the player must stand close
				//player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
			}
			else
			{
				showMainMenu(player);
			}
		}
		// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	private void sendHtmlMessage(L2PcInstance player, NpcHtmlMessage html)
    {
        html.replace("%objectId%", String.valueOf(getObjectId()));
        html.replace("%npcId%", String.valueOf(getNpcId()));
        player.sendPacket(html);
    }
	
	/**
	 * Sends an html packet to player with Give Buffs menu info for player and pet,
	 * depending on targettype parameter {player, pet}
	 * 
	 * @param player
	 * @param targettype
	 */	
	private void showGiveBuffsWindow(L2PcInstance player, String targettype)
	{
		FastMap<String, FastList<L2Skill>> map =  CharSchemesTable.getInstance().getAllSchemes(player.getObjectId());
		TextBuilder tb = new TextBuilder();
		
		tb.append("<html><body><center>" +
				"<img src=\"L2UI_CH3.herotower_deco\" width=256 height=32 /><br>");
		tb.append("Here are your defined profiles and their fee,<br1>just click on it to receive effects<br>");
		if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) == null 
				|| CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).isEmpty()) 
			tb.append("You have not defined any valid scheme,<br1>please go to Manage scheme and create at least one");
		else			
		{
			int cost;
			tb.append("My Schemes:<br>");
			for (FastMap.Entry<String, FastList<L2Skill>> e = map.head(), end = map.tail(); (e = e.getNext()) != end;)
			{
				cost = getFee(e.getValue());
				tb.append("<a action=\"bypass -h npc_%objectId%_givebuffs "+targettype+" "+e.getKey()+" "+String.valueOf(cost)+"\">"+e.getKey()+"</a> Cost: "+cost+"<br1>");
			}
		}
		tb.append("</body></html>");
		
		NpcHtmlMessage html = new NpcHtmlMessage(1);
		html.setHtml(tb.toString());
		sendHtmlMessage(player, html);
	}
	
	/**
	 * Sends an html packet to player with Manage scheme menu info. This allows player 
	 * to create/delete/clear schemes
	 * 
	 * @param player
	 */
	private void showManageSchemeWindow(L2PcInstance player)
	{
		TextBuilder tb = new TextBuilder();
		
		tb.append("<html><body><center>");
		tb.append("<img src=\"L2UI_CH3.herotower_deco\" width=256 height=32 /><br>");
		
		if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) == null 
				|| CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).isEmpty())
		{
			tb.append("Here is a list of your schemes.<br1>" +
					"To create, fill name box and press create.<br1>"
					+"Each scheme must have different name.<br1>" +
						"Name must have up to 14 chars.<br1>" +
						"Spaces (\" \") are not allowed.<br1>" +
						"DO NOT click on Create until you have filled quick box<br>");
			tb.append("<font color=\"LEVEL\">You have not created any scheme</font><br>");
		}
		else
		{
			tb.append("Here is a list of your schemes.<br1>" +
					"To delete one just click on drop button.<br1>" +
					"To create, fill name box and press create.<br1>"
					+"Each scheme must have different name.<br1>" +
							"Name must have up to 14 chars.<br1>" +
							"Spaces (\" \") are not allowed.<br1>" +
							"DO NOT click on Create until you have filled quick box<br>");
			tb.append("<table>");
			
			for (FastMap.Entry<String, FastList<L2Skill>> 
			e = CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).head(), 
			end = CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).tail(); 
			(e = e.getNext()) != end;)
			{
				tb.append("<tr><td width=\"140\">"+e.getKey()+" ("+String.valueOf(CharSchemesTable.getInstance().getScheme(player.getObjectId(), e.getKey()).size())+" skill(s))</td>");
				tb.append("<td width=\"60\"><button value=\"Clear\" action=\"bypass -h npc_%objectId%_clearscheme "+e.getKey()+"\" width=55 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
				tb.append("<td width=\"60\"><button value=\"Drop\" action=\"bypass -h npc_%objectId%_deletescheme "+e.getKey()+"\" width=55 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr>");
			}
		}
		tb.append("<br><table width=240>");
		tb.append("<tr><td><edit var=\"name\" width=120 height=15></td><td><button value=\"Create\" action=\"bypass -h npc_%objectId%_createscheme $name\" width=55 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td>");
		tb.append("<td><button value=\"Back\" action=\"bypass -h npc_%objectId%_menu\" width=55 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr></table>");
		tb.append("<br>Max schemes per player: "+String.valueOf(Config.NPCBUFFER_MAX_SCHEMES)+"<br>");
		tb.append("</body></html>");
		
		NpcHtmlMessage html = new NpcHtmlMessage(1);
		html.setHtml(tb.toString());
		sendHtmlMessage(player, html);
	}
	
	/**
	 * This sends an html packet to player with Edit Scheme Menu info. This allows player to edit 
	 * each created scheme (add/delete skills)
	 * 
	 * @param player
	 * @param skill_group
	 * @param scheme_key
	 */
	
	private void showEditSchemeWindow(L2PcInstance player, String skill_group, String scheme_key)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(1);
		html.setFile(PARENT_DIR+"schememenu.htm");
		html.replace("%typesframe%", getTypesFrame(scheme_key));
		
		if (skill_group.equalsIgnoreCase("unselected"))
		{
			html.replace("%schemelistframe%", getPlayerSchemeListFrame(player, skill_group, scheme_key));
			html.replace("%skilllistframe%", getGroupSkillListFrame(player, null, null));			
			html.replace("%myschemeframe%", getPlayerSkillListFrame(player, null, null));
		}
		else
		{
			html.replace("%schemelistframe%", getPlayerSchemeListFrame(player, skill_group, scheme_key));
			html.replace("%skilllistframe%", getGroupSkillListFrame(player, skill_group, scheme_key));			
			html.replace("%myschemeframe%", getPlayerSkillListFrame(player, skill_group, scheme_key));
		}
		sendHtmlMessage(player, html);
	}
	
	
	private void showMessageWindow(L2PcInstance player)
    {
        NpcHtmlMessage html = new NpcHtmlMessage(1);
        final StringBuilder strBuffer = StringUtil.startAppend(3500, "<html><title>Buffer</title><body><center>");

        if (player.isSitting()) 
        {
        	player.sendMessage("You can't use buffer while you're sitting.");
            strBuffer.append("Stand up, <font color=\"LEVEL\">%charname%</font>!<br>");
            strBuffer.append("How dare you to talk with me while you're sitting?!<br>");
        }
        else if (player.isAlikeDead()) 
        {
        	player.sendMessage("You can't use buffer while you're dead or using fake death.");
            strBuffer.append("Sadly, <font color=\"LEVEL\">%charname%</font>, you're dead.<br>");
            strBuffer.append("I can't offer any support effect for dead people...<br>");
        }
        else if (player.isInCombat()) 
        {
        	player.sendMessage("You can't use buffer while you're in combat.");
            strBuffer.append("Sadly, <font color=\"LEVEL\">%charname%</font>, I can't serve you.<br>");
            strBuffer.append("Came back when you will not be in a combat.<br>");
        }
        else 
        {
        	strBuffer.append("Welcome, <font color=\"LEVEL\">%charname%</font>!<br>");
 		    strBuffer.append("Here is the list of all available buffs:<br>");
 			strBuffer.append("<img src=\"L2UI_CH3.onscrmsg_pattern01_2\" width=300 height=32 align=left>");
 			strBuffer.append("<table width=300>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1204 2\">Wind Walk</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1040 3\">Shield</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1243 6\">Bless Shield</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1068 3\">Might</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1036 2\">Magic Barrier</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1259 4\">Resist Shock</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1035 4\">Mental Shield</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1045 6\">Blessed Body</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1304 3\">Advanced Block</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1048 6\">Blessed Soul</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1062 2\">Berserker Spirit</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1189 3\">Resist Wind</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1086 2\">Haste</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1240 3\">Guidance</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1393 3\">Unholy Resistance</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1242 3\">Death Whisper</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1077 3\">Focus</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1033 3\">Resist Poison</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1268 4\">Vampiric Rage</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1087 3\">Agility</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1191 3\">Resist Fire</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1085 3\">Acumen</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1059 3\">Empower</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1182 3\">Resist Aqua</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1303 2\">Wild Magic</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 1078 6\">Concentration</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1352 1\">Elemental Protection</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1353 1\">Divine Protection</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1397 3\">Clarity</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1392 3\">Holy Resistance</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 1043 1\">Holy Weapon</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1032 3\">Invigor</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1044 3\">Regeneration</a></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td><font color=\"ff9900\">Dances:</font></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 275 1\">Fury</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 273 1\">Mystic</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 365 1\">Siren's</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 274 1\">Fire</a></td>  <td><a action=\"bypass -h npc_%objectId%_getbuff 276 1\">Concentration</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 310 1\">Vampire</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 271 1\">Warrior</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 277 1\">Light</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 272 1\">Inspiration</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 311 1\">Protection</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 309 1\">Earth Guard</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 307 1\">Aqua Guard</a></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td><font color=\"ff9900\">Songs:</font></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 264 1\">Earth</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 269 1\">Hunter</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 270 1\">Invocation</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 266 1\">Water</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 267 1\">Warding</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 304 1\">Vitality</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 268 1\">Wind</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 364 1\">Champion</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 349 1\">Renewal</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 265 1\">Life</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 363 1\">Meditation</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 305 1\">Vengeance</a></td></tr>");
 			strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 308 1\">Storm Guard</a></td> <td><a action=\"bypass -h npc_%objectId%_getbuff 306 1\">Flame Guard</a></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td><font color=\"ff9900\">Other:</font></td></tr>");
 	        strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_cancel\"><font color=\"ffffff\">Cancel</font></a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1388 3\">Greater Might</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1389 3\">Greater Shield</a></td></tr>");
 	        strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_restore\"><font color=\"ffffff\">Restore</font></a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1363 1\">Chant of Victory</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1356 1\">Prophecy of Fire</a></td></tr>");
 	        strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 4699 13\">Bless of Queen</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1355 1\">Prophecy of Water</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 1357 1\">Prophecy of Wind</a></td></tr>");
 	        strBuffer.append("<tr><td><a action=\"bypass -h npc_%objectId%_getbuff 4700 13\">Gift of Queen</a></td><td><a action=\"bypass -h npc_%objectId%_getbuff 4703 13\">Gift of Seraphim</a></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td></td></tr>");
 			strBuffer.append("<tr><td></td><td></td><td></td></tr>");
 			strBuffer.append("<tr><td></td><td></td><td></td></tr>");
 			strBuffer.append("</table><img src=\"L2UI_CH3.onscrmsg_pattern01_2\" width=300 height=32 align=left>");
         }
 		strBuffer.append("</center></body></html>");
 		html.setHtml(strBuffer.toString());
 		html.replace("%objectId%", String.valueOf(getObjectId()));
 		html.replace("%charname%", player.getName());
 		player.sendPacket(html);
     }
	
	private void showMainMenu(L2PcInstance player)
    {
        NpcHtmlMessage html = new NpcHtmlMessage(1);
        final StringBuilder strBuffer = StringUtil.startAppend(3500, "<html><title>Npc Buffer</title><body><center>");

        if (player.isSitting()) 
        {
        	player.sendMessage("You can't use buffer while you're sitting.");
            strBuffer.append("Stand up, <font color=\"LEVEL\">%charname%</font>!<br>");
            strBuffer.append("How dare you to talk with me while you're sitting?!<br>");
        }
        else if (player.isAlikeDead()) 
        {
        	player.sendMessage("You can't use buffer while you're dead or using fake death.");
            strBuffer.append("Sadly, <font color=\"LEVEL\">%charname%</font>, you're dead.<br>");
            strBuffer.append("I can't offer any support effect for dead people...<br>");
        }
        else if (player.isInCombat()) 
        {
        	player.sendMessage("You can't use buffer while you're in combat.");
            strBuffer.append("Sadly, <font color=\"LEVEL\">%charname%</font>, I can't serve you.<br>");
            strBuffer.append("Came back when you will not be in a combat.<br>");
        }
        else 
        {
    		strBuffer.append("<html>");
    		strBuffer.append("<body>");
    		strBuffer.append("<br>");
    		strBuffer.append("<center><img src=\"preciousTex.logo2\" width=252 height=128></center>");
    		strBuffer.append("<br>");
    		strBuffer.append("<tr><td></td></tr>");
    		strBuffer.append("<center> <table><tr>"
    		+ "<td><button value=\"Fighter\" action=\"bypass -h custom_dobuff fighterbuff\" width=75 height=21 back=\"L2UI_CH3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></td> "
    		+ "<td><button value=\"Mage\" action=\"bypass -h custom_dobuff magebuff\" width=75 height=21 back=\"L2UI_ch3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></td>"
    		+ "</tr></table></center>");
    		strBuffer.append("<br>");
    		strBuffer.append("<br>");
    		strBuffer.append("<center><button value=\"Scheme\" action=\"bypass -h npc_" + getObjectId() + "_schemebuff\" width=75 height=21 back=\"L2UI_ch3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></center>");
    		strBuffer.append("<br>");
       		strBuffer.append("<center><button value=\"Buffer\" action=\"bypass -h npc_" + getObjectId() + "_aiobuff\" width=75 height=21 back=\"L2UI_ch3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></center>");
    		strBuffer.append("<br>");
       		strBuffer.append("<center><button value=\"Restore\" action=\"bypass -h npc_" + getObjectId() + "_restore\" width=75 height=21 back=\"L2UI_ch3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></center>");
    		strBuffer.append("<br>");
       		strBuffer.append("<center><button value=\"Cancel\" action=\"bypass -h npc_" + getObjectId() + "_cancel\" width=75 height=21 back=\"L2UI_ch3.Btn1_normalOn\" fore=\"L2UI_ch3.Btn1_normal\"></center>");
    		strBuffer.append("<br><br><br><br>");
    		strBuffer.append("</body>");
    		strBuffer.append("</html>");
         }
 		strBuffer.append("</center></body></html>");
 		html.setHtml(strBuffer.toString());
 		html.replace("%objectId%", String.valueOf(getObjectId()));
 		html.replace("%charname%", player.getName());
 		player.sendPacket(html);
     }
	
	/**
	 * Returns a table with info about player's scheme list.<br>
	 * If player scheme list is null, it returns a warning message
	 * @param player 
	 * @param skill_group 
	 * @param scheme_key 
	 * @return 
	 */
	private static String getPlayerSchemeListFrame(L2PcInstance player, String skill_group, String scheme_key)
	{
		if (CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()) == null 
				|| CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).isEmpty()) 
			return "Please create at least one scheme";
		if (skill_group == null) skill_group = "def";
		if (scheme_key == null) scheme_key = "def";
		
		TextBuilder tb = new TextBuilder();
		tb.append("<table>");
		
		int count = 0;

		for (FastMap.Entry<String, FastList<L2Skill>> e = CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).head(), 
				end = CharSchemesTable.getInstance().getAllSchemes(player.getObjectId()).tail(); 
		(e = e.getNext()) != end;)
		{
			if (count == 0) tb.append("<tr>");
			tb.append("<td  width=\"90\"><a action=\"bypass -h npc_%objectId%_editschemes "+skill_group+" "+
					e.getKey()+"\">"+e.getKey()+"</a></td>");
			if (count == 3)
			{
				tb.append("</tr>");
				count = 0;
			}
			count++;
		}
		
		if (!tb.toString().endsWith("</tr>")) tb.append("</tr>");
		tb.append("</table>");
		
		return tb.toString();
	}
	/**
	 * 
	 * @param player
	 * @param skill_group
	 * @param scheme_key
	 * @return a table with info about skills stored in each skill_group
	 */
	private static String getGroupSkillListFrame(L2PcInstance player, String skill_group,  String scheme_key)
	{
		if (skill_group == null || skill_group == "unselected") return "Please, select a valid group of skills";		
		else if (scheme_key == null || scheme_key == "unselected") return "Please, select a valid scheme";
		
		TextBuilder tb = new TextBuilder();
		
		tb.append("<table>");
		int count = 0;
		for (L2Skill sk : BufferSkillsTable.getInstance().getSkillsByType(skill_group))
		{
			if (CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key) != null 
					&& !CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key).isEmpty() 
					&& CharSchemesTable.getInstance().getSchemeContainsSkill(player.getObjectId(),scheme_key, sk.getId())) 
				{
					continue;
				}
			
			if (count == 0)
				tb.append("<tr>");
			
			if (sk.getId() <= 100)
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill00"+(sk.getId())+"\" fore=\"icon.skill00"+(sk.getId())+"\"></td>");
			else if (sk.getId() <= 1000)
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill0"+(sk.getId())+"\" fore=\"icon.skill0"+(sk.getId())+"\"></td>");
			else
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill"+(sk.getId())+"\" fore=\"icon.skill"+(sk.getId())+"\"></td>");
			
			count++;
			if (count == 4)
			{
				tb.append("</tr>");
				count = 0;
			}
		}
		if (!tb.toString().endsWith("</tr>")) tb.append("</tr>");
		tb.append("</table>");
		
		return tb.toString();		
	}
	/**
	 * 
	 * @param player
	 * @param skill_group
	 * @param scheme_key
	 * @return a table with info about selected skills
	 */
	private static String getPlayerSkillListFrame(L2PcInstance player, String skill_group, String scheme_key)
	{
		if (skill_group == null || skill_group=="unselected") return "<br>Please, select a valid group of skills";		
		else if (scheme_key == null || scheme_key=="unselected") return "<br>Please, select a valid scheme";
		
		if (CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key) == null) 
			return "Please choose your Scheme";
		
		if (CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key).isEmpty()) 
			return "Empty Scheme";
		
		TextBuilder tb = new TextBuilder();
		tb.append("Scheme: "+scheme_key+"<br>");
		tb.append("<table>");
		int count = 0;
		
		for (L2Skill sk : CharSchemesTable.getInstance().getScheme(player.getObjectId(), scheme_key))
		{
			if (count == 0)
				tb.append("<tr>");
			
			if (sk.getId() <= 100)
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill00"+(sk.getId())+"\" fore=\"icon.skill00"+(sk.getId())+"\"></td>");
			else if (sk.getId() <= 1000)
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill0"+(sk.getId())+"\" fore=\"icon.skill0"+(sk.getId())+"\"></td>");
			else
				tb.append("<td><button value=\"\" action=\"bypass -h npc_%objectId%_skillselect "+skill_group+" "+scheme_key+" "+String.valueOf(sk.getId())+"\" width=32 height=32 back=\"icon.skill"+(sk.getId())+"\" fore=\"icon.skill"+(sk.getId())+"\"></td>");
			
			count++;
			if (count == 4)
			{
				tb.append("</tr>");
				count = 0;
			}
		}
		if (!tb.toString().endsWith("<tr>")) tb.append("<tr>");
		tb.append("</table>");
		
		return tb.toString();
	}
	
	/**
	 * 
	 * @param scheme_key
	 * @return an string with skill_groups table.
	 */
	private static String getTypesFrame(String scheme_key)
	{
		TextBuilder tb = new TextBuilder();
		tb.append("<table>");
		int count = 0;
		
		if (scheme_key == null) scheme_key = "unselected";
		
		for (String s : BufferSkillsTable.getInstance().getSkillsTypeList())
		{
			if (count == 0) tb.append("<tr>");
			tb.append("<td width=\"90\"><a action=\"bypass -h npc_%objectId%_editscheme "+s+" "+scheme_key+"\">"+s+"</a></td>");
			if (count == 2)
			{
				tb.append("</tr>");
				count = -1;
			}
			count++;			
		}
		if (!tb.toString().endsWith("</tr>")) tb.append("</tr>");
		tb.append("</table>");
		
		return tb.toString();
	}
	
	/**
	 * 
	 * @param list
	 * @return fee for all skills contained in list. 
	 */
	private static int getFee(FastList<L2Skill> list)
	{
		int fee = 0;
		if (Config.NPCBUFFER_STATIC_BUFF_COST >= 0) 
			return (list.size()* Config.NPCBUFFER_STATIC_BUFF_COST);
		for (L2Skill sk : list)
		{
			fee += BufferSkillsTable.getInstance().getSkillFee(sk.getId());
		}		
     return fee;
	}
}