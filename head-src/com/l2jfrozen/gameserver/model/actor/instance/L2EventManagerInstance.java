package com.l2jfrozen.gameserver.model.actor.instance;

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */



import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.model.entity.event.ArenaEvent;
import com.l2jfrozen.gameserver.model.entity.event.CTF;
import com.l2jfrozen.gameserver.model.entity.event.DM;
import com.l2jfrozen.gameserver.model.entity.event.LastManStanding;
import com.l2jfrozen.gameserver.model.entity.event.Raid;
import com.l2jfrozen.gameserver.model.entity.event.TownWarEventNew;
import com.l2jfrozen.gameserver.model.entity.event.TvT;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import com.l2jfrozen.gameserver.network.serverpackets.StatusUpdate;
import com.l2jfrozen.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfrozen.gameserver.templates.L2NpcTemplate;
import com.l2jfrozen.util.random.Rnd;


/**
 * <b><font size=3>NPC Buffer instance handler</font></b><br>
 * <br>
 * This class contains some methods that can be sorted by different types and functions:<br><br>
 * 
 * - Methods that overrides to superclass' (L2FolkInstance):
 *  <li>onAction
 *  <li>onBypassFeedback 
 *  <li>onActionShift
 *  <br><br>
 * 
 * - Methods to show html windows: 
 * <li>showGiveBuffsWindow
 * <li>showManageSchemeWindow
 * <li>showEditSchemeWindow
 * <br></br>
 * 
 * - Methods to get and build info (Strings, future html content) from character schemes, state, etc.
 * <li>getPlayerSchemeListFrame: Returns a table with player's schemes names
 * <li>getGroupSkillListFrame: Returns a table with skills available in the skill_group
 * <li>getPlayerSkillListFrame: Returns a table with skills already in player's scheme (scheme_key)
 * 		
 * <br>
 * <br>
 * @author  House
 */

public class L2EventManagerInstance extends L2NpcInstance
{
	private static final String PARENT_DIR = "data/html/mods/";
	
	public L2EventManagerInstance(int objectId, L2NpcTemplate template)
	{
		super(objectId, template);
	}
	
	@Override
	public void onAction(L2PcInstance player)
	{
		if (!canTarget(player) || System.currentTimeMillis() - player.getTimerToAttack() < Config.CLICK_TASK)
			return;
		
		if (Olympiad.getInstance().isRegistered(player))
		{
			player.sendMessage("You cannot use the event manager while registered in Olympiad.");
			player.sendPacket(ActionFailed.STATIC_PACKET);
			return;
		}
		
		// Check if the L2PcInstance already target the L2NpcInstance
		if (this != player.getTarget())
		{
			if (Config.DEBUG)
			{
				_log.fine("new target selected:" + getObjectId());
			}
			
			// Set the target of the L2PcInstance player
			player.setTarget(this);
			
			// Check if the player is attackable (without a forced attack)
			if (isAutoAttackable(player))
			{
				// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
				// The player.getLevel() - getLevel() permit to display the correct color in the select window
				MyTargetSelected my = new MyTargetSelected(getObjectId(), player.getLevel() - getLevel());
				player.sendPacket(my);
				my = null;
				
				// Send a Server->Client packet StatusUpdate of the L2NpcInstance to the L2PcInstance to update its HP bar
				StatusUpdate su = new StatusUpdate(getObjectId());
				su.addAttribute(StatusUpdate.CUR_HP, (int) getCurrentHp());
				su.addAttribute(StatusUpdate.MAX_HP, getMaxHp());
				player.sendPacket(su);
				su = null;
			}
			else
			{
				// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
				MyTargetSelected my = new MyTargetSelected(getObjectId(), 0);
				player.sendPacket(my);
				my = null;
			}
			
			player.setTimerToAttack(System.currentTimeMillis());
			// Send a Server->Client packet ValidateLocation to correct the L2NpcInstance position and heading on the client
			player.sendPacket(new ValidateLocation(this));
		}
		else
		{
			
			player.sendPacket(new ValidateLocation(this));
			
			// Check if the player is attackable (without a forced attack) and isn't dead
			if (isAutoAttackable(player) && !isAlikeDead())
			{
				// Check the height difference
				if (Math.abs(player.getZ() - getZ()) < 400) // this max heigth difference might need some tweaking
				{
					// Set the L2PcInstance Intention to AI_INTENTION_ATTACK
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_ATTACK, this);
					// player.startAttack(this);
				}
				else
				{
					// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
					player.sendPacket(ActionFailed.STATIC_PACKET);
				}
			}
			else if (!isAutoAttackable(player))
			{
				// Calculate the distance between the L2PcInstance and the L2NpcInstance
				if (!canInteract(player))
				{
					// Notify the L2PcInstance AI with AI_INTENTION_INTERACT
					player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
				}
				else
				{
					// Like L2OFF if char is dead, is sitting, is in trade or is in fakedeath can't interact with npc
					if (player.isSitting() || player.isDead() || player.isFakeDeath() || player.getActiveTradeList() != null)
						return;
					
					// Send a Server->Client packet SocialAction to the all L2PcInstance on the _knownPlayer of the L2NpcInstance to display a social action of the L2NpcInstance on their client
					SocialAction sa = new SocialAction(getObjectId(), Rnd.get(8));
					broadcastPacket(sa);
					
					if (TvT._isEventMobTvT)
					{
						TvT.showEventHtml(player, String.valueOf(getObjectId()));
					}
					else if (DM._isEventMobDM)
					{
						DM.showEventHtml(player, String.valueOf(this.getObjectId()));
					}
					else if (CTF._isEventMobCTF)
					{
						CTF.showEventHtml(player, String.valueOf(getObjectId()));
					}
					else if (Raid._isEventMobRaid)
					{
						Raid.showEventHtml(player, String.valueOf(getObjectId()));
					}
					else if (TownWarEventNew._inTWPeriod)
					{
						NpcHtmlMessage html = new NpcHtmlMessage(1);
						html.setFile(PARENT_DIR+"TW.htm");
						sendHtmlMessage(player, html);
					}
					else if (ArenaEvent._inCompPeriod)
					{
						NpcHtmlMessage html = new NpcHtmlMessage(1);
						html.setFile(PARENT_DIR+"2vs2.htm");
						sendHtmlMessage(player, html);
					}
					else if (LastManStanding.isActive())
					{
						if(!player.isAio())
					    {
						  if (LastManStanding.started)
						  {
							LastManStanding.showEventHtmlStarted(player, String.valueOf(this.getObjectId()));
						  }
						  else if(!LastManStanding.players.contains(player) && LastManStanding.isActive())
						  {
						    LastManStanding.showEventHtml(player, String.valueOf(this.getObjectId()));
						  }
						  else
						  {
						    LastManStanding.showEventHtmlJoined(player, String.valueOf(this.getObjectId()));
					      }
					   }
					  else
					   LastManStanding.showAioHtml(player, String.valueOf(this.getObjectId()));
					}
				else
				{
					NpcHtmlMessage html = new NpcHtmlMessage(1);
					html.setFile(PARENT_DIR+"EventManager.htm");
					sendHtmlMessage(player, html);
				}
				// Like L2OFF player must rotate to the Npc
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_FOLLOW, this);
			}
			
			// to avoid player stuck
			player.sendPacket(ActionFailed.STATIC_PACKET);
		}
		else
		{
			player.sendPacket(ActionFailed.STATIC_PACKET);
		}
	}
}
	
	private void sendHtmlMessage(L2PcInstance player, NpcHtmlMessage html)
    {
        html.replace("%objectId%", String.valueOf(getObjectId()));
        html.replace("%npcId%", String.valueOf(getNpcId()));
        player.sendPacket(html);
    }
}