/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.model.actor.instance;

import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.managers.CastleManager;
import com.l2jfrozen.gameserver.model.entity.siege.Castle;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jfrozen.gameserver.network.serverpackets.SiegeInfo;
import com.l2jfrozen.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfrozen.gameserver.templates.L2NpcTemplate;



public class L2SiegeNpcInstance extends L2NpcInstance
{
	private final int GLUDIO_MESSENGER = 35104;
    private final int DION_MESSENGER = 35146;
    private final int GIRAN_MESSENGER = 35188;
    private final int OREN_MESSENGER = 35232;
    private final int ADEN_MESSENGER = 35278;
    private final int INNADRIL_MESSENGER = 35320;
    private final int GODDARD_MESSENGER = 35367;
    private final int RUNE_MESSENGER = 35513;
    private final int SCHUTTGART_MESSENGER = 35559;
	
	public L2SiegeNpcInstance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}
	
	@Override
	public void onAction(L2PcInstance player)
	{
		if(!canTarget(player))
			return;

		// Check if the L2PcInstance already target the L2NpcInstance
		if(this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);

			// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
			MyTargetSelected my = new MyTargetSelected(getObjectId(), 0);
			player.sendPacket(my);
			my = null;

			player.sendPacket(new ValidateLocation(this));
		}
		else
		{
			// Calculate the distance between the L2PcInstance and the L2NpcInstance
			if(!canInteract(player))
			{
				// Notify the L2PcInstance AI with AI_INTENTION_INTERACT
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
			}
			else
			{
					showChatWindow(player);
			}
		}
		// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{
		if(command.startsWith("Gludio"))
		{
			showChatWindow(player);
		}
		return;
	}
	
	@Override
	public void showChatWindow(L2PcInstance player)
	{
		Castle castle = null;
		switch (getNpcId())
        {
                case GLUDIO_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(1);
                        break;
                case DION_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(2);
                        break;
                case GIRAN_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(3);
                        break;
                case OREN_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(4);
                        break;
                case ADEN_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(5);
                        break;
                case INNADRIL_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(6);
                        break;
                case GODDARD_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(7);
                        break;
                case RUNE_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(8);
                        break;
                case SCHUTTGART_MESSENGER:
                        castle = CastleManager.getInstance().getCastleById(9);
                        break;
        }
        //Shouldnt be necessary
        if(castle != null)
                player.sendPacket(new SiegeInfo(castle));
       
        return;
	}
}