/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.actor.instance;

import java.util.StringTokenizer;

import javolution.text.TextBuilder;

import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.datatables.sql.ItemTable;
import com.l2jfrozen.gameserver.datatables.xml.ExperienceData;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.MyTargetSelected;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.network.serverpackets.UserInfo;
import com.l2jfrozen.gameserver.network.serverpackets.ValidateLocation;
import com.l2jfrozen.gameserver.templates.L2NpcTemplate;

/**
 * This class ...
 * @author -Sensei-
 */
public class L2DelevelV2Instance extends L2FolkInstance
{
	
	private static final int MinLevel = 10; // Minimum Level, (e.g if you set 10, players wont be able to be level 9).
	private static final int ItemConsumeId = 57; // Item Consume id
	private int levels ; // Item Consume id
	private static final int ItemConsumeNumEveryLevel = 100; // Item ItemConsumeNumEveryLevel
	private static String ItemName = ItemTable.getInstance().createDummyItem(ItemConsumeId).getItemName();
	private static final String PARENT_DIR = "data/html/mods/";

	public L2DelevelV2Instance(int objectID, L2NpcTemplate template)
	{
		super(objectID, template);
	}
	
	@Override
	public void onBypassFeedback(L2PcInstance player, String command)
	{	
		StringTokenizer st = new StringTokenizer(command, " ");
	    String actualCommand = st.nextToken();
	    
	    String val = "";
		if(st.countTokens() >= 1)
		{
			val = st.nextToken();
		}
		
		if(actualCommand.equalsIgnoreCase("delevel"))
		{
			try
				{
				final byte lvl = Byte.parseByte(val);
				int k = player.getLevel();
				levels = k - lvl;
				
				    if (lvl > 80)
				    {
					player.sendMessage("You must specify a level lower than 80.");
					player.sendPacket(ActionFailed.STATIC_PACKET);
					return;
				    }
					if (player.getInventory().getItemByItemId(ItemConsumeId) == null)
					{
						player.sendMessage("You don't have enough adena!");
						player.sendPacket(ActionFailed.STATIC_PACKET);
			        	return;
					}
					if (player.isAio())
					{
			    		aio(player);
			    		player.sendPacket(ActionFailed.STATIC_PACKET);
			        	return;
					}
					if (player.getKarma() > 0)
					{
			    		karma(player);
			    		player.sendPacket(ActionFailed.STATIC_PACKET);
			        	return;
					}
					if (lvl < 10)
					{
						player.sendMessage("Incorrect Level Number!");
						player.sendPacket(ActionFailed.STATIC_PACKET);
						return;
					}
					if (lvl < MinLevel)
					{
						player.sendMessage("Incorrect Level Number!");
						player.sendPacket(ActionFailed.STATIC_PACKET);
						return;
					}
					if (player.getLevel() <= lvl)
					{
						player.sendMessage("Your level is already lower.");
						player.sendPacket(ActionFailed.STATIC_PACKET);
						return;
					}
					if (player.getInventory().getItemByItemId(ItemConsumeId).getCount() < ItemConsumeNumEveryLevel*levels)
					{
						
						player.sendMessage("You don't have the enough requirments!");
						player.sendPacket(ActionFailed.STATIC_PACKET);
						return;
					}
					if (lvl >= 1 && player.getInventory().getItemByItemId(ItemConsumeId).getCount() >= ItemConsumeNumEveryLevel)
					{	
						final long pXp = player.getStat().getExp();
						final long tXp = ExperienceData.getInstance().getExpForLevel(lvl);
						player.getStat().removeExpAndSp(pXp - tXp, 0);
						player.sendMessage("Congratulations! You are now level "+lvl+".");
						player.broadcastStatusUpdate();
						player.broadcastUserInfo();
						player.sendPacket(new UserInfo(player));
						player.sendSkillList();
						NpcHtmlMessage html = new NpcHtmlMessage(1);
					    html.setFile(PARENT_DIR+"delevel.htm");
					    sendHtmlMessage(player, html);
					    levels = k - lvl;
					    player.destroyItemByItemId("delevel", ItemConsumeId, ItemConsumeNumEveryLevel*levels, player, true);
					}
					else
					{
						player.sendMessage("You need to specify a level");
						player.sendPacket(ActionFailed.STATIC_PACKET);
						return;
					}
			}
			catch(final NumberFormatException e)
				{
				  player.sendMessage("You need to specify a level");
				}
		    }
	    }
	
	/**
	 * this is called when a player interacts with this NPC
	 * 
	 * @param player
	 */
	@Override
	public void onAction(L2PcInstance player)
	{
		if(!canTarget(player))
			return;

		// Check if the L2PcInstance already target the L2NpcInstance
		if(this != player.getTarget())
		{
			// Set the target of the L2PcInstance player
			player.setTarget(this);

			// Send a Server->Client packet MyTargetSelected to the L2PcInstance player
			MyTargetSelected my = new MyTargetSelected(getObjectId(), 0);
			player.sendPacket(my);
			my = null;

			player.sendPacket(new ValidateLocation(this));
		}
		else
		{
			// Calculate the distance between the L2PcInstance and the L2NpcInstance
			if(!canInteract(player))
			{
				// Notify the L2PcInstance AI with AI_INTENTION_INTERACT
				player.getAI().setIntention(CtrlIntention.AI_INTENTION_INTERACT, this);
			}
			else
			{
				NpcHtmlMessage html = new NpcHtmlMessage(1);
			    html.setFile(PARENT_DIR+"delevel.htm");
			    sendHtmlMessage(player, html);
			}
		}
		// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
		player.sendPacket(ActionFailed.STATIC_PACKET);
	}

	private void aio(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>Aio buffers are not allowed to delevel!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	private void karma(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<body>");
		sb.append("<font color=\"LEVEL\"><center>Rapana the xp eater:<br></center></font>");
		sb.append("<br>");
		sb.append("<center>Players with karma cannot delevel!</center>");
		sb.append("<br>");
		sb.append("</body>");
		sb.append("</html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
		}
	
	@SuppressWarnings("unused")
	private void showhtml(L2PcInstance player)
	{
		NpcHtmlMessage html = new NpcHtmlMessage(getObjectId());
		TextBuilder sb = new TextBuilder();
		sb.append("<html>");
		sb.append("<title>%player%</title>");
		sb.append("<body><center>");
		sb.append("<img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center><br>");
		sb.append("Hello there, if you want to low your current level, you came to the right place!<br>");
		sb.append("You have just to choose the <font color=FF0000>number</font> of level you would like to be.<br>");
		sb.append("In exchange, i am going to take you <font color=LEVEL>1000 Adena</font> for <font color=LEVEL>each level");
		sb.append("</font> you decrease.<br>");
		sb.append("Remember: You can't choose a number higher than your current level.<br>");

		sb.append("<table width=270><tr>");
		sb.append("<td><edit var=\"lv\" width=50></td>");
		sb.append("<td><button value=\"Change my level to: \" action=\"bypass -h npc_" + getObjectId() + "_delevel $lv\" width=180 height=21 back=\"L2UI_CT1.Button_DF_Down\" fore=\"L2UI_CT1.Button_DF\"></td>");
		sb.append("</tr></table>");


		sb.append("<br><br><br><br>");
		sb.append("<center>");
		sb.append("<img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center>");
		sb.append("</body></html>");
		html.setHtml(sb.toString());
		player.sendPacket(html);
		html = null;
		sb = null;
	}
	
	private void sendHtmlMessage(L2PcInstance player, NpcHtmlMessage html)
    {
        html.replace("%objectId%", String.valueOf(getObjectId()));
        html.replace("%npcId%", String.valueOf(getNpcId()));
        html.replace("%player%", String.valueOf(player.getName()));
        html.replace("%player%", String.valueOf(player.getName()));
        html.replace("%player%", String.valueOf(player.getName()));
        html.replace("%itemname%", ItemName);
		html.replace("%price%", ""+ItemConsumeNumEveryLevel+"");
        player.sendPacket(html);
    }
}

