/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.entity;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javolution.text.TextBuilder;
import javolution.util.FastList;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.cache.HtmCache;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.clientpackets.Say2;
import com.l2jfrozen.gameserver.network.serverpackets.CreatureSay;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.gameserver.script.DateRange;
import com.l2jfrozen.gameserver.util.Broadcast;
import com.l2jfrozen.util.StringUtil;

/**
 * @author ProGramMoS
 * @version 1.6
 */
public class Announcements
{
	private static Logger _log = Logger.getLogger(Announcements.class.getName());

	private static Announcements _instance;
	private List<String> _announcements = new FastList<String>();
	private List<String> _critAnnouncements = new FastList<String>();
	private List<List<Object>> _eventAnnouncements = new FastList<List<Object>>();

	public Announcements()
	{
		loadAnnouncements();
	}

	public static Announcements getInstance()
	{
		if(_instance == null)
		{
			_instance = new Announcements();
		}

		return _instance;
	}

	public void loadAnnouncements()
	{
		_announcements.clear();
		//File file = new File(Config.DATAPACK_ROOT, "data/announcements.txt");

		//if(file.exists())
		//{
			//readFromDisk(file);
		//}
		//else
		//{
			//_log.config("data/announcements.txt doesn't exist");
		//}
		_critAnnouncements.clear();
		readFromDisk("data/announcements.txt", _announcements);
		readFromDisk("data/critannouncements.txt", _critAnnouncements);
				
		if (Config.DEBUG)
		_log.info("Announcements: Loaded " + (_announcements.size() + _critAnnouncements.size())  + " announcements.");

	}

	public void showAnnouncements(L2PcInstance activeChar)
	{
		//for(int i = 0; i < _announcements.size(); i++)
		for (String announce : _announcements)
		{
			//CreatureSay cs = new CreatureSay(0, Say2.ANNOUNCEMENT, activeChar.getName(), _announcements.get(i).replace("%name%", activeChar.getName()));
			CreatureSay cs = new CreatureSay(0, Say2.ANNOUNCEMENT, activeChar.getName(), announce);
			activeChar.sendPacket(cs);
			cs = null;
		}

		//for(int i = 0; i < _eventAnnouncements.size(); i++)
		for (String critAnnounce : _critAnnouncements)
		{
			//List<Object> entry = _eventAnnouncements.get(i);
			CreatureSay cs = new CreatureSay(0, Say2.CRITICAL_ANNOUNCE, activeChar.getName(), critAnnounce);
			activeChar.sendPacket(cs);
			}
					
				for (List<Object> eventAnnounce : _eventAnnouncements)
			{
			List<Object> entry = eventAnnounce;

			DateRange validDateRange = (DateRange) entry.get(0);
			String[] msg = (String[]) entry.get(1);
			Date currentDate = new Date();

			if(!validDateRange.isValid() || validDateRange.isWithinRange(currentDate))
			{
				SystemMessage sm = new SystemMessage(SystemMessageId.S1_S2);

				for(String element : msg)
				{
					sm.addString(element);
				}

				activeChar.sendPacket(sm);
				sm = null;
			}

			entry = null;
			validDateRange = null;
			msg = null;
			currentDate = null;
		}
	}

	public void addEventAnnouncement(DateRange validDateRange, String[] msg)
	{
		List<Object> entry = new FastList<Object>();
		entry.add(validDateRange);
		entry.add(msg);
		_eventAnnouncements.add(entry);

		entry = null;
	}

	public void listAnnouncements(L2PcInstance activeChar)
	{
		String content = HtmCache.getInstance().getHtmForce("data/html/admin/announce.htm");
		NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
		adminReply.setHtml(content);
		TextBuilder replyMSG = new TextBuilder("<br>");

		for(int i = 0; i < _announcements.size(); i++)
		{
			replyMSG.append("<table width=260><tr><td width=220>" + _announcements.get(i) + "</td><td width=40>");
			replyMSG.append("<button value=\"Delete\" action=\"bypass -h admin_del_announcement " + i + "\" width=60 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr></table>");
		}

		adminReply.replace("%announces%", replyMSG.toString());
		activeChar.sendPacket(adminReply);
	}
		//critical announcement
		public void listCritAnnouncements(L2PcInstance activeChar)
			{
				String content = HtmCache.getInstance().getHtmForce("data/html/admin/critannounce.htm");
				NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
				adminReply.setHtml(content);
				final StringBuilder replyMSG = StringUtil.startAppend(500, "<br>");
				for (int i = 0; i < _critAnnouncements.size(); i++)
				{
					StringUtil.append(replyMSG, "<table width=260><tr><td width=220>", _critAnnouncements.get(i), "</td><td width=40>"
							+ "<button value=\"Delete\" action=\"bypass -h admin_del_critannouncement ", String.valueOf(i), "\" width=60 height=20 back=\"sek.cbui94\" fore=\"sek.cbui92\"></td></tr></table>");
				}
				adminReply.replace("%critannounces%", replyMSG.toString());
				activeChar.sendPacket(adminReply);
			}

	public void addAnnouncement(String text)
	{
		_announcements.add(text);
		//saveToDisk();
		saveToDisk(false);
	}

	public void delAnnouncement(int line)
	{
		_announcements.remove(line);
		//saveToDisk();
		saveToDisk(false);
	}

	public void addCritAnnouncement(String text)
	{
		_critAnnouncements.add(text);
				saveToDisk(true);
			}
			
			public void delCritAnnouncement(int line)
			{
				_critAnnouncements.remove(line);
				saveToDisk(true);
			}
			
			@SuppressWarnings("null")
			private void readFromDisk(String path, List<String> list)
			{
				File file = new File(Config.DATAPACK_ROOT, path);
				
				if (file.exists())
		//LineNumberReader lnr = null;
		//FileReader reader = null;
		//try
		{
			//int i = 0;

			//String line = null;
			//reader = new FileReader(file);
			//lnr = new LineNumberReader(reader);

			//while((line = lnr.readLine()) != null)
			LineNumberReader lnr = null;
			try
			{
				//StringTokenizer st = new StringTokenizer(line, "\n\r");
				//if(st.hasMoreTokens())
				String line = null;
				lnr = new LineNumberReader(new FileReader(file));
				while ((line = lnr.readLine()) != null)
				{
					//String announcement = st.nextToken();
					//_announcements.add(announcement);

					//i++;
			StringTokenizer st = new StringTokenizer(line, "\n\r");
					if (st.hasMoreTokens())
				{
					String announcement = st.nextToken();
					list.add(announcement);
				}
				}
			}
			catch (IOException e1)
			{
			_log.log(Level.SEVERE, "Error reading announcements: ", e1);
			}
			finally
			{
				try
								{
									lnr.close();
								}
								catch (Exception e2)
								{
									// nothing
								}
			}
		}
				else
				_log.warning(file.getAbsolutePath() + " doesn't exist");
	}

	//private void saveToDisk()
	private void saveToDisk(boolean isCritical)
	{
		//File file = new File("data/announcements.txt");
		String path;
		List<String> list;
				
		if (isCritical)
		{
			path = "data/critannouncements.txt";
					list = _critAnnouncements;
	    }
		else
		{
			path = "data/announcements.txt";
			list = _announcements;
		}
			
				File file = new File(path);
		FileWriter save = null;

		try
		{
			save = new FileWriter(file);
			//for(int i = 0; i < _announcements.size(); i++)
			for (String announce : list)
			{
				//save.write(_announcements.get(i));
				save.write(announce);
				save.write("\r\n");
			}
			save.flush();
		}
		catch(IOException e)
		{
			if(Config.ENABLE_ALL_EXCEPTIONS)
				e.printStackTrace();
			
			_log.warning("saving the announcements file has failed: " + e);
		}finally{
			
			if(save != null)
				try
				{
					save.close();
				}
				catch(IOException e)
				{
					e.printStackTrace();
				}
		}

	}

	public void announceToAll(String text)
	{
		CreatureSay cs = new CreatureSay(0, Say2.ANNOUNCEMENT, "", text);

		for(L2PcInstance player : L2World.getInstance().getAllPlayers())
		{
			player.sendPacket(cs);
		}

		cs = null;
	}	
	
	// Colored Announcements 8D
	public void gameAnnounceToAll(String text)
	{
		CreatureSay cs = new CreatureSay(0, 18, "", text);

		for(L2PcInstance player : L2World.getInstance().getAllPlayers())
		{
			if(player != null)
				if(player.isOnline()!=0)
					player.sendPacket(cs);
		}

		cs = null;
	}
	
	public void announceToAll(String text, boolean isCritical)
	{
		Broadcast.announceToOnlinePlayers(text, isCritical);
 	}

	public void announceToAll(SystemMessage sm)
	{
		for(L2PcInstance player : L2World.getInstance().getAllPlayers())
		{
			player.sendPacket(sm);
		}
	}

	// Method fo handling announcements from admin
	public void handleAnnounce(String command, int lengthToTrim, boolean isCritical)
	{
		try
		{
			// Announce string to everyone on server
			String text = command.substring(lengthToTrim);
			Announcements.getInstance().announceToAll(text, isCritical);
			text = null;
		}

		// No body cares!
		catch(StringIndexOutOfBoundsException e)
		{
			// empty message.. ignore
			if(Config.ENABLE_ALL_EXCEPTIONS)
				e.printStackTrace();
		}
	}
}
