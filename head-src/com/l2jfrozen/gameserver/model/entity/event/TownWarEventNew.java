/* 
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.model.entity.event;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.managers.TownManager;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventManager;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventTask;
import com.l2jfrozen.gameserver.model.zone.type.L2TownZone;



/**
 * @author Sensei
 *
 */
public class TownWarEventNew implements EventTask
{
	
	/** The Constant LOGGER. */
	protected static final Logger LOGGER = LoggerFactory.getLogger(Raid.class.getName());
	
	public static String _eventName = "Town War";
	
	public static boolean _inTWPeriod;
	
	private String startEventTime;
	
	public static int _eventTime = EventManager.TW_TIME;
	
	@SuppressWarnings("deprecation")
	public static void eventOnceStart()
	{
				if(Config.TW_ALL_TOWNS)
				{
					TownManager.getInstance().getTown(1).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(2).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(3).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(4).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(5).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(6).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(7).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(8).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(9).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(10).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(11).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(12).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(13).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(14).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(15).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(16).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(17).setParameter("noPeace", "true");
					TownManager.getInstance().getTown(19).setParameter("noPeace", "true");
				}
				
				// A Town will become War Zone
				if(!Config.TW_ALL_TOWNS && Config.TW_TOWN_ID != 18 && Config.TW_TOWN_ID != 21 && Config.TW_TOWN_ID != 22)
				{
					TownManager.getInstance().getTown(Config.TW_TOWN_ID).setParameter("noPeace", "true");
				}

				Collection<L2PcInstance> pls = L2World.getInstance().getAllPlayers();
				{
					int x,y,z;
					L2TownZone Town;
					byte zonaPaz = 1;

					for(L2PcInstance onlinePlayer : pls)
						if(onlinePlayer.isOnline() == 1 )
						{
							x = onlinePlayer.getX();
							y = onlinePlayer.getY();
							z = onlinePlayer.getZ();

							Town = TownManager.getInstance().getTown(x, y, z);
							if(Town != null)
							{
								if(Town.getTownId() == Config.TW_TOWN_ID && !Config.TW_ALL_TOWNS)
								{
									onlinePlayer.setInsideZone(zonaPaz, false);
									onlinePlayer.revalidateZone(true);
								}
								else if(Config.TW_ALL_TOWNS)
								{
									onlinePlayer.setInsideZone(zonaPaz, false);
									onlinePlayer.revalidateZone(true);
								}
							}
								onlinePlayer.setInTownWar(true);
							}
				}
				_inTWPeriod = true;
				// Announce for all towns
				if(Config.TW_ALL_TOWNS)
				{
					Announcements.getInstance().gameAnnounceToAll("Town War Event started!");
					Announcements.getInstance().gameAnnounceToAll("All towns have been set to war zone.");
				}
				
				// Announce for one town
				if(!Config.TW_ALL_TOWNS)
				{
					Announcements.getInstance().announceToAll("Town War Event Started!",true);
					Announcements.getInstance().announceToAll(TownManager.getInstance().getTown(Config.TW_TOWN_ID).getName() + " has been set to war zone.",true);
				}
				if (_inTWPeriod)
				{
				waiter(_eventTime * 60 * 1000); // minutes for event time
				finishEvent();
				_inTWPeriod = false;
				}
	        }	
	
	private static void waiter(long interval)
	{
		long startWaiterTime = System.currentTimeMillis();


		while (startWaiterTime + interval > System.currentTimeMillis())
		{
			long startOneSecondWaiterStartTime = System.currentTimeMillis();

			// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
			while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
			{
				try
				{
					Thread.sleep(1);
				}
				catch (InterruptedException ie)
				{
				}
			}
		}
	}
	
	    @SuppressWarnings("deprecation")
		public static void finishEvent()
		{
	    	if (Config.TW_ALL_TOWNS) {
	            TownManager.getInstance().getTown(1).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(2).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(3).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(4).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(5).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(6).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(7).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(8).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(9).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(10).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(11).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(12).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(13).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(14).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(15).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(16).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(17).setParameter("noPeace", "false");
	            TownManager.getInstance().getTown(19).setParameter("noPeace", "false");
	        }

	        // A Town will become Peace Zone
	        if (!Config.TW_ALL_TOWNS && Config.TW_TOWN_ID != 18 && Config.TW_TOWN_ID != 21 && Config.TW_TOWN_ID != 22) {
	            TownManager.getInstance().getTown(Config.TW_TOWN_ID).setParameter("noPeace", "false");
	        }

	        Collection<L2PcInstance> pls = L2World.getInstance().getAllPlayers();
	        {
	            int xx, yy, zz;
	            L2TownZone Town;
	            byte zonaPaz = 1;

	            for (L2PcInstance onlinePlayer : pls) {
	                if (onlinePlayer.isOnline() == 1) {
	                    xx = onlinePlayer.getX();
	                    yy = onlinePlayer.getY();
	                    zz = onlinePlayer.getZ();

	                    Town = TownManager.getInstance().getTown(xx, yy, zz);
	                    if (Town != null) {
	                        if (Town.getTownId() == Config.TW_TOWN_ID && !Config.TW_ALL_TOWNS) {
	                            onlinePlayer.setInsideZone(zonaPaz, true);
	                            onlinePlayer.revalidateZone(true);
	                        } else if (Config.TW_ALL_TOWNS) {
	                            onlinePlayer.setInsideZone(zonaPaz, true);
	                            onlinePlayer.revalidateZone(true);
	                        }
	                    }
	                    onlinePlayer.setInTownWar(false);
	                }
	            }
	        }

	        if (!Config.TW_ALL_TOWNS) 
	        {
				Announcements.getInstance().announceToAll("Town War Event ended!",true);
	            Announcements.getInstance().announceToAll(TownManager.getInstance().getTown(Config.TW_TOWN_ID).getName() + " has been set back to normal!",true);
	        }
	        LOGGER.info(_eventName + " Event Finished!");
		}
		public void setEventStartTime(String newTime)
		{
			startEventTime = newTime;
		}
		
		public static TownWarEventNew getNewInstance()
		{
			return new TownWarEventNew();
		}
		
		public static String get_eventName()
		{
			return _eventName;
		}

		@Override
		public void run() 
		{
			LOGGER.info(_eventName + " Event Started!");
			eventOnceStart();
		}

		@Override
		public String getEventIdentifier()
		{
			return _eventName;
		}

		@Override
		public String getEventStartTime()
		{
			return startEventTime;
		}
		
		public static void setEventTime(int time)
		{
			_eventTime = time;
		}
		
		public boolean inTWPeriod()
		{
			return _inTWPeriod;
		}
	}