package com.l2jfrozen.gameserver.model.entity.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventManager;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventTask;

/**
 * Event Arena Manager.
 * 
 * 
 * This Event works only in pairs. Two players must to do a party and the leader register in the event
 * if any member of party already registered, then the registrations fails. Only leader can removes
 * from event. anyone can see the battles except the registered player in event.
 * The battle only finish when only one party keep alive. In the end of each battle, winners earn a reward.
 * 
 * The event is open all the time. have a little interval between call of battles of 1 minute
 * 
 * @author Sensei
 *
 */
public class ArenaEvent implements EventTask
{
	/** The Constant LOGGER. */
	protected static final Logger LOGGER = LoggerFactory.getLogger(Raid.class.getName());
	public static String _eventName = "2vs2";
	private String startEventTime;
	public static int _eventTime = EventManager.ARENA_TIME;
	public static boolean _inCompPeriod;	
	
	public static void eventOnceStart()
    {
		Announcements.getInstance().announceToAll("Blademir: 2vs2 event is open Giran Harbor!",true);
		_inCompPeriod = true;
		
		if (_inCompPeriod)
		{
		waiter(_eventTime * 60 * 1000); // minutes for event time
		finish();
		}
    	
    }
	
	public static void finish()
	{
		_inCompPeriod = false;
    	Announcements.getInstance().announceToAll("Blademir: 2vs2 event has ended! Thank you to all participants!",true);
		LOGGER.info(_eventName + " Event Finished!");
	}
	
	private static void waiter(long interval)
	{
		long startWaiterTime = System.currentTimeMillis();


		while (startWaiterTime + interval > System.currentTimeMillis())
		{
			long startOneSecondWaiterStartTime = System.currentTimeMillis();

			// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
			while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
			{
				try
				{
					Thread.sleep(1);
				}
				catch (InterruptedException ie)
				{
				}
			}
		}
	}
	
	@Override
	public void run() 
	{
		LOGGER.info(_eventName + " Event Started!");
		eventOnceStart();
     }
	
	
	public void setEventStartTime(String newTime){
		startEventTime = newTime;
	}
	
	public static ArenaEvent getNewInstance()
	{
		return new ArenaEvent();
	}
	
	public static String get_eventName()
	{
		return _eventName;
	}

	@Override
	public String getEventIdentifier()
	{
		return _eventName;
	}

	@Override
	public String getEventStartTime()
	{
		return startEventTime;
	}
	
	public static void setEventTime(int time)
	{
		_eventTime = time;
	}
	
	public static boolean inCompPeriod()
	{
		return _inCompPeriod;
	}
}