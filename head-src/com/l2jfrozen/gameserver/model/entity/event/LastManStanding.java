/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.entity.event;

import java.util.List;
import java.util.Vector;

import javolution.text.TextBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.datatables.csv.DoorTable;
import com.l2jfrozen.gameserver.datatables.csv.MapRegionTable;
import com.l2jfrozen.gameserver.datatables.sql.ItemTable;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Party;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.base.ClassId;
import com.l2jfrozen.gameserver.model.entity.Announcements;
import com.l2jfrozen.gameserver.model.entity.event.manager.EventTask;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.model.spawn.L2Spawn;
import com.l2jfrozen.gameserver.network.clientpackets.Say2;
import com.l2jfrozen.gameserver.network.serverpackets.ActionFailed;
import com.l2jfrozen.gameserver.network.serverpackets.CreatureSay;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.util.random.Rnd;

/**
 * 
 * 
 * @author Kiannzell
 *
 */

public class LastManStanding implements EventTask
{
	public static int joined = 0;
	public static boolean started = false;
	public static int MinPlayers = Config.LMS_MIN_PLAYERS;
	public static int MaxPlayers = Config.LMS_MAX_PLAYERS;
	public static int _minlvl = Config.LMS_MIN_LVL;
	public static Vector<L2PcInstance> players = new Vector<L2PcInstance>();
	protected static final Logger LOGGER = LoggerFactory.getLogger(LastManStanding.class.getName());
	public static String _eventName = "LMS";
	private String startEventTime;
	public static int _joinTime = Config.LMS_REG_TIME;
	public static L2Spawn _npc;
	protected static StateEnum _state = StateEnum.INACTIVE;
    public static L2PcInstance winner = null;
	public static int itemID = Config.LMS_REWARD_ID;
	public static int itemCount = Config.LMS_REWARD_COUNT;
	enum StateEnum 
	{
		ACTIVE,
		INACTIVE
	}
	public static void SetActive() 
	{
		_state = StateEnum.ACTIVE;
	}
	public static void SetInActive() 
	{
		_state = StateEnum.INACTIVE;
	}
	public static boolean isActive()
	{
		return _state == StateEnum.ACTIVE;

	}
	public static boolean isInActive()
	{
		return _state == StateEnum.INACTIVE;
	}
	
	public static void AnnounceRegistration()
	{
		Announcements.getInstance().announceToAll("LMS: Last Man Standing event registration is opened for " + Config.LMS_REG_TIME + " minutes!",true);
		Announcements.getInstance().announceToAll("Reward: " + itemCount + " " + ItemTable.getInstance().getTemplate(itemID).getName(),true);
		Announcements.getInstance().announceToAll("LMS: Recruiting levels " + _minlvl + " to " + "80",true);
		Announcements.getInstance().announceToAll("LMS: Joinable in Giran harbor!",true);
	}
	public static void teleAllPlayers()
	{
		int offset = Config.LMS_SPAWN_OFFSET;
		for (L2PcInstance player : players)
		player.teleToLocation(Config.LMS_X + Rnd.get(offset),Config.LMS_Y + Rnd.get(offset),Config.LMS_Z);
		for (L2PcInstance player : players)
		player.setIsParalyzed(true);
		for (L2PcInstance player : players)
		player.setTeam(1);
		for (L2PcInstance player : players)	
		player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS: ", "In 1 minute you will be unparalyzed and you will start fighting.The last one who survives is the winner"));
		for (L2PcInstance player : players)	
		if(player.getParty() != null)
		{
			L2Party party = player.getParty();
			party.removePartyMember(player);
		}
	}
	
	public static void showAioHtml(L2PcInstance eventPlayer, String objectId)
	{
		try
		{
			NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			
			TextBuilder replyMSG = new TextBuilder("<html><title>" + _eventName + "</title><body>");
			replyMSG.append("<center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center><br1>");
			replyMSG.append("<center><font color=\"3366CC\">Current event:</font></center><br1>");
			replyMSG.append("<center>Name:&nbsp;<font color=\"00FF00\">" + _eventName + "</font></center><br1>");
			replyMSG.append("<center>Description:&nbsp;<font color=\"00FF00\"> Last Man Standing </font></center><br><br>");
			replyMSG.append("<center>Event Type:&nbsp;<font color=\"00FF00\"> Kill everyone </font></center><br><br>");
			replyMSG.append("<center>Your lvl: <font color=\"00FF00\">" + eventPlayer.getLevel() + "</font></center><br>");
			replyMSG.append("<center>Min lvl: <font color=\"00FF00\">" + _minlvl + "</font></center><br>");
			replyMSG.append("<center>Max lvl: <font color=\"00FF00\"> 80 </font></center><br>");
			replyMSG.append("<center>Participant(s): <font color=\"00FF00\">" + joined + "</font></center><br><br>");
			replyMSG.append("<center><font color=\"3366CC\">You're an aio buffer, you cannot join!</font></center><br1>");
			replyMSG.append("</body></html>");
			adminReply.setHtml(replyMSG.toString());
			eventPlayer.sendPacket(adminReply);
			
			// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
			eventPlayer.sendPacket(ActionFailed.STATIC_PACKET);
		}
		catch (Exception e) 
		{
            LOGGER.error("", e);

            LOGGER.info(_eventName + " Engine[showEventHtlm(" + eventPlayer.getName() + ", " + objectId + ")]: exception" + e.getMessage());
        }
	}
	
	public static void showEventHtml(L2PcInstance eventPlayer, String objectId)
	{
		try
		{
			NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			
			TextBuilder replyMSG = new TextBuilder("<html><title>" + _eventName + "</title><body>");
			replyMSG.append("<center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center><br1>");
			replyMSG.append("<center><font color=\"3366CC\">Current event:</font></center><br1>");
			replyMSG.append("<center>Name:&nbsp;<font color=\"00FF00\">" + _eventName + "</font></center><br1>");
			replyMSG.append("<center>Description:&nbsp;<font color=\"00FF00\"> Last Man Standing </font></center><br><br>");
			replyMSG.append("<center>Event Type:&nbsp;<font color=\"00FF00\"> Kill everyone </font></center><br><br>");
			replyMSG.append("<center>Your lvl: <font color=\"00FF00\">" + eventPlayer.getLevel() + "</font></center><br>");
			replyMSG.append("<center>Min lvl: <font color=\"00FF00\">" + _minlvl + "</font></center><br>");
			replyMSG.append("<center>Max lvl: <font color=\"00FF00\"> 80 </font></center><br>");
			replyMSG.append("<center>Participant(s): <font color=\"00FF00\">" + joined + "</font></center><br><br>");
			replyMSG.append("<center><button value=\"Join\" action=\"bypass -h voice .lmsJOIN\" width=50 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></center><br>");	
			replyMSG.append("</body></html>");
			adminReply.setHtml(replyMSG.toString());
			eventPlayer.sendPacket(adminReply);
			
			// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
			eventPlayer.sendPacket(ActionFailed.STATIC_PACKET);
		}
		catch (Exception e) 
		{
            LOGGER.error("", e);

            LOGGER.info(_eventName + " Engine[showEventHtlm(" + eventPlayer.getName() + ", " + objectId + ")]: exception" + e.getMessage());
        }
	}
	
	public static void showEventHtmlJoined(L2PcInstance eventPlayer, String objectId)
	{
		try
		{
			NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			
			TextBuilder replyMSG = new TextBuilder("<html><title>" + _eventName + "</title><body>");
			replyMSG.append("<center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center><br1>");
			replyMSG.append("<center><font color=\"3366CC\">Current event:</font></center><br1>");
			replyMSG.append("<center>Name:&nbsp;<font color=\"00FF00\">" + _eventName + "</font></center><br1>");
			replyMSG.append("<center>Description:&nbsp;<font color=\"00FF00\"> Last Man Standing </font></center><br><br>");
			replyMSG.append("<center>Event Type:&nbsp;<font color=\"00FF00\"> Kill everyone </font></center><br><br>");
			replyMSG.append("<center>Your lvl: <font color=\"00FF00\">" + eventPlayer.getLevel() + "</font></center><br>");
			replyMSG.append("<center>Min lvl: <font color=\"00FF00\">" + _minlvl + "</font></center><br>");
			replyMSG.append("<center>Max lvl: <font color=\"00FF00\"> 80 </font></center><br>");
			replyMSG.append("<center>Participant(s): <font color=\"00FF00\">" + joined + "</font></center><br><br>");
			replyMSG.append("<center><button value=\"Leave\" action=\"bypass -h voice .lmsLEAVE\" width=50 height=15 back=\"sek.cbui94\" fore=\"sek.cbui92\"></center><br><br><br>");	
			replyMSG.append("</body></html>");
			adminReply.setHtml(replyMSG.toString());
			eventPlayer.sendPacket(adminReply);
			
			// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
			eventPlayer.sendPacket(ActionFailed.STATIC_PACKET);
		}
		catch (Exception e) 
		{
            LOGGER.error("", e);

            LOGGER.info(_eventName + " Engine[showEventHtlm(" + eventPlayer.getName() + ", " + objectId + ")]: exception" + e.getMessage());
        }
	}
	
	public static void showEventHtmlStarted(L2PcInstance eventPlayer, String objectId)
	{
		try
		{
			NpcHtmlMessage adminReply = new NpcHtmlMessage(5);
			
			TextBuilder replyMSG = new TextBuilder("<html><title>" + _eventName + "</title><body>");
			replyMSG.append("<center><img src=\"L2UI_CH3.herotower_deco\" width=256 height=32></center><br1>");
			replyMSG.append("<center><font color=\"3366CC\">Current event:</font></center><br1>");
			replyMSG.append("<center>Name:&nbsp;<font color=\"00FF00\">" + _eventName + "</font></center><br1>");
			replyMSG.append("<center>Description:&nbsp;<font color=\"00FF00\"> Last Man Standing </font></center><br><br>");
			replyMSG.append("<center>Event Type:&nbsp;<font color=\"00FF00\"> Kill everyone </font></center><br><br>");
			replyMSG.append("<center>Your lvl: <font color=\"00FF00\">" + eventPlayer.getLevel() + "</font></center><br>");
			replyMSG.append("<center>Min lvl: <font color=\"00FF00\">" + _minlvl + "</font></center><br>");
			replyMSG.append("<center>Max lvl: <font color=\"00FF00\"> 80 </font></center><br>");
			replyMSG.append("<center>Remaining Player(s): <font color=\"00FF00\">" + joined + "</font></center><br><br>");
			replyMSG.append("</body></html>");
			adminReply.setHtml(replyMSG.toString());
			eventPlayer.sendPacket(adminReply);
			
			// Send a Server->Client ActionFailed to the L2PcInstance in order to avoid that the client wait another packet
			eventPlayer.sendPacket(ActionFailed.STATIC_PACKET);
		}
		catch (Exception e) 
		{
            LOGGER.error("", e);

            LOGGER.info(_eventName + " Engine[showEventHtlm(" + eventPlayer.getName() + ", " + objectId + ")]: exception" + e.getMessage());
        }
	}
	
	public static void register(L2PcInstance player)
	{
		if(isActive() && joined < MaxPlayers && canRegister(player))
		{
		  addPlayer(player);
		}
		else if(joined == MaxPlayers)
		{
			player.sendMessage("There is no slot for you.There are Already" + MaxPlayers + "players that have joined.");
		}
		else if(!canRegister(player))
		{
			player.sendMessage("The attempt of you to register has failed.");
		}
		else 
		{
			player.sendMessage("There is no LastManStanding at the moment.");
		}
	}
	
	public static void addPlayer(L2PcInstance player)
	{
		players.add(player);
		joined++;
		player.sendMessage("LMS: Successfully registered.");
	}

	public static void unRegister(L2PcInstance player)
	{
	if(players.contains(player))
	{
		if (!started)
		{
		   players.remove(player);
		   joined--;
		   player.sendMessage("You've been removed from the event.");
		}
		else
			player.sendMessage("You cannot leave the event!");
		    return;
	}
	}
	
	public static void disconnect(L2PcInstance player)
	{	
	  if (player.isInLMS() || players.contains(player))
		{
			players.remove(player);
			joined--;
			player.setInLMS(false);
			player.setTeam(0);
			player.setIsParalyzed(false);
		}
			
	  if (started && player.isInLMS())
		  {
			announcewinnerDiscon();
		  }
	   }
	
	public static void prepare()
	{
		for (L2PcInstance player : players)
		player.setInLMS(true);
		for (L2PcInstance player : players)
		player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "In 2 minutes you will be teleported to the fighting area."));
		Announcements.getInstance().announceToAll("LMS: Event will start in 2 minutes!",true);
	}
	
    public static void endAndReward()
    {
    winner.addItem("LastManStanding", itemID, itemCount, null, true);
    winner.teleToLocation(46398, 187655, -3507);
    SetInActive();
	for (L2PcInstance player : players)
	player.setTeam(0);
    Announcements.getInstance().announceToAll("LMS: " + winner.getName() + " has won the Event!",true);
    Announcements.getInstance().announceToAll("LMS: Event is ended! Thank you to all participants!",true);
    for (L2PcInstance player : players)
	player.setInLMS(false);
    started = false;
    for (L2PcInstance player : players)
    player.setIsParalyzed(false);
    EventClear();
    openAdenColosseumDoors();
    }
    
    public static void tie()
    {
    	SetInActive();
    	for (L2PcInstance player : players)
    	player.setTeam(0);
    	for (L2PcInstance player : players)
    	player.teleToLocation(46398, 187655, -3507);
    	Announcements.getInstance().announceToAll("LMS: Event is ended in tie! Nobody won the event.",true);
    	for (L2PcInstance player : players)
    	player.setIsParalyzed(false);
    	for (L2PcInstance player : players)
    	player.setInLMS(false);
    	started = false;
    	EventClear();
    	openAdenColosseumDoors();
    }
    
    public static void removeNoneParticipantsInZone()
    {
    	for(L2PcInstance p : L2World.getInstance().getAllPlayers())
		{
		if (p.isInsideZone(L2Character.ZONE_EVENT))
		{
			if (!p.isInLMS())
			{
				p.teleToLocation(MapRegionTable.TeleportWhereType.Town);
				p.sendMessage("Coliseum will be use by LMS, you cannot stay.");
			}
		}
	}
   }
    
    public static void announcewinnerDiscon()
    {	
    	for(L2PcInstance p : L2World.getInstance().getAllPlayers())
		{
		if (p.isInsideZone(L2Character.ZONE_EVENT))
		{
			if (started && p.isInLMS())
			{
				if (joined < 2 || joined == 1 )
			    {
					LastManStanding.winner = p;
					for (L2PcInstance player : LastManStanding.players)
					player.sendMessage(LastManStanding.winner + " is the winner of the event.");
		            LastManStanding.endAndReward();
		            break;
			    }
			}
		}
	  }
    }
    
    public static void EventClear()
    {
        players.clear();
        winner = null;
        joined = 0;
    }
    	private static boolean canRegister(L2PcInstance player)
    	{
    		if (players.contains(player))
    		{
    			player.sendMessage("You already registered to the event!");
    			return false;
    		}
    		
    		if (started)
    		{
    			player.sendMessage("You cannot join anymore!");
    			return false;
    		}
    		
    		if (player.getClassId() == ClassId.cardinal || player.getClassId() == ClassId.evaSaint || player.getClassId() == ClassId.shillienSaint)
    		{
    			player.sendMessage("Healer class type are not allowed in this event!");
    			return false;
    		}
    		
    		if (player.getLevel() < _minlvl)
    		{
    			player.sendMessage("Your level is too low!");
    			return false;
    		}
    		
    		if(player._inEventCTF || player._inEventTvT || player._inEventRaid || player._inEventDM)
    		{
    			player.sendMessage("You already participated to another event!");
    			return false;
    		}
    		
    		if (Olympiad.getInstance().isRegistered(player) || player.isInOlympiadMode())
    		{
    			player.sendMessage("You already participated in Olympiad!");
    			return false;
    		}
    		
    		if (player.isInJail())
    		{
    			player.sendMessage("You cant register from the jail.");
    			return false;
    		}
    
    		if (player.isInOlympiadMode())
    		{
    			player.sendMessage("You cant register while you're in the olympiad.");
    			return false;
    		}
    
    		if (player.getKarma() > 0)
    		{
    			player.sendMessage("You cant register if you have karma.");
    			return false;
    		}
    
    		if (player.isCursedWeaponEquiped())
    		{
    			player.sendMessage("You cant register with a cursed weapon.");
    			return false;
    		}
    
    		if (player.isDead())
    		{
    			player.sendMessage("You cant register while you're dead.");
    			return false;
    		}
    		
    		if (!Config.Allow_Same_HWID_On_Events && player.getHWid() != null && player.getHWid().length() != 0)
    		{
    			List<String> players_in_boxes = player.active_boxes_characters;
    			L2PcInstance pc = null;
    			for(String character_name:  players_in_boxes)
    			{
    				pc = L2World.getInstance().getPlayer(character_name);
    				if (pc != null && player.getHWid().equals(pc.getHWid()))
    				{
    					if(players.contains(player))
    					{
    						player.sendMessage("You already participated in event with a different char!"); 
    						return false;
    					}
    				}
    			}
    		}
          return true;
    	}
    	
    	//Admin command to start
		public static void LMSrun()
    	 {
    		LOGGER.info(_eventName + " Event Started!");
    		removeNoneParticipantsInZone();
 	    	SetActive();
 	    	AnnounceRegistration();
 	    	eventWait(_joinTime * 60 * 1000);
 	    	if(checkMinPlayers(joined))
 	    	{
 	    	prepare();
 	    	started = true;
 	    	prepareWait(2 * 60 * 1000);
 	    	closeAdenColosseumDoors();
 	    	teleAllPlayers();
 	    	battleWait(1 * 60 * 1000);
 	    	for(L2PcInstance player : players)
 	    	player.setIsParalyzed(false);
 	    	for (L2PcInstance player : players)
 	 	    player.setInLMS(true);
 	    	for (L2PcInstance player : players)
 	    	player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event started! Go kill your enemies!"));
 	    	battleTime(Config.LMS_BATTLE_TIME * 60 * 1000);
 	    	ending();
 	    }
 	    else
 	    	{
 	    		Announcements.getInstance().announceToAll("LMS: Not enough players for the event. Min Requested : " + MinPlayers + ", Participating : " + joined,true);
 	    		SetInActive();
 	    	    EventClear();
 	    	   for (L2PcInstance player : players)
 	    	   player.setInLMS(false);
 	    	}
    	 }
    	
		public static void ending()
		{
			if (joined > 1)
		    {
		         tie();
		    }
	    }
		
		@Override
		public void run() 
		{
			LOGGER.info(_eventName + " Event Started!");
			removeNoneParticipantsInZone();
	    	SetActive();
	    	AnnounceRegistration();
	    	eventWait(_joinTime * 60 * 1000);
	    	if(checkMinPlayers(joined))
	    	{
	    	prepare();
 	    	started = true;
	    	prepareWait(2 * 60 * 1000);
	    	closeAdenColosseumDoors();
	    	teleAllPlayers();
	    	battleWait(1 * 60 * 1000);
	    	for(L2PcInstance player : players)
	    	player.setIsParalyzed(false);
	    	for (L2PcInstance player : players)
	    	player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event started! Go kill your enemies!"));
	    	battleTime(Config.LMS_BATTLE_TIME * 60 * 1000);
	    	ending();
	    	}
	    	else
	    	{
	    		Announcements.getInstance().announceToAll("LMS: Not enough players for the event. Min Requested : " + MinPlayers + ", Participating : " + joined,true);
	    		SetInActive();
	    	    EventClear();
	    	}
		}
		   public static void eventWait(long interval)
		    {
			   long startWaiterTime = System.currentTimeMillis();
				int seconds = (int) (interval / 1000);

				while (startWaiterTime + interval > System.currentTimeMillis())
				{
					seconds--; // here because we don't want to see two time announce at the same time

					if (isActive())
					{
						switch (seconds)
						{
						case 3600: // 1 hour left
							if (isActive())
							{
								Announcements.getInstance().gameAnnounceToAll("LMS: Joinable in Giran Harbor!");
								Announcements.getInstance().gameAnnounceToAll("LMS: " + seconds / 60 / 60 + " hour(s) till registration ends!");

							}
							break;
						case 1800: // 30 minutes left
						case 900: // 15 minutes left
						case 600: //  10 minutes left 
						case 300: // 5 minutes left
						case 60: // 1 minute left
							if (isActive())
							{
								Announcements.getInstance().gameAnnounceToAll("LMS: Joinable in Giran Harbor!");
								Announcements.getInstance().gameAnnounceToAll("LMS: " + seconds / 60 + " minute(s) till registration ends!");
							}
							break;
						case 30: // 30 seconds left
						case 10: // 10 seconds left
						case 3: // 3 seconds left
						case 2: // 2 seconds left
						case 1: // 1 seconds left
							if (isActive())
								Announcements.getInstance().gameAnnounceToAll("LMS: " + seconds + " second(s) till registration ends!");
							break;
						}
					}

					long startOneSecondWaiterStartTime = System.currentTimeMillis();

					// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
					while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
					{
						try
						{
							Thread.sleep(1);
						}
						catch (InterruptedException ie)
						{
						}
					}
				}
			}
		   
		   public static void prepareWait(long interval)
		    {
			   long startWaiterTime = System.currentTimeMillis();
				int seconds = (int) (interval / 1000);

				while (startWaiterTime + interval > System.currentTimeMillis())
				{
					seconds--; // here because we don't want to see two time announce at the same time

					if (isActive())
					{
						switch (seconds)
						{
						case 3600: // 1 hour left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "You will be teleported to the fighting area in " + seconds / 60 / 60 +" hour(s)."));
							}
							break;
						case 1800: // 30 minutes left
						case 900: // 15 minutes left
						case 600: //  10 minutes left 
						case 300: // 5 minutes left
						case 60: // 1 minute left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "You will be teleported to the fighting area in " + seconds / 60 +" minute(s)."));
							}
							break;
						case 30: // 30 seconds left
						case 10: // 10 seconds left
						case 3: // 3 seconds left
						case 2: // 2 seconds left
						case 1: // 1 seconds left
							if (isActive())
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "You will be teleported to the fighting area in " + seconds  +" second(s)."));
							break;
						}
					}

					long startOneSecondWaiterStartTime = System.currentTimeMillis();

					// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
					while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
					{
						try
						{
							Thread.sleep(1);
						}
						catch (InterruptedException ie)
						{
						}
					}
				}
			}
		   
		   public static void battleWait(long interval)
		    {
			   long startWaiterTime = System.currentTimeMillis();
				int seconds = (int) (interval / 1000);

				while (startWaiterTime + interval > System.currentTimeMillis())
				{
					seconds--; // here because we don't want to see two time announce at the same time

					if (isActive())
					{
						switch (seconds)
						{
						case 3600: // 1 hour left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS: ", "In " + seconds / 60 / 60 + " hour(s) you will be unparalyzed and you will start fighting.The last one who survives is the winner"));
							}
							break;
						case 1800: // 30 minutes left
						case 900: // 15 minutes left
						case 600: //  10 minutes left 
						case 300: // 5 minutes left
						case 60: // 1 minute left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS: ", "In " + seconds / 60 + " minute(s) you will be unparalyzed and you will start fighting.The last one who survives is the winner"));
							}
							break;
						case 30: // 30 seconds left
						case 10: // 10 seconds left
						case 3: // 3 seconds left
						case 2: // 2 seconds left
						case 1: // 1 seconds left
							if (isActive())
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS: ", "In " + seconds + " second(s) you will be unparalyzed and you will start fighting.The last one who survives is the winner"));
							break;
						}
					}

					long startOneSecondWaiterStartTime = System.currentTimeMillis();

					// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
					while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
					{
						try
						{
							Thread.sleep(1);
						}
						catch (InterruptedException ie)
						{
						}
					}
				}
			}
		   
		   public static void battleTime(long interval)
		    {
			   long startWaiterTime = System.currentTimeMillis();
				int seconds = (int) (interval / 1000);

				while (startWaiterTime + interval > System.currentTimeMillis())
				{
					seconds--; // here because we don't want to see two time announce at the same time

					if (isActive())
					{
						switch (seconds)
						{
						case 3600: // 1 hour left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event ends in " + seconds / 60 / 60 +" hour(s)."));
								//announcewinnerDiscon();
							}
							break;
						case 1800: // 30 minutes left
						case 900: // 15 minutes left
						case 600: //  10 minutes left 
						case 300: // 5 minutes left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event ends in " + seconds / 60 +" minute(s)."));
								//announcewinnerDiscon();
							}
							break;
						case 60: // 1 minute left
							if (isActive())
							{
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event ends in " + seconds / 60 +" minute(s)."));
								//announcewinnerDiscon();
							}
							break;
						case 30: // 30 seconds left
						case 10: // 10 seconds left
						case 3: // 3 seconds left
						case 2: // 2 seconds left
						case 1: // 1 seconds left
							if (isActive())
								for (L2PcInstance player : players)
								player.sendPacket(new CreatureSay(1, Say2.HERO_VOICE, "LMS:", "Event ends in " + seconds  +" second(s)."));
							    announcewinnerDiscon();
							break;
						}
					}

					long startOneSecondWaiterStartTime = System.currentTimeMillis();

					// only the try catch with Thread.sleep(1000) give bad countdown on high wait times
					while (startOneSecondWaiterStartTime + 1000 > System.currentTimeMillis())
					{
						try
						{
							Thread.sleep(1);
						}
						catch (InterruptedException ie)
						{
						}
					}
				}
			}
		   
		   private static void closeAdenColosseumDoors()
			{
				DoorTable.getInstance().getDoor(24190002).closeMe();
				DoorTable.getInstance().getDoor(24190003).closeMe();

				try
				{
					Thread.sleep(20);
				}
				catch(InterruptedException ie) {
		            LOGGER.error("", ie);

		            LOGGER.info("Error, " + ie.getMessage());
		        }
			}

			/**
			 * Open aden colosseum doors.
			 */
			private static void openAdenColosseumDoors()
			{
				DoorTable.getInstance().getDoor(24190002).openMe();
				DoorTable.getInstance().getDoor(24190003).openMe();

			}
			
			public static boolean checkMinPlayers(int players)
			{
				return MinPlayers <= players;
			}
			
		public void setEventStartTime(String newTime)
		{
			startEventTime = newTime;
		}
		
		public static void setJoinTime(int time)
		{
			_joinTime = time;
		}
		
		public static LastManStanding getNewInstance()
		{
			return new LastManStanding();
		}
		
		public static String get_eventName()
		{
			return _eventName;
		}

		@Override
		public String getEventIdentifier()
		{
			return _eventName;
		}

		@Override
		public String getEventStartTime()
		{
			return startEventTime;
		}
}