package com.l2jfrozen.gameserver.ai.custom;

import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;

import java.util.logging.Logger;

/**
 * @Iridin
 * 
 */
public class StatusChecker {
    protected static final Logger _log = Logger.getLogger(StatusChecker.class.getName());


    public static void checkStatuses(L2PcInstance player) {
        if (player.isKamalokaStatus()) {
            long now = System.currentTimeMillis();
            long endDay = player.getKamalokaStatusEnd();

            if (now > endDay)
            {
                TimeLimit.removeKamalokaStatus(player);
                _log.info("4S mission 1 restriction ends: " + player.getName());
            }
        }
        
        if (player.isKamaloka2Status()) {
            long now = System.currentTimeMillis();
            long endDay = player.getKamaloka2StatusEnd();

            if (now > endDay)
            {
                TimeLimit.removeKamaloka2Status(player);
                _log.info("4S mission 2 restriction ends: " + player.getName());
            }
        }
        
        if (player.isKamaloka3Status()) {
            long now = System.currentTimeMillis();
            long endDay = player.getKamaloka3StatusEnd();

            if (now > endDay)
            {
                TimeLimit.removeKamaloka3Status(player);
                _log.info("4S mission 3 restriction ends: " + player.getName());
            }
        }
    }


    public static void startChecker() {
        ThreadPoolManager.getInstance().scheduleEffectAtFixedRate(new Checker(), 0, 300000);
    }

    private static class Checker implements Runnable
    {
        Checker() {}

        @Override
        public void run()
        {
            for (L2PcInstance player : L2World.getInstance().getAllPlayers())
            {
                checkStatuses(player);
            }

        }
    }



}
