package com.l2jfrozen.gameserver.ai.custom;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * @Iridin
 * 
 */
public class StatusDbUpdater {
    protected static final Logger _log = Logger.getLogger(StatusDbUpdater.class.getName());

    private static String REPLACE_QUERY =
            "REPLACE INTO characters_custom_data (obj_Id, char_name, hero, noble, donator, kama_limit, kama_end_date, kama2_limit, kama2_end_date, kama3_limit, kama3_end_date, kama_hwid, kama2_hwid, kama3_hwid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private static final String SELECT_QUERY =
            "SELECT kama_limit, kama_end_date, kama2_limit, kama2_end_date, kama3_limit, kama3_end_date FROM characters_custom_data WHERE obj_Id = ?";

    public static void updateDb(L2PcInstance player) {
        if(player == null)
            return;

        Connection con = null;

        try
        {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement stmt = con.prepareStatement(REPLACE_QUERY);

            stmt.setInt(1, player.getObjectId());
            stmt.setString(2, player.getName());
            stmt.setInt(3, player.isHero() ? 1 : 0);
            stmt.setInt(4, player.isNoble() ? 1 : 0);            
            stmt.setInt(5, player.isDonator() ? 1 : 0);    
            stmt.setInt(6, player.isKamalokaStatus() ? 1 : 0);
            stmt.setLong(7, player.getKamalokaStatusEnd());
            stmt.setInt(8, player.isKamaloka2Status() ? 1 : 0);
            stmt.setLong(9, player.getKamaloka2StatusEnd());
            stmt.setInt(10, player.isKamaloka3Status() ? 1 : 0);
            stmt.setLong(11, player.getKamaloka3StatusEnd());
            stmt.setString(12, player.isKamalokaStatus() ? player.getHWid() : null);
            stmt.setString(13, player.isKamaloka2Status() ? player.getHWid() : null);
            stmt.setString(14, player.isKamaloka3Status() ? player.getHWid() : null);
            stmt.execute();
            stmt.close();
            stmt = null;

        }
        catch (SQLException e)
        {
            _log.warning("Error: could not store char statuses data info: " + e);
        }
        finally
        {
            CloseUtil.close(con);
        }
    }

    public static void updatePlayer(L2PcInstance player) {
        Connection con = null;

        try
        {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement statement = con.prepareStatement(SELECT_QUERY);
            statement.setInt(1, player.getObjectId());

            ResultSet rset = statement.executeQuery();

            while(rset.next())
            {
                int kamaLimit = rset.getInt("kama_limit");
                long kamaEnd = rset.getLong("kama_end_date");
                int kama2Limit = rset.getInt("kama2_limit");
                long kama2End = rset.getLong("kama2_end_date");
                int kama3Limit = rset.getInt("kama3_limit");
                long kama3End = rset.getLong("kama3_end_date");
                
                if (kamaLimit > 0) {
                    player.setKamalokaStatus(true);
                    player.setKamalokaStatusEnd(kamaEnd);
                }
                
                if (kama2Limit > 0) {
                    player.setKamaloka2Status(true);
                    player.setKamaloka2StatusEnd(kama2End);
                }
                
                if (kama3Limit > 0) {
                    player.setKamaloka3Status(true);
                    player.setKamaloka3StatusEnd(kama3End);
                }
            }
            rset.close();
            statement.close();
            statement = null;
            rset = null;
        }
        catch(SQLException e)
        {
            if(Config.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();
            _log.warning("Error: could not restore char statuses data info: " + e);
        }
        finally
        {
            CloseUtil.close(con);
        }
    }
}