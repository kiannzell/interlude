/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.ai.custom;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import com.l2jfrozen.gameserver.ai.custom.KamalokaCoT;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

/**
 * @author Iridin
 * 
 */
public class TimeLimit
{
    protected static final Logger _log = Logger.getLogger(TimeLimit.class.getName());

    public static void addKamalokaStatus(L2PcInstance targetPlayer, int hours) {

        if(!targetPlayer.isKamalokaStatus())
        {
            targetPlayer.setKamalokaStatus(true);
            long time = System.currentTimeMillis() + (hours * 3600000);
            targetPlayer.setKamalokaStatusEnd(time);

            //update to db
            StatusDbUpdater.updateDb(targetPlayer);

            //targetPlayer.updateNameTitleColor();
            targetPlayer.sendMessage("You will be allowed to join 4S mission 1 again in " + hours + " hours!");
        }
        targetPlayer = null;
    }
        
    public static void addKamaloka2Status(L2PcInstance targetPlayer, int hours) {
    	if(!targetPlayer.isKamaloka2Status())
        {
            targetPlayer.setKamaloka2Status(true);
            long time = System.currentTimeMillis() + (hours * 3600000);
            targetPlayer.setKamaloka2StatusEnd(time);

            //update to db
            StatusDbUpdater.updateDb(targetPlayer);

            //targetPlayer.updateNameTitleColor();
            targetPlayer.sendMessage("You will be allowed to join 4S mission 2 again in " + hours + " hours!");
        }
        targetPlayer = null;
    }
    
    public static void addKamaloka3Status(L2PcInstance targetPlayer, int hours) {       
       if(!targetPlayer.isKamaloka3Status())
        {
            targetPlayer.setKamaloka3Status(true);
            long time = System.currentTimeMillis() + (hours * 3600000);
            targetPlayer.setKamaloka3StatusEnd(time);

            //update to db
            StatusDbUpdater.updateDb(targetPlayer);

            //targetPlayer.updateNameTitleColor();
            targetPlayer.sendMessage("You will be allowed to join 4S mission 3 again in " + hours + " hours!");
        }

        targetPlayer = null;
    }

    public static void removeKamalokaStatus(L2PcInstance targetPlayer) {
        if(targetPlayer.isKamalokaStatus())
        {
            targetPlayer.setKamalokaStatus(false);
            targetPlayer.setKamalokaStatusEnd(0);
            KamalokaCoT.releaseHwidList(targetPlayer.getHWid());

            //update db
            StatusDbUpdater.updateDb(targetPlayer);

            targetPlayer.sendMessage("You are allowed to join 4S mission 1 event now!");
        }
        targetPlayer = null;
    }
    
    public static void removeKamaloka2Status(L2PcInstance targetPlayer) {
        if(targetPlayer.isKamaloka2Status())
        {
            targetPlayer.setKamaloka2Status(false);
            targetPlayer.setKamaloka2StatusEnd(0);
            KamalokaDvC.releaseHwidList(targetPlayer.getHWid());

            //update db
            StatusDbUpdater.updateDb(targetPlayer);

            targetPlayer.sendMessage("You are allowed to join 4S mission 2 event now!");
        }
        targetPlayer = null;
    }
        
    public static void removeKamaloka3Status(L2PcInstance targetPlayer) {
    	if(targetPlayer.isKamaloka3Status())
        {
            targetPlayer.setKamaloka3Status(false);
            targetPlayer.setKamaloka3StatusEnd(0);
            KamalokaGc.releaseHwidList(targetPlayer.getHWid());

            //update db
            StatusDbUpdater.updateDb(targetPlayer);

            targetPlayer.sendMessage("You are allowed to join Kamaloka 4S mission 3 now!");
        }

        targetPlayer = null;
    }

    public static void checkKamalokaStatus(L2PcInstance targetPlayer) {
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamalokaStatusEnd();

        if (now > endDay)
        {
            TimeLimit.removeKamalokaStatus(targetPlayer);
        }
        else
        {
            TimeLimit.sendInfo(targetPlayer);
        }
    }
    
    public static void checkKamaloka2Status(L2PcInstance targetPlayer) {
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamaloka2StatusEnd();

        if (now > endDay)
        {
            TimeLimit.removeKamaloka2Status(targetPlayer);
        }
        else
        {
            TimeLimit.sendInfo(targetPlayer);
        }
    }
    
    public static void checkKamaloka3Status(L2PcInstance targetPlayer) {
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamaloka3StatusEnd();

        if (now > endDay)
        {
            TimeLimit.removeKamaloka3Status(targetPlayer);
        }
        else
        {
            TimeLimit.sendInfo(targetPlayer);
        }
    }

    public static void sendInfo(L2PcInstance targetPlayer) {

        if (!targetPlayer.isKamalokaStatus()) {
            targetPlayer.sendMessage("You do not have Experience status.");
            return;
        }
        
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamalokaStatusEnd();
        Date dt = new Date(endDay);
        long _daysleft = (endDay - now)/86400000;
        if (_daysleft > 30) {
            SimpleDateFormat df = new SimpleDateFormat("dd MM yyyy");
            targetPlayer.sendMessage("4S mission 1 restriction ends in " + df.format(dt) + ". enjoy the Game.");
        }
        else if (_daysleft > 0)
            targetPlayer.sendMessage("Left " + ((int)_daysleft + 1) + " day(s) to 4S mission 1 restriction ends.");
        else if (_daysleft < 1)
        {
            long hour = (endDay - now)/3600000;
            if (hour > 0)
                targetPlayer.sendMessage("Left " + ((int)hour + 1) + " hour(s) to 4S mission 1 restriction status ends.");
            else {
                targetPlayer.sendMessage("Left less then one hour to 4S mission 1 restriction ends.");
            }
        }
    }
    
    public static void sendInfo2(L2PcInstance targetPlayer) {

        if (!targetPlayer.isKamaloka2Status()) {
            targetPlayer.sendMessage("You do not have Experience status.");
            return;
        }
        
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamaloka2StatusEnd();
        Date dt = new Date(endDay);
        long _daysleft = (endDay - now)/86400000;
        if (_daysleft > 30) {
            SimpleDateFormat df = new SimpleDateFormat("dd MM yyyy");
            targetPlayer.sendMessage("4S mission 2 restriction ends in " + df.format(dt) + ". enjoy the Game.");
        }
        else if (_daysleft > 0)
            targetPlayer.sendMessage("Left " + ((int)_daysleft + 1) + " day(s) to 4S mission 2 restriction ends.");
        else if (_daysleft < 1)
        {
            long hour = (endDay - now)/3600000;
            if (hour > 0)
                targetPlayer.sendMessage("Left " + ((int)hour + 1) + " hour(s) to 4S mission 2 restriction status ends.");
            else {
                targetPlayer.sendMessage("Left less then one hour to 4S mission 2 restriction ends.");
            }
        }
    }
    
    public static void sendInfo3(L2PcInstance targetPlayer) {

        if (!targetPlayer.isKamaloka3Status()) {
            targetPlayer.sendMessage("You do not have Experience status.");
            return;
        }
        
        long now = System.currentTimeMillis();
        long endDay = targetPlayer.getKamaloka3StatusEnd();
        Date dt = new Date(endDay);
        long _daysleft = (endDay - now)/86400000;
        if (_daysleft > 30) {
            SimpleDateFormat df = new SimpleDateFormat("dd MM yyyy");
            targetPlayer.sendMessage("4S mission 3 restriction ends in " + df.format(dt) + ". enjoy the Game.");
        }
        else if (_daysleft > 0)
            targetPlayer.sendMessage("Left " + ((int)_daysleft + 1) + " day(s) to 4S mission 3 restriction ends.");
        else if (_daysleft < 1)
        {
            long hour = (endDay - now)/3600000;
            if (hour > 0)
                targetPlayer.sendMessage("Left " + ((int)hour + 1) + " hour(s) to 4S mission 3 restriction status ends.");
            else {
                targetPlayer.sendMessage("Left less then one hour to 4S mission 3 restriction ends.");
            }
        }
    }
}
