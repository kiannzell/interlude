/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.ai.custom;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javolution.util.FastList;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.managers.GrandBossManager;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Party;
import com.l2jfrozen.gameserver.model.actor.instance.L2NpcInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.entity.event.LastManStanding;
import com.l2jfrozen.gameserver.model.entity.olympiad.Olympiad;
import com.l2jfrozen.gameserver.model.quest.Quest;
import com.l2jfrozen.gameserver.network.L2GameClient;
import com.l2jfrozen.gameserver.network.serverpackets.ExShowScreenMessage;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import com.l2jfrozen.util.random.Rnd;


/**
 * Kamaloka
 * @author Iridin
 */
public class KamalokaCoT extends Quest implements Runnable
{
	
	protected static final Logger _log = Logger.getLogger(KamalokaCoT.class.getName());
	
	private static final int[][] _mobLoc = //minions ID and locations
	{
		{ Config.K1_MINIBOSS1_ID,Config.K1_MINIBOSS1A_X,Config.K1_MINIBOSS1A_Y,Config.K1_MINIBOSS1A_Z,243 },	//spawn miniRB 1 - location
		{ Config.K1_MINIBOSS2_ID,Config.K1_MINIBOSS2_X,Config.K1_MINIBOSS2_Y,Config.K1_MINIBOSS2_Z,32847 },		//spawn miniRB 2 - location
		{ Config.K1_MINIBOSS1_ID,Config.K1_MINIBOSS1B_X,Config.K1_MINIBOSS1B_Y,Config.K1_MINIBOSS1B_Z,16216 },	//spawn miniRB 3 - location
		{ Config.K1_MINIBOSS1_ID,Config.K1_MINIBOSS1C_X,Config.K1_MINIBOSS1C_Y,Config.K1_MINIBOSS1C_Z,49043 },	//spawn miniRB 4 - location
		{ Config.K1_MINIBOSS1_ID,Config.K1_MINIBOSS1D_X,Config.K1_MINIBOSS1D_Y,Config.K1_MINIBOSS1D_Z,32847 },	//spawn miniRB 5 - location
		{ Config.K1_MINIBOSS3_ID,Config.K1_MINIBOSS3_X,Config.K1_MINIBOSS3_Y,Config.K1_MINIBOSS3_Z,32847 },		//spawn miniRB 6 - location
		/*{ 35643,15427,-113167,-3117,20374 },	//gate
		{ 35643,19859,-110999,-3237,42408 },	//gate
		{ 35643,22378,-118955,-3301,14064 }		//gate	*/
	};
	
	private static final int MINI1 = Config.K1_MINIBOSS1_ID;
	private static final int MINI2 = Config.K1_MINIBOSS2_ID;
	private static final int MINI3 = Config.K1_MINIBOSS3_ID;
	private static final int KAMABOSS = Config.K1_FINALBOSS_ID; //kama Head boss ID
	private static final int GUIDE = 99994;
	private static final int CUBE = 99993;
	
	//Kamaloka status:
	private static final byte DORMANT = 0;		//Kamaloka is available
	private static final byte OCCUPIED = 1; 	//Someone is in event
	
	private static int _KillMinion = 0; // low bosses at stat (2-4)
	private static int _KillMinion2 = 0; // medium boss (1)
	private static int _KillMinion3 = 0; // last boss before KAMABOSS (1)
	
	private List<L2PcInstance> _PlayersInside = new FastList<L2PcInstance>();
	private List<L2NpcInstance> _Room1Mobs = new FastList<L2NpcInstance>();
	private List<L2NpcInstance> _Room2Mobs = new FastList<L2NpcInstance>();
	private List<L2NpcInstance> _KamaMob = new FastList<L2NpcInstance>();
	private List<L2NpcInstance> _Doors12Mobs = new FastList<L2NpcInstance>();
	private List<L2NpcInstance> _Cubes = new FastList<L2NpcInstance>();
	private static List<String> _hwidlist; 
	
	public KamalokaCoT(int id, String name, String descr)
	{
		super(id,name,descr);
		int[] mob = {KAMABOSS,Config.K1_MINIBOSS1_ID,Config.K1_MINIBOSS2_ID,Config.K1_MINIBOSS3_ID,35643};
		int[] cubes = {99993};
		registerMobs(mob);
		registerMobs(cubes);
		addStartNpc(GUIDE);
		addTalkId(GUIDE);
		addStartNpc(CUBE);
		addTalkId(CUBE);
		GrandBossManager.getInstance().setBossStatus(KAMABOSS, DORMANT);
		loadHwidTimeList();
	}
	
	@Override
	public String onAdvEvent (String event, L2NpcInstance npc, L2PcInstance player)
	{
		if (event.equalsIgnoreCase("room1_spawn"))
		{
			for (int i = 0; i <= 5; i++)
			{
				L2NpcInstance mob = addSpawn(_mobLoc[i][0],_mobLoc[i][1],_mobLoc[i][2],_mobLoc[i][3],_mobLoc[i][4],false,0);
				synchronized(_Room1Mobs){
					_Room1Mobs.add(mob);
				}
				
			}
		}
		else if (event.equalsIgnoreCase("room2_spawn"))
		{
			for (int i = 6; i <= 6; i++)
			{
				L2NpcInstance mob = addSpawn(_mobLoc[i][0],_mobLoc[i][1],_mobLoc[i][2],_mobLoc[i][3],_mobLoc[i][4],false,0);
				synchronized(_Room2Mobs){
					_Room2Mobs.add(mob);
				}
				
			}
		}
		else if (event.equalsIgnoreCase("room1_del"))
		{
			synchronized(_Room1Mobs){
				for (L2NpcInstance mob : _Room1Mobs)
				{
					if (mob != null)
						mob.deleteMe();
				}
				_Room1Mobs.clear();
			}			
		}
		else if (event.equalsIgnoreCase("room2_del"))
		{
			synchronized(_Room2Mobs){
				for (L2NpcInstance mob : _Room2Mobs)
				{
					if (mob != null)
						mob.deleteMe();
				}
				_Room2Mobs.clear();
			}		
		}
		else if (event.equalsIgnoreCase("kama_spawn"))
		{
			L2NpcInstance mob = addSpawn(Config.K1_FINALBOSS_ID,Config.K1_FINALBOSS_X,Config.K1_FINALBOSS_Y,Config.K1_FINALBOSS_Z,50177,false,0);
			synchronized(_KamaMob){
				_KamaMob.add(mob);	
			}
		}
		else if (event.equalsIgnoreCase("kama_del"))
		{
			synchronized(_KamaMob){
				for (L2NpcInstance mob : _KamaMob)
				{
					if (mob != null)
						mob.deleteMe();
				}
				_KamaMob.clear();
			}		
		}
		else if (event.equalsIgnoreCase("clean"))
		{
			_KillMinion = 0;
			_KillMinion2 = 0;
			_KillMinion3 = 0;
		}
		else if (event.equalsIgnoreCase("close_room12"))
		{
			for (int i = 7; i <= 9; i++)
			{
				L2NpcInstance mob = addSpawn(_mobLoc[i][0],_mobLoc[i][1],_mobLoc[i][2],_mobLoc[i][3],_mobLoc[i][4],false,0);
				synchronized(_Doors12Mobs){
				_Doors12Mobs.add(mob);
				}
			}
		}
		else if (event.equalsIgnoreCase("open_room12"))
		{
			synchronized(_Doors12Mobs){
				for (L2NpcInstance mob : _Doors12Mobs)
				{
					if (mob != null)
						mob.deleteMe();
				}
				_Doors12Mobs.clear();
			}
		}
		else if (event.equalsIgnoreCase("minions_death"))
		{		
			cancelQuestTimer("room1_spawn", npc, null);
			cancelQuestTimer("room2_spawn", npc, null);
			startQuestTimer("room1_del", 1000, npc, null);
			startQuestTimer("room2_del", 1000, npc, null);
		}
		else if (event.equalsIgnoreCase("port_out"))
		{		
			portOutAllPlayers();
		}
		else if (event.equalsIgnoreCase("kama_death"))
		{
			cancelQuestTimers("timelimit");
			startQuestTimer("kama_del", 1000, npc, null);
			startQuestTimer("open_room12", 1000, npc, null);
			startQuestTimer("spawn_cubes", 1000, npc, null);
			startQuestTimer("clean", 1000, npc, null);
			
			startQuestTimer("port_out", 25000, npc, null);
			startQuestTimer("despawn_cubes", 26250, npc, null);
			startQuestTimer("kamaloka_unlock", 26500, npc, null);
		}
		else if (event.equalsIgnoreCase("spawn_cubes"))
		{
			L2NpcInstance cubes = addSpawn(CUBE,Config.K1_FINALBOSS_X,Config.K1_FINALBOSS_Y,Config.K1_FINALBOSS_Z,16384,false,900000); //cube out spawn
			synchronized(_Cubes){
				_Cubes.add(cubes);	
			}
		}
		else if (event.equalsIgnoreCase("despawn_cubes"))
		{
			synchronized(_Cubes){
				for (L2NpcInstance cubes : _Cubes)
				{
					if (cubes != null)
						cubes.deleteMe();
				}
				_Cubes.clear();
			}
		}
		else if (event.equalsIgnoreCase("kamaloka_unlock"))
		{
			GrandBossManager.getInstance().setBossStatus(KAMABOSS,DORMANT);
		}
		else if (event.equalsIgnoreCase("timelimit"))
		{

			startQuestTimer("minions_death", 500, npc, null);
			
			startQuestTimer("open_room12", 1000, npc, null);
			startQuestTimer("port_out", 5000, npc, null);

			startQuestTimer("clean", 6000, npc, null);
			startQuestTimer("despawn_cubes", 6250, npc, null);
			startQuestTimer("kamaloka_unlock", 6500, npc, null);
		}
		
		return super.onAdvEvent(event, npc, player);
	}
	
	@Override
	public String onTalk (L2NpcInstance npc, L2PcInstance player)
	{
		if (npc.getNpcId() == CUBE)
		{
			int x = Config.KAMALOKA_PORT_OUT_X + Rnd.get(500);
			int y = Config.KAMALOKA_PORT_OUT_Y + Rnd.get(500);
			int z = Config.KAMALOKA_PORT_OUT_Z;
			player.teleToLocation(x, y, z);
			synchronized(_PlayersInside) {
				_PlayersInside.remove(player);
			}
			return null;
		}
		
		String htmltext = "";
		
		Integer status = GrandBossManager.getInstance().getBossStatus(KAMABOSS);
		
		if (status == OCCUPIED)
		{
			htmltext = "<html><body>Someone is already fighting in event - please wait until they finish</body></html>";
		}
		else if (status == DORMANT)
		{
			boolean party_check_success = true;
			boolean party_check = Config.K1_KAMA_CHECKPARTY;
			
			L2Party party = player.getParty();			
			if (party_check) {
				
				if ((!player.isInParty() || !player.getParty().isLeader(player)) || (player.getParty().getMemberCount() < Config.K1_KAMA_PARTY_SIZE)) {
					htmltext = "<html><body>You have to be LEADER of "+ Config.K1_KAMA_PARTY_SIZE +" member party.</body></html>";
					party_check_success = false;
				}
				
				if (player.isInParty() && player.getPvpKills() < Config.K1_KAMA_PVP_NEEDS) {
					htmltext = "<html><body>You need to have "+ Config.K1_KAMA_PVP_NEEDS +" Pvps.</body></html>";
					party_check_success = false;
				}
				
				if (player.isInParty() && player.isKamalokaStatus()) {
					htmltext = "<html><body>You can't join now wait some hours.</body></html>";
					TimeLimit.sendInfo(player);
					party_check_success = false;
				}
				
				if (player.isInParty()) {													
					for (L2PcInstance member : party.getPartyMembers())
					{						
						if (member != member.getParty().getLeader()) {
							if (!Config.K1_ALLOW_HWIDBOX_KAMA) {
								if (player.getHWid()!=null && player.getHWid().length()!=0) {
									if(L2GameClient.isSameHWID(player.getHWid(),member.getHWid())) 
									{
										htmltext = "<html><body>Only one character per PC is available</body></html>";
										party_check_success = false;
									}
									else if(player._inEventTvT || player._inEventDM || player._inEventRaid)
									{
										htmltext = "<html><body>You cannot join while register in another event!</body></html>";
										party_check_success = false;
									}
									else if(member._inEventTvT ||member._inEventDM || member._inEventRaid)
									{
										htmltext = "<html><body>Your party members is already register in another event!</body></html>";
										party_check_success = false;
									}
									else if(Olympiad.getInstance().isRegistered(player) || player.isInOlympiadMode())
									{
										htmltext = "<html><body>You're registered in olympiad!</body></html>";
										party_check_success = false;
									}
									else if(Olympiad.getInstance().isRegistered(member) || member.isInOlympiadMode())
									{
										htmltext = "<html><body>Your party members is registered in olympiad!</body></html>";
										party_check_success = false;
									}
									else if(LastManStanding.players.contains(player))
									{
										htmltext = "<html><body>You are registered in LMS event.</body></html>";
										party_check_success = false;
									}
									else if(LastManStanding.players.contains(member))
									{
										htmltext = "<html><body>Your party member is registered in LMS event.</body></html>";
										party_check_success = false;
									}
									else if(player.isAio() || member.isAio())
									{
										htmltext = "<html><body>Aio buffers cannot join!</body></html>";
										party_check_success = false;
									}
								}
							}
						}
						
						if (member.getPvpKills() < Config.K1_KAMA_PVP_NEEDS) {
							htmltext = "<html><body>Some party member can't join in event because he dont have "+Config.K1_KAMA_PVP_NEEDS+" pvps </body></html>";
							party_check_success = false;
						}
						
						if (member.isKamalokaStatus()) {
							htmltext = "<html><body>Some party member can't join 4S Mission 1 event. He is in time restriction.<br><center>Or try again in 3 seconds. Hwids has been refreshed.</center></body></html>";
							StatusChecker.checkStatuses(member);
							TimeLimit.sendInfo(member);
							party_check_success = false;
						}
						
						
						if (!member.isInsideRadius(npc, 500, false, false)) {
							htmltext = "<html><body>All members must be near to 4S Manager.</body></html>";
							party_check_success = false;
						}
						
						String memberHwid = member.getHWid();						
						if (_hwidlist.contains(memberHwid)) {
							StatusChecker.checkStatuses(member);
							htmltext = "<html><body>Some Party member hwid is under 4S Mission 1 time restriction.<br><center>Or try again in 3 seconds. Hwids has been refreshed.</center></body></html>";
							party_check_success = false;
						}
							
					}
				}
			}
			
			if (party_check_success) {

					if (player.getParty() != null) {
						GrandBossManager.getInstance().setBossStatus(KAMABOSS, OCCUPIED);						
						for (L2PcInstance member : party.getPartyMembers())
						{
							if (!member.isInsideRadius(npc, 600, false, false))
								continue;
							
							synchronized(_PlayersInside) 
							{
								_PlayersInside.add(member);
							}							
							member.teleToLocation(getXFix(Config.K1_SPAWN_LOCX + Rnd.get(50)), getYFix(Config.K1_SPAWN_LOCY + Rnd.get(50)), getZFix(Config.K1_SPAWN_LOCZ));
							member.sendPacket(new ExShowScreenMessage("Kill all Bosses to get Final Boss !", 3*1000));
							
							player.setInKamaloka(true);
							member.setInKamaloka(true);
							
							TimeLimit.addKamalokaStatus(member,Config.K1_REPEAT_TIME);
							_hwidlist.add(member.getHWid());
														
							startQuestTimer("room1_spawn", 2000, npc, null);
							startQuestTimer("room2_spawn", 2000, npc, null);
							startQuestTimer("close_room12", 2000, npc, null);
							
							startQuestTimer("timelimit", (Config.K1_KAMA_TIMELIMIT*1000), npc, null, false); // 20 minute limit
						}							
					}
					else { //cannot port one player
						htmltext = "<html><body>You have to be leader of "+ Config.K1_KAMA_PARTY_SIZE +" member party.</body></html>";		
					}
			}
		}
		return htmltext;
	}
	
	@Override
	public String onAttack (L2NpcInstance npc, L2PcInstance attacker, int damage, boolean isPet)
	{
		return super.onAttack(npc, attacker, damage, isPet);
	}
	
	@Override
	public String onKill (L2NpcInstance npc, L2PcInstance killer, boolean isPet)
	{
		Integer status = GrandBossManager.getInstance().getBossStatus(KAMABOSS);
		L2Party party = killer.getParty();

		
		if (npc.getNpcId() == KAMABOSS && status == OCCUPIED)
		{
			startQuestTimer("kama_death", 1000, npc, null);			
		}
		
		else if (npc.getNpcId() == MINI1)
		{
			_KillMinion++;
			if (_KillMinion == 1)
			{
				for (L2PcInstance member : party.getPartyMembers())
				{
					member.sendPacket(new ExShowScreenMessage("First Knight down, keep going !", 6*1000));
				}
			}
			if (_KillMinion == 4)
			{
				for (L2PcInstance member : party.getPartyMembers())
				{
					member.sendPacket(new ExShowScreenMessage("Almost near to final boss !!", 6*1000));
				}
			}
			if (Config.K1_MUST_MINIBOSSES_KILLED) {
				if ((_KillMinion == 4) && (_KillMinion2 == 1) && (_KillMinion3 == 1))
				{
					for (L2PcInstance member : party.getPartyMembers())
					{
						member.sendPacket(new ExShowScreenMessage("Final Boss has been spawned!", 6*1000));
					}
					startQuestTimer("kama_spawn", 1000, npc, null);
					startQuestTimer("minions_death", 1000, npc, null);
				}
			}
		}
		
		else if (npc.getNpcId() == MINI2)
		{
			_KillMinion2++;
			if (_KillMinion2 == 1)
			{
				for (L2PcInstance member : party.getPartyMembers())
				{
					member.sendPacket(new ExShowScreenMessage("Another monster down! Good luck!", 6*1000));
				}
			}
			if (Config.K1_MUST_MINIBOSSES_KILLED) {
				if ((_KillMinion == 4) && (_KillMinion2 == 1) && (_KillMinion3 == 1))
				{
					for (L2PcInstance member : party.getPartyMembers())
					{
						member.sendPacket(new ExShowScreenMessage("Final Boss has been spawned!", 6*1000));
					}
					startQuestTimer("kama_spawn", 1000, npc, null);
					startQuestTimer("minions_death", 1000, npc, null);
				}
			}
			
		}
		
		else if (npc.getNpcId() == MINI3)
		{
			_KillMinion3 ++;
			if (_KillMinion3 == 1)
			{
				if (Config.K1_MUST_MINIBOSSES_KILLED) {
					if ((_KillMinion == 4) && (_KillMinion2 == 1) && (_KillMinion3 == 1))
					{
						for (L2PcInstance member : party.getPartyMembers())
						{
							member.sendPacket(new ExShowScreenMessage("Final Boss has been spawned!", 6*1000));
						}
						startQuestTimer("kama_spawn", 1000, npc, null);
						startQuestTimer("minions_death", 1000, npc, null);
					} else {
						for (L2PcInstance member : party.getPartyMembers())
						{
							member.sendPacket(new ExShowScreenMessage("Kill all Minions before spawn Final boss!", 6*1000));
						}
					}
				} else {
					startQuestTimer("kama_spawn", 1000, npc, null);
					startQuestTimer("minions_death", 1000, npc, null);
				}
			}
		}	
		return super.onKill(npc,killer,isPet);
	}
	
	@Override
	public L2NpcInstance addSpawn(int npcId, int x, int y, int z, int heading, boolean randomOffset, int despawnDelay)
	{
		return super.addSpawn(npcId, getXFix(x), getYFix(y), getZFix(z), heading, randomOffset, despawnDelay);
	}

	
	public int getXFix(int x)
	{
		return x /* - 262016*/;
	}
	public int getYFix(int y)
	{
		return y /* - 65278*/;
	}
	public int getZFix(int z)
	{
		return z /* - 4065*/;
	}
	
	public int reverseXFix(int x)
	{
		return x /* + 262016*/;
	}
	public int reverseYFix(int y)
	{
		return y /* + 65278*/;
	}
	public int reverseZFix(int z)
	{
		return z /* + 4065*/;
	}
	
	public void portOutAllPlayers()
	{
		if(_PlayersInside == null)
			return;

		if(_PlayersInside.isEmpty())
			return;

		for(L2Character character : _PlayersInside)
		{
			if(character == null)
			{
				continue;
			}

			if(character instanceof L2PcInstance)
			{
				L2PcInstance player = (L2PcInstance) character;

				if(player.isOnline() == 1)
				{
					player.teleToLocation(getXFix(Config.KAMALOKA_PORT_OUT_X + Rnd.get(50)), getYFix(Config.KAMALOKA_PORT_OUT_Y + Rnd.get(50)), getZFix(Config.KAMALOKA_PORT_OUT_Z));
					player.setInKamaloka(false);
				}

				player = null;
			}
		}
		_PlayersInside.clear();
	}
	
	public void loadHwidTimeList() {
		_hwidlist = new FastList<String>();
		Connection con = null;
		
		try
        {
            con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement stmt = con.prepareStatement("SELECT kama_hwid FROM characters_custom_data");

            ResultSet rset = stmt.executeQuery();
            while (rset.next()) {
				_hwidlist.add(rset.getString("kama_hwid"));
            }
            
            rset.close();
            stmt.close();
            stmt = null;
            rset = null;  
        }
        catch (SQLException e)
        {
            _log.warning("Error: Loading hwidlist1" + e);
        }
        finally
        {
        	CloseUtil.close(con);
        }
	}
	
	public static void releaseHwidList(String string) {
		_hwidlist.remove(string);
	}
	
	@Override
	public void run()
	{
	}
}